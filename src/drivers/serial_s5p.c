/* drivers/serial_s5p.c
 *
 * Samsung S5P serial driver.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <delay.h>
#include <dinos/file.h>
#include <dinos/io.h>
#include <dinos/irq.h>
#include <dinos/init.h>

#include <mach/board.h>
//#include <mach/map-exynos4.h>

/* FIXME: This value is dependent on the target board. */
static unsigned int base_addr=0;

enum {
    ULCON   = 0x0,
    UCON    = 0x4,
    UFCON   = 0x8,
    UMCON   = 0xC,
    UTRSTAT = 0x10,
    UERSTAT = 0x14,
    UFSTAT  = 0x18,
    UMSTAT  = 0x1C,
    UTXH    = 0x20,
    URXH    = 0x24,
    UBRDIV  = 0x28,
    UFRACVAL= 0x2C,
    UINTP   = 0x30,
    UINTSP  = 0x34,
    UINTM   = 0x38
};

static int serial_err_check(int op)
{
    u32 mask;

    if (op)
        mask = 0x8;
    else
        mask = 0xf;

    return Io.readl(base_addr+UERSTAT) & mask;
}

static int serial_open(struct file *f)
{
    return 0;
}

static int serial_close(struct file *f)
{
    return -1;
}

static int serial_read(struct file *f, char *ptr, int len)
{
    int i;
    if (len == 0)
	return 0;

    /* Check busy. */
    wait(!(Io.readl(base_addr+UTRSTAT) & 0x1));

    *ptr++ = Io.readl(base_addr+URXH);
    for(i = 1; i < len; ++i) {
        while(!(Io.readl(base_addr+UTRSTAT) & 0x2)) {
            if (serial_err_check(0))
                return -1;
        }
        *ptr++ = Io.readl(base_addr+URXH);
    }

    return i;
}

static int serial_write(struct file *f, const char *ptr, int len)
{
    int i;

    for (i = 0; i < len; ++i) {
        if (*ptr) {
            /* Check busy. */
            while(!(Io.readl(base_addr+UTRSTAT) & 0x2)) {
                if (serial_err_check(1))
                    return -1;
            }
            Io.writel(base_addr+UTXH, *ptr++);
        }
        if (*ptr == '\n') 
            Io.writel(base_addr+UTXH, '\r');
    }

    return i;
}

struct file_op drv_s5p_serial_op = {
    .open	= serial_open,
    .close	= serial_close,
    .read	= serial_read,
    .write	= serial_write,
};

int serial_init(void)
{
    base_addr = S5P_PA_UART1;

    return 0;
}

module_init(serial_init);
