/* drivers/core.c
 *
 * Driver core.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <string.h>
#include <dinos/file.h>
#include <dinos/driver.h>
#include <dinos/mem.h>

static int drv_init(void)
{
	return 0;	
}

static int drv_register(struct file_op *f_op, const char *name)
{
	struct file *f = Mem.alloc(sizeof(struct file), 0);

	f->f_op = f_op;
	strcpy(f->name, name);
	Vfs.add(f);

	return 0;
}

struct Drv Drv = {
	.init = drv_init,
	.reg = drv_register,
};
