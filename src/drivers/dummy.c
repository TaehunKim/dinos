/* drivers/dummy.c */

#include <stdio.h>
#include <string.h>

#include <dinos/file.h>
#include <dinos/io.h>
#include <dinos/init.h>
#include <dinos/mem.h>
#include <dinos/driver.h>

#include <mach/board.h>

static u32 rp = 0;

static int dummy_open(struct file *f)
{
    int i;

    DEBUGF("%s\n", __func__);

    rp = 0;
    return 0;
}

static int dummy_close(struct file *f)
{
    DEBUGF("%s\n", __func__);
    return 0;
}

static int dummy_read(struct file *f, char *ptr, int len)
{
    int i;
    DEBUGF("%s, rp = %d\n", __func__, rp);
   
    for (i = 0; i < len; ++i)
        ptr[i] = Io.readb(V2M_NOR0 + rp + i);
    rp += len;
    return len;
}

static int dummy_write(struct file *f, const char *ptr, int len)
{
    return 0;
}

struct file_op drv_dummy = {
    .open	= dummy_open,
    .close	= dummy_close,
    .read	= dummy_read,
    .write	= dummy_write,
};

int dummy_init(void)
{
    int fd;
    DEBUGF("Dummy device initialization.\n");
    fd = Drv.reg(&drv_dummy, "/pysh");
    DEBUGF("Dummy file descriptor = %d\n", fd);

    return 0;
}

module_init(dummy_init);
