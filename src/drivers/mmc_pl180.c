/* drivers/mmci_pl180.c
 *
 * ARM MMCI(Multimedia Card Interface) PL180 driver
 *
 * Copyright (C) 2015 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>

#include <dinos/file.h>
#include <dinos/io.h>
#include <dinos/irq.h>
#include <dinos/init.h>

#include <mach/board.h>

static u32 mmci_base = V2M_MMCI;

static int mmci_open(struct file *f)
{
    return 0;
}

static int mmci_close(struct file *f)
{
    return 0;
}

static int mmci_read(struct file *f, char *ptr, int len)
{
    return 0;
}

static int mmci_write(struct file *f, const char *ptr, int len)
{
    return 0;
}

static int mmci_ioctl(struct file *file, const unsigned int cmd, void *data)
{
    return 0;
}

struct file_op drv_pl80_op = {
    .open	= mmci_open,
    .close	= mmci_close,
    .read	= mmci_read,
    .write	= mmci_write,
    .ioctl  = mmci_ioctl,
};

int mmci_init(void)
{
    DEBUGF("PL180 Multimedia Card Interface initialization.\n");

    return 0;
}

module_init(mmci_init);
