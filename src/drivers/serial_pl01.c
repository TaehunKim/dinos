/* drivers/serial_pl01.c
 *
 * pl01 serial driver.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <delay.h>

#include <dinos/file.h>
#include <dinos/io.h>
#include <dinos/irq.h>
#include <dinos/init.h>

#include <mach/board.h>

enum {
    DR      = 0x00,
    ECR     = 0x04,
    FR      = 0x18,
    IBRD    = 0x24,
    FBRD    = 0x28,
    LCR_H   = 0x2c,
    CR      = 0x30,
    IFLS    = 0x34,
    IMSC    = 0x38,
    MIS     = 0x40,
    ICR     = 0x44
};

static u32 uart_base = V2M_UART0;

enum {
    FR_RXFE = 0x10,
    FR_RXFF = 0x40,
    FR_TXFF = 0x20,
};

static int serial_open(struct file *f)
{
    return 0;
}



static int serial_close(struct file *f)
{
    return -1;
}

static int serial_read(struct file *f, char *ptr, int len)
{
    int i;
    u32 data;
    if (len == 0)
        return 0;

    /* Check busy. */
    wait(Io.readl(uart_base + FR) & FR_RXFE)
        ;

    data = Io.readl(uart_base + DR);
    if (data & 0xffffff00)
        Io.writel(uart_base + ECR, 0xffffffff);

    *ptr++ = data;

    for(i = 1; i < len; ++i) {
        if (Io.readl(uart_base + FR) & FR_RXFF)
            break;
        *ptr++ = Io.readl(uart_base + DR);
    }

    return i;
}

static int serial_write(struct file *f, const char *ptr, int len)
{
    int i;

    for (i = 0; i < len; ++i) {
        /* Check busy. */
        while ((Io.readl(uart_base + FR) & FR_TXFF))
           udelay(1); 

        Io.writel(uart_base + DR, *ptr++);
    }

    return i;
}

struct file_op drv_pl01_op = {
    .open	= serial_open,
    .close	= serial_close,
    .read	= serial_read,
    .write	= serial_write,
};

int serial_init(void)
{
    DEBUGF("PL011 initialization.\n");

    return 0;
}

module_init(serial_init);
