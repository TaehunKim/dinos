/* drivers/video_pl110.c
 *
 * pl110 Color LCD driver.
 *
 * Copyright (C) 2013 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <delay.h>
#include <assert.h>
#include <string.h>

#include <dinos/driver.h>
#include <dinos/file.h>
#include <dinos/io.h>
#include <dinos/irq.h>
#include <dinos/init.h>

#include <arch/video.h>
#include <arch/errno.h>
#include <mach/board.h>

static volatile u32 clcd_base = CLCD_BASE;

static struct clcd_panel xvga = {
    .mode = {
        .name       = "XVGA",
        .refresh    = 60,
        .xres       = 1024,
        .yres       = 768,
        .pixclock   = 15748,
        .left_margin = 152,
        .right_margin = 48,
        .upper_margin = 23,
        .lower_margin = 3,
        .hsync_len  = 104,
        .vsync_len  = 4,
        .sync       = 0,
        .vmode      = 0,
    },
    .width  = -1,
    .height = -1,
    .tim2   = TIM2_BCD | TIM2_IPC,
    .cntl   = CNTL_LCDTFT | CNTL_BGR | CNTL_LCDVCOMP(1),
    .bpp    = 16,
};

#define FB_SIZE     (1024 * 768) 
u16 *fb = (u16 *)V2M_VIDEO_SRAM; /* Video SRAM area. */

static int clcd_open(struct file *f)
{
    u32 cntl = Io.readl(clcd_base + CLCD_CTRL);

    DEBUGF("%s\n", __func__);

    cntl |= CNTL_LCDEN;
    Io.writel(clcd_base + CLCD_CTRL, cntl);

    mdelay(20);

    cntl |= CNTL_LCDPWR;
    Io.writel(clcd_base + CLCD_CTRL, cntl);
    DEBUGF("%s clde opend\n", __func__);
 
    return 0;
}

static int clcd_close(struct file *f)
{
    u32 cntl = Io.readl(clcd_base + CLCD_CTRL);

    DEBUGF("%s\n", __func__);

    cntl &= ~CNTL_LCDEN;
    Io.writel(clcd_base + CLCD_CTRL, cntl);

    mdelay(20);

    cntl &= ~CNTL_LCDPWR;
    Io.writel(clcd_base + CLCD_CTRL, cntl);

    return 0;
}

static int clcd_read(struct file *f, char *ptr, int len)
{
    u16 *fb2 = (u16 *)ptr;

    DEBUGF("%s\n", __func__);

    if (len > FB_SIZE) {
        printf("Invalid frame buffer size.\n");
        return -EINVAL;
    }
    memcpy(fb2, fb, sizeof(u16) * len);

    return 0;
}

/* TODO: Supporting the file pointer(position) */
static int clcd_write(struct file *f, const char *ptr, int len)
{
    u16 *fb2 = (u16 *)ptr;

    DEBUGF("%s, val = 0x%X, size = %d\n", __func__, (u16)*ptr, len);
    if (len > FB_SIZE) {
        printf("Invalid frame buffer size.\n");
        return -EINVAL;
    }
    memcpy(fb, fb2, sizeof(u16) * len);

    return len;
}

static int clcd_ioctl(struct file *file, const unsigned int cmd, void *data)
{
    switch (cmd) {
        case CMD_GET_FBSIZE:
            *((u32 *)data) = FB_SIZE;
            break;
        default:
            break;
    }

    return 0;
}

struct file_op drv_pl110_op = {
    .open	= clcd_open,
    .close	= clcd_close,
    .read	= clcd_read,
    .write	= clcd_write,
    .ioctl  = clcd_ioctl,
};

int clcd_init(void)
{
    u32 pid[4];
    u32 tim[4];
    u32 cntl, i;
    u32 ustart = (u32)fb;
    int fd;

    DEBUGF("PL110 Color LCD initialization.\n");

    /* Find CLCD cell IDs. */
    for (i = 0; i < 4; ++i)
        pid[i] = Io.readl(clcd_base + CLCD_CELLID0 + i * 4) & 0xff;
    assert(pid[0] == 0x0D && pid[1] == 0xF0 && pid[2] == 0x05 && pid[3] == 0xB1);
    DEBUGF("CLCD base address = 0x%X\n", clcd_base);
    DEBUGF("Frame buffer address = 0x%X\n", (u32)fb);

    /* Disable interrupt */
    Io.writel(clcd_base + CLCD_IMSC, 0);

    /* Set the timings */
    tim[0] = ((xvga.mode.xres / 16) - 1) << 2;  /* PPL(Pixel Per Line) */
    tim[0] |= (xvga.mode.hsync_len -1) << 8;    /* HSW(Horizontal Synchronization pulse Width) */
    tim[0] |= (xvga.mode.right_margin - 1) << 16;   /* HFP(Horizontal Front Porch) */
    tim[0] |= (xvga.mode.left_margin - 1) << 24;    /* HBP(Horizontal Back Porch)) */

    tim[1] = xvga.mode.yres - 1;
    tim[1] |= (xvga.mode.vsync_len - 1) << 10;
    tim[1] |= xvga.mode.lower_margin << 16;
    tim[1] |= xvga.mode.upper_margin << 24;

    tim[2] = xvga.tim2;
    tim[2] |= (xvga.mode.xres - 1) << 16; /* TFT */
    tim[2] |= TIM2_IVS | TIM2_IHS;

    tim[3] = xvga.tim3;

    Io.writel(clcd_base + CLCD_TIM0, tim[0]);
    Io.writel(clcd_base + CLCD_TIM1, tim[1]);
    Io.writel(clcd_base + CLCD_TIM2, tim[2]);
    Io.writel(clcd_base + CLCD_TIM3, tim[3]);

    /* Set the frame buffer */
    Io.writel(clcd_base + CLCD_UPBASE, ustart);

    /* Set control register & enable CLCD. */
    cntl = xvga.cntl;
    cntl |= CNTL_LCDBPP16_565;
    Io.writel(clcd_base + CLCD_CTRL, cntl);

    fd = Drv.reg(&drv_pl110_op, "/dev/video");
    DEBUGF("Video file descriptor = %d\n", fd);

    return 0;
}

module_init(clcd_init);
