/* drivers/eth_smc9118.c
 *
 * SMC9118 network driver.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * From: Linux Kernel v3.1 (drivers/net/smsc911x.c)
 *       Copyright (C) 2004-2008 SMSC
 *       Copyright (C) 2005-2008 ARM
 */

#include "eth_smc9118.h"
#include <delay.h>
#include <errno.h>
#include <arch/types.h>
#include <dinos/driver.h>
#include <dinos/init.h>
#include <dinos/mem.h>
#include <mach/board.h>
#include <mach/gic.h>
#include <net/netif.h>

#define CARDNAME	"smc9118"

/* this enables an interrupt in the interrupt mask register */
#define SMC_ENABLE_INT(lp, x) do {		\
	unsigned int  __mask;			\
	__mask = SMC_GET_INT_EN((lp));		\
	__mask |= (x);				\
	SMC_SET_INT_EN((lp), __mask);		\
    } while (0)


static int watchdog = 5000;
static int tx_fifo_kb = 4;
static unsigned short lastTag;

#if 0
static void 
PRINT_PKT(unsigned char *buf, int length)
{
    int i;
    int remainder;
    int lines;

    lines = length / 16;
    remainder = length % 16;

    for (i = 0; i < lines ; i ++) {
        int cur;
        for (cur = 0; cur < 8; cur++) {
            unsigned char a, b;
            a = *buf++;
            b = *buf++;
            DEBUGF("%.2x%.2x ", a, b);
        }
        DEBUGF("\r\n");
    }
    for (i = 0; i < remainder/2 ; i++) {
        unsigned char a, b;
        a = *buf++;
        b = *buf++;
        DEBUGF("%.2x%.2x ", a, b);
    }
    DEBUGF("\r\n");
}
#else
#define PRINT_PKT(x...)	do {} while(0)
#endif

static inline int smc911x_rcv(struct smc911x_local *lp, char *data);

static void 
smc911x_enable(struct smc911x_local *lp)
{
    unsigned mask, cfg, cr;
    unsigned long flags;

    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();

    SMC_SET_MAC_ADDR(lp, lp->mac_addr);

    /* Enable TX */
    cfg = SMC_GET_HW_CFG(lp);
    cfg &= HW_CFG_TX_FIF_SZ_ | 0xFFF;
    cfg |= HW_CFG_SF_;
    SMC_SET_HW_CFG(lp, cfg);
    SMC_SET_FIFO_TDA(lp, 0xFF);
    /* Update TX stats on every 64 packets received or every 1 sec */
    SMC_SET_FIFO_TSL(lp, 64);
    SMC_SET_GPT_CFG(lp, GPT_CFG_TIMER_EN_ | 10000);

    SMC_GET_MAC_CR(lp, cr);
    cr |= MAC_CR_TXEN_ | MAC_CR_HBDIS_;
    SMC_SET_MAC_CR(lp, cr);
    SMC_SET_TX_CFG(lp, TX_CFG_TX_ON_);

    /* Add 2 byte padding to start of packets */
    SMC_SET_RX_CFG(lp, (2<<8) & RX_CFG_RXDOFF_);

    /* Turn on receiver and enable RX */
    if (cr & MAC_CR_RXEN_)
        DEBUGF("%s: Receiver already enabled\r\n", CARDNAME);

    SMC_SET_MAC_CR(lp, cr | MAC_CR_RXEN_);

    /* Interrupt on every received packet */
    SMC_SET_FIFO_RSA(lp, 0x01);
    SMC_SET_FIFO_RSL(lp, 0x00);

    /* now, enable interrupts */
    mask = INT_EN_TDFA_EN_ | INT_EN_TSFL_EN_ | INT_EN_RSFL_EN_ |
        INT_EN_GPT_INT_EN_ | INT_EN_RXDFH_INT_EN_ | INT_EN_RXE_EN_ |
        INT_EN_PHY_INT_EN_;

    if (IS_REV_A(lp->revision))
        mask|=INT_EN_RDFL_EN_;
    else {
        mask|=INT_EN_RDFO_EN_;
    }
    SMC_ENABLE_INT(lp, mask);

    //spin_unlock_irqrestore(&lp->lock, flags);
    Irq.restore(flags);
}

static void 
smc911x_phy_powerdown(struct smc911x_local *lp, int phy)
{
    unsigned int bmcr;

    /* Enter Link Disable state */
    SMC_GET_PHY_BMCR(lp, phy, bmcr);
    bmcr |= BMCR_PDOWN;
    SMC_SET_PHY_BMCR(lp, phy, bmcr);
}

static void 
smc911x_shutdown(struct smc911x_local *lp)
{
    unsigned cr;
    unsigned long flags;

    /* Disable IRQ's */
    SMC_SET_INT_EN(lp, 0);

    /* Turn of Rx and TX */
    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();
    SMC_GET_MAC_CR(lp, cr);
    cr &= ~(MAC_CR_TXEN_ | MAC_CR_RXEN_ | MAC_CR_HBDIS_);
    SMC_SET_MAC_CR(lp, cr);
    SMC_SET_TX_CFG(lp, TX_CFG_STOP_TX_);
    //spin_unlock_irqrestore(&lp->lock, flags);
    Irq.restore(flags);
}

static int 
smc911x_phy_read(struct smc911x_local *lp, int phyaddr, int phyreg)
{
    unsigned int phydata;

    SMC_GET_MII(lp, phyreg, phyaddr, phydata);
    return phydata;
}

static void 
smc911x_phy_write(struct smc911x_local *lp, int phyaddr, int phyreg, int phydata)
{
    SMC_SET_MII(lp, phyreg, phyaddr, phydata);
}

static int 
smc911x_phy_reset(struct smc911x_local *lp)
{
    int timeout;
    unsigned long flags;
    unsigned int reg;

    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();
    reg = SMC_GET_PMT_CTRL(lp);
    reg &= ~0xfffff030;
    reg |= PMT_CTRL_PHY_RST_;
    SMC_SET_PMT_CTRL(lp, reg);
    //spin_unlock_irqrestore(&lp->lock, flags);
    Irq.restore(flags);

    for (timeout = 2; timeout; timeout--) {
        //msleep(50);
        mdelay(50);
        //spin_lock_irqsave(&lp->lock, flags);
        flags = Irq.save();
        reg = SMC_GET_PMT_CTRL(lp);
        //spin_unlock_irqrestore(&lp->lock, flags);
        Irq.restore(flags);
        if (!(reg & PMT_CTRL_PHY_RST_)) {
            /* extra delay required because the phy may
             * not be completed with its reset
             * when PHY_BCR_RESET_ is cleared. 256us
             * should suffice, but use 500us to be safe
             */
            udelay(500);
            break;
        }
    }

    return reg & PMT_CTRL_PHY_RST_;
}

static void 
smc911x_phy_check_media(struct smc911x_local *lp, int init)
{
    int phyaddr = lp->mii.phy_id;
    unsigned int bmcr, cr;

    //if (mii_check_media(&lp->mii, netif_msg_link(lp), init)) {
    /* duplex state has changed */
    SMC_GET_PHY_BMCR(lp, phyaddr, bmcr);
    SMC_GET_MAC_CR(lp, cr);
    if (lp->mii.full_duplex) {
	bmcr |= BMCR_FULLDPLX;
	cr |= MAC_CR_RCVOWN_;
    } else {
	bmcr &= ~BMCR_FULLDPLX;
	cr &= ~MAC_CR_RCVOWN_;
    }
    SMC_SET_PHY_BMCR(lp, phyaddr, bmcr);
    SMC_SET_MAC_CR(lp, cr);
    //}
}

static void 
smc911x_phy_configure(struct smc911x_local *lp)
{
    int phyaddr = lp->mii.phy_id;
    int my_phy_caps; /* My PHY capabilities */
    int my_ad_caps; /* My Advertised capabilities */
    int status;
    unsigned long flags;

    /*
     * We should not be called if phy_type is zero.
     */
    if (lp->phy_type == 0)
        return;

    if (smc911x_phy_reset(lp)) {
        DEBUGF("%s: PHY reset timed out\r\n", CARDNAME);
        return;
    }
    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();

    /*
     * Enable PHY Interrupts (for register 18)
     * Interrupts listed here are enabled
     */
    SMC_SET_PHY_INT_MASK(lp, phyaddr, PHY_INT_MASK_ENERGY_ON_ |
            PHY_INT_MASK_ANEG_COMP_ | PHY_INT_MASK_REMOTE_FAULT_ |
            PHY_INT_MASK_LINK_DOWN_);

#if 0 /* This routine is used case of do not auto neg function. */
    /* If the user requested no auto neg, then go set his request */
    if (lp->mii.force_media) {
        smc911x_phy_fixed(pktsdev);
        goto smc911x_phy_configure_exit;
    }
#endif

    /* Copy our capabilities from MII_BMSR to MII_ADVERTISE */
    SMC_GET_PHY_BMSR(lp, phyaddr, my_phy_caps);
    if (!(my_phy_caps & BMSR_ANEGCAPABLE)) {
        DEBUGF("Auto negotiation NOT supported\r\n");
        //smc911x_phy_fixed(dev);
        goto smc911x_phy_configure_exit;
    }

    /* CSMA capable w/ both pauses */
    my_ad_caps = ADVERTISE_CSMA | ADVERTISE_PAUSE_CAP | ADVERTISE_PAUSE_ASYM;

    if (my_phy_caps & BMSR_100BASE4)
        my_ad_caps |= ADVERTISE_100BASE4;
    if (my_phy_caps & BMSR_100FULL)
        my_ad_caps |= ADVERTISE_100FULL;
    if (my_phy_caps & BMSR_100HALF)
        my_ad_caps |= ADVERTISE_100HALF;
    if (my_phy_caps & BMSR_10FULL)
        my_ad_caps |= ADVERTISE_10FULL;
    if (my_phy_caps & BMSR_10HALF)
        my_ad_caps |= ADVERTISE_10HALF;

    /* Disable capabilities not selected by our user */
    if (lp->ctl_rspeed != 100)
        my_ad_caps &= ~(ADVERTISE_100BASE4|ADVERTISE_100FULL|ADVERTISE_100HALF);

    if (!lp->ctl_rfduplx)
        my_ad_caps &= ~(ADVERTISE_100FULL|ADVERTISE_10FULL);

    /* Update our Auto-Neg Advertisement Register */
    SMC_SET_PHY_MII_ADV(lp, phyaddr, my_ad_caps);
    lp->mii.advertising = my_ad_caps;

    /*
     * Read the register back.	 Without this, it appears that when
     * auto-negotiation is restarted, sometimes it isn't ready and
     * the link does not come up.
     */
    udelay(10);
    SMC_GET_PHY_MII_ADV(lp, phyaddr, status);

    DEBUGF("%s: phy caps = 0x%X\r\n", CARDNAME, my_phy_caps);
    DEBUGF("%s: phy advertised caps = 0x%X\r\n", CARDNAME, my_ad_caps);

    /* Restart auto-negotiation process in order to advertise my caps */
    SMC_SET_PHY_BMCR(lp, phyaddr, BMCR_ANENABLE | BMCR_ANRESTART);

    smc911x_phy_check_media(lp, 1);

smc911x_phy_configure_exit:
    //spin_unlock_irqrestore(&lp->lock, flags);
    Irq.restore(flags);
}

static void 
smc911x_reset(struct smc911x_local *lp)
{
    unsigned int reg, timeout=0, resets=1, irq_cfg;
    unsigned long flags;

    /*	 Take out of PM setting first */
    if ((SMC_GET_PMT_CTRL(lp) & PMT_CTRL_READY_) == 0) {
        /* Write to the bytetest will take out of powerdown */
        SMC_SET_BYTE_TEST(lp, 0);
        timeout=10;
        do {
            udelay(10);
            reg = SMC_GET_PMT_CTRL(lp) & PMT_CTRL_READY_;
        } while (--timeout && !reg);
        if (timeout == 0) {
            DEBUGF("smc911x_reset timeout waiting for PM restore\r\n");
            return;
        }
    }

    /* Disable all interrupts */
    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();
    SMC_SET_INT_EN(lp, 0);
    Irq.restore(flags);
    //spin_unlock_irqrestore(&lp->lock, flags);
    lp->tx_throttle = 0;

    while (resets--) {
        SMC_SET_HW_CFG(lp, HW_CFG_SRST_);
        timeout=10;
        do {
            udelay(10);
            reg = SMC_GET_HW_CFG(lp);
            /* If chip indicates reset timeout then try again */
            if (reg & HW_CFG_SRST_TO_) {
                DEBUGF("smc911x chip reset timeout, retrying...\r\n");
                resets++;
                break;
            }
        } while (--timeout && (reg & HW_CFG_SRST_));
    }

    if (timeout == 0) {
        DEBUGF("smc911x_reset timeout waiting for reset\r\n");
        return;
    }

    /* make sure EEPROM has finished loading before setting GPIO_CFG */
    timeout=1000;
    while (--timeout && (SMC_GET_E2P_CMD(lp) & E2P_CMD_EPC_BUSY_))
        udelay(10);

    if (timeout == 0){
        DEBUGF("smc911x_reset timeout waiting for EEPROM busy\r\n");
        return;
    }

    /* Initialize interrupts */
    SMC_SET_INT_EN(lp, 0);
    SMC_ACK_INT(lp, -1);

    /* Reset the FIFO level and flow control settings */
    SMC_SET_HW_CFG(lp, (lp->tx_fifo_kb & 0xF) << 16);
    //TODO: Figure out what appropriate pause time is
    SMC_SET_FLOW(lp, FLOW_FCPT_ | FLOW_FCEN_);
    SMC_SET_AFC_CFG(lp, lp->afc_cfg);

    /* Set to LED outputs */
    //SMC_SET_GPIO_CFG(lp, 0x70070000);

    /*
     * Deassert IRQ for 1*10us for edge type interrupts
     * and drive IRQ pin push-pull
     */
    irq_cfg = (1 << 24) | INT_CFG_IRQ_EN_ | INT_CFG_IRQ_TYPE_;
    SMC_SET_IRQ_CFG(lp, irq_cfg);

    /* clear anything saved */
    if (lp->pending_tx_pkb != NULL) {
        //mm->free(lp->pending_tx_pkb);
        lp->pending_tx_pkb = NULL;
    }
}

static void 
smc911x_phy_detect(struct smc911x_local *lp)
{
    int phyaddr;
    unsigned int cfg, id1, id2;

    lp->phy_type = 0;

    /*
     * Scan all 32 PHY addresses if necessary, starting at
     * PHY#1 to PHY#31, and then PHY#0 last.
     */
    switch(lp->version) {
        case CHIP_9115:
        case CHIP_9117:
        case CHIP_9215:
        case CHIP_9217:
            cfg = SMC_GET_HW_CFG(lp);
            if (cfg & HW_CFG_EXT_PHY_DET_) {
                cfg &= ~HW_CFG_PHY_CLK_SEL_;
                cfg |= HW_CFG_PHY_CLK_SEL_CLK_DIS_;
                SMC_SET_HW_CFG(lp, cfg);
                udelay(10); /* Wait for clocks to stop */

                cfg |= HW_CFG_EXT_PHY_EN_;
                SMC_SET_HW_CFG(lp, cfg);
                udelay(10); /* Wait for clocks to stop */

                cfg &= ~HW_CFG_PHY_CLK_SEL_;
                cfg |= HW_CFG_PHY_CLK_SEL_EXT_PHY_;
                SMC_SET_HW_CFG(lp, cfg);
                udelay(10); /* Wait for clocks to stop */

                cfg |= HW_CFG_SMI_SEL_;
                SMC_SET_HW_CFG(lp, cfg);

                for (phyaddr = 1; phyaddr < 32; ++phyaddr) {

                    /* Read the PHY identifiers */
                    SMC_GET_PHY_ID1(lp, phyaddr & 31, id1);
                    SMC_GET_PHY_ID2(lp, phyaddr & 31, id2);

                    /* Make sure it is a valid identifier */
                    if (id1 != 0x0000 && id1 != 0xffff &&
                            id1 != 0x8000 && id2 != 0x0000 &&
                            id2 != 0xffff && id2 != 0x8000) {
                        /* Save the PHY's address */
                        lp->mii.phy_id = phyaddr & 31;
                        lp->phy_type = id1 << 16 | id2;
                        break;
                    }
                }
                if (phyaddr < 32)
                    /* Found an external PHY */
                    break;
            }
        default:
            /* Internal media only */
            SMC_GET_PHY_ID1(lp, 1, id1);
            SMC_GET_PHY_ID2(lp, 1, id2);
            /* Save the PHY's address */
            lp->mii.phy_id = 1;
            lp->phy_type = id1 << 16 | id2;
    }

    DEBUGF("%s: phy_id1=0x%x, phy_id2=0x%x phyaddr=0x%d\r\n",
            CARDNAME, id1, id2, lp->mii.phy_id);
}



static void 
smc911x_phy_interrupt(struct smc911x_local *lp)
{
    int phyaddr = lp->mii.phy_id;
    int status;

    if (lp->phy_type == 0)
        return;

    smc911x_phy_check_media(lp, 0);
    /* read to clear status bits */
    SMC_GET_PHY_INT_SRC(lp, phyaddr,status);
}

static void 
smc911x_tx(struct smc911x_local *lp)
{
    unsigned int tx_status;

    /* Collect the TX status */
    while (((SMC_GET_TX_FIFO_INF(lp) & TX_FIFO_INF_TSUSED_) >> 16) != 0) {
        tx_status = SMC_GET_TX_STS_FIFO(lp);
        //dev->stats.tx_packets++;
        //dev->stats.tx_bytes+=tx_status>>16;

        /* count Tx errors, but ignore lost carrier errors when in
         * full-duplex mode */
        if ((tx_status & TX_STS_ES_) && !(lp->ctl_rfduplx &&
                    !(tx_status & 0x00000306))) {
            //dev->stats.tx_errors++;
        }
        if (tx_status & TX_STS_MANY_COLL_) {
            //dev->stats.collisions+=16;
            //dev->stats.tx_aborted_errors++;
        } else {
            //dev->stats.collisions+=(tx_status & TX_STS_COLL_CNT_) >> 3;
        }
        /* carrier error only has meaning for half-duplex communication */
        if ((tx_status & (TX_STS_LOC_ | TX_STS_NO_CARR_)) &&
                !lp->ctl_rfduplx) {
            //dev->stats.tx_carrier_errors++;
        }
        if (tx_status & TX_STS_LATE_COLL_) {
            //dev->stats.collisions++;
            //dev->stats.tx_aborted_errors++;
        }
    }
}

/* SMC LAN9118 Interrupt Handler. */
static void
smc911x_interrupt(void *arg)
{
#if 1
    struct smc911x_local *lp = (struct smc911x_local *)arg;
    unsigned int status, mask, timeout;
    unsigned int rx_overrun=0, cr, pkts;
    unsigned long flags;

    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();

    /* Spurious interrupt check */
    if ((SMC_GET_IRQ_CFG(lp) & (INT_CFG_IRQ_INT_ | INT_CFG_IRQ_EN_)) !=
            (INT_CFG_IRQ_INT_ | INT_CFG_IRQ_EN_)) {
        Irq.restore(flags);
        return;
    }
    DEBUGF("eth int\r\n");

    mask = SMC_GET_INT_EN(lp);
    SMC_SET_INT_EN(lp, 0);

    /* set a timeout value, so I don't stay here forever */
    timeout = 8;

    do {
        status = SMC_GET_INT(lp);
        status &= mask;
        if (!status)
            break;

        /* Handle SW interrupt condition */
        if (status & INT_STS_SW_INT_) {
            DEBUGF("STS\r\n");
            SMC_ACK_INT(lp, INT_STS_SW_INT_);
            mask &= ~INT_EN_SW_INT_EN_;
        }

        /* Handle various error conditions */
        if (status & INT_STS_RXE_) {
            DEBUGF("ERR\r\n");
            SMC_ACK_INT(lp, INT_STS_RXE_);
            //dev->stats.rx_errors++;
        }
        if (status & INT_STS_RXDFH_INT_) {
            DEBUGF("RX DFH\r\n");
            SMC_ACK_INT(lp, INT_STS_RXDFH_INT_);
            //dev->stats.rx_dropped+=SMC_GET_RX_DROP(lp);
        }

        /* Undocumented interrupt-what is the right thing to do here? */
        if (status & INT_STS_RXDF_INT_) {
            DEBUGF("RX DF\r\n");
            SMC_ACK_INT(lp, INT_STS_RXDF_INT_);
        }

        /* Rx Data FIFO exceeds set level */
        if (status & INT_STS_RDFL_) {
            DEBUGF("STS RDFL\r\n");
            if (IS_REV_A(lp->revision)) {
                rx_overrun=1;
                SMC_GET_MAC_CR(lp, cr);
                cr &= ~MAC_CR_RXEN_;
                SMC_SET_MAC_CR(lp, cr);
                //dev->stats.rx_errors++;
                //dev->stats.rx_fifo_errors++;
            }
            SMC_ACK_INT(lp, INT_STS_RDFL_);
        }

        if (status & INT_STS_RDFO_) {
            DEBUGF("RDFO\r\n");
            if (!IS_REV_A(lp->revision)) {
                SMC_GET_MAC_CR(lp, cr);
                cr &= ~MAC_CR_RXEN_;
                SMC_SET_MAC_CR(lp, cr);
                rx_overrun=1;
                //dev->stats.rx_errors++;
                //dev->stats.rx_fifo_errors++;
            }
            SMC_ACK_INT(lp, INT_STS_RDFO_);
        }

        /* Handle receive condition */
        if ((status & INT_STS_RSFL_) || rx_overrun) {
            unsigned int fifo;
            DEBUGF("RSFL\r\n");
            fifo = SMC_GET_RX_FIFO_INF(lp);
            pkts = (fifo & RX_FIFO_INF_RXSUSED_) >> 16;
            if (pkts != 0) {
                //smc911x_rcv(dev);
            }
            SMC_ACK_INT(lp, INT_STS_RSFL_);
        }

        /* Handle transmit FIFO available */
        if (status & INT_STS_TDFA_) {
            DEBUGF("TDFA\r\n");
            SMC_SET_FIFO_TDA(lp, 0xFF);
            lp->tx_throttle = 0;
            //netif_wake_queue(dev); <- TODO: Enable transmition state.
            SMC_ACK_INT(lp, INT_STS_TDFA_);
        }

        /* Handle transmit done condition */
        if (status & (INT_STS_TSFL_ | INT_STS_GPT_INT_)) {
            DEBUGF("TSFL\r\n");
            smc911x_tx(lp);
            SMC_SET_GPT_CFG(lp, GPT_CFG_TIMER_EN_ | 10000);
            SMC_ACK_INT(lp, INT_STS_TSFL_);
            SMC_ACK_INT(lp, INT_STS_TSFL_ | INT_STS_GPT_INT_);
        }

        /* Handle PHY interrupt condition */
        if (status & INT_STS_PHY_INT_) {
            DEBUGF("PHY\r\n");
            smc911x_phy_interrupt(lp);
            SMC_ACK_INT(lp, INT_STS_PHY_INT_);
        }
    } while (--timeout);

    /* restore mask state */
    SMC_SET_INT_EN(lp, mask);

    //spin_unlock_irqrestore(&lp->lock, flags);
    Irq.restore(flags);
#endif
}

static int 
smc911x_probe(struct smc911x_local *lp)
{
    int i, retval=0;
    unsigned int val, chip_id, revision;
    const char *version_string;

    /* First, see if the endian word is recognized */
    val = SMC_GET_BYTE_TEST(lp);
    DEBUGF("%s: endian probe returned 0x%X\r\n", CARDNAME, val);
    if (val != 0x87654321) {
        DEBUGF("Invalid chip endian 0x%X\r\n", val);
        retval = -ENODEV;
        goto err_out;
    }

    /*
     * check if the revision register is something that I
     * recognize.	These might need to be added to later,
     * as future revisions could be added.
     */
    chip_id = SMC_GET_PN(lp);
    DEBUGF("%s: id probe returned 0x%X\r\n", CARDNAME, chip_id);

    for(i=0;chip_ids[i].id != 0; i++) {
        if (chip_ids[i].id == chip_id) break;
    }

    if (!chip_ids[i].id) {
        DEBUGF("Unknown chip ID 0x%X\r\n", chip_id);
        retval = -ENODEV;
        goto err_out;
    }
    version_string = chip_ids[i].name;

    revision = SMC_GET_REV(lp);
    DEBUGF("%s: revision = 0x%X\r\n", CARDNAME, revision);

    /* At this point I'll assume that the chip is an SMC911x. */
    DEBUGF("%s: Found a %s\r\n", CARDNAME, chip_ids[i].name);

    /* Validate the TX FIFO size requested */
    if ((tx_fifo_kb < 2) || (tx_fifo_kb > 14)) {
        DEBUGF("Invalid TX FIFO size requested %d\r\n", tx_fifo_kb);
        retval = -EINVAL;
        goto err_out;
    }

    /* fill in some of the fields */
    lp->version = chip_ids[i].id;
    lp->revision = revision;
    lp->tx_fifo_kb = tx_fifo_kb;
    /* Reverse calculate the RX FIFO size from the TX */
    lp->tx_fifo_size=(lp->tx_fifo_kb<<10) - 512;
    lp->rx_fifo_size= ((0x4000 - 512 - lp->tx_fifo_size) / 16) * 15;

    /* Set the automatic flow control values */
    switch(lp->tx_fifo_kb) {
        /*
         *	 AFC_HI is about ((Rx Data Fifo Size)*2/3)/64
         *	 AFC_LO is AFC_HI/2
         *	 BACK_DUR is about 5uS*(AFC_LO) rounded down
         */
        case 2:/* 13440 Rx Data Fifo Size */
            lp->afc_cfg=0x008C46AF;break;
        case 3:/* 12480 Rx Data Fifo Size */
            lp->afc_cfg=0x0082419F;break;
        case 4:/* 11520 Rx Data Fifo Size */
            lp->afc_cfg=0x00783C9F;break;
        case 5:/* 10560 Rx Data Fifo Size */
            lp->afc_cfg=0x006E374F;break;
        case 6:/* 9600 Rx Data Fifo Size */
            lp->afc_cfg=0x0064328F;break;
        case 7:/* 8640 Rx Data Fifo Size */
            lp->afc_cfg=0x005A2D7F;break;
        case 8:/* 7680 Rx Data Fifo Size */
            lp->afc_cfg=0x0050287F;break;
        case 9:/* 6720 Rx Data Fifo Size */
            lp->afc_cfg=0x0046236F;break;
        case 10:/* 5760 Rx Data Fifo Size */
            lp->afc_cfg=0x003C1E6F;break;
        case 11:/* 4800 Rx Data Fifo Size */
            lp->afc_cfg=0x0032195F;break;
            /*
             *	 AFC_HI is ~1520 bytes less than RX Data Fifo Size
             *	 AFC_LO is AFC_HI/2
             *	 BACK_DUR is about 5uS*(AFC_LO) rounded down
             */
        case 12:/* 3840 Rx Data Fifo Size */
            lp->afc_cfg=0x0024124F;break;
        case 13:/* 2880 Rx Data Fifo Size */
            lp->afc_cfg=0x0015073F;break;
        case 14:/* 1920 Rx Data Fifo Size */
            lp->afc_cfg=0x0006032F;break;
        default:
            DEBUGF("%s: ERROR -- no AFC_CFG setting found",
                    CARDNAME);
            break;
    }

    //spin_lock_init(&lp->lock);

    /* Get the MAC address */
    SMC_GET_MAC_ADDR(lp, lp->mac_addr);
    DEBUGF("%s: mac addr = 0x%X\r\n", CARDNAME, lp->mac_addr);

    /* now, reset the chip, and put it into a known state */
    smc911x_reset(lp);
    lp->irq = ETH_IRQ;

    lp->mii.phy_id_mask = 0x1f;
    lp->mii.reg_num_mask = 0x1f;
    lp->mii.force_media = 0;
    lp->mii.full_duplex = 0;
    //lp->mii.mdio_read = smc911x_phy_read;
    //lp->mii.mdio_write = smc911x_phy_write;

    /*
     * Locate the phy, if any.
     */
    smc911x_phy_detect(lp);

    /* Set default parameters */
    //lp->msg_enable = NETIF_MSG_LINK;
    lp->ctl_rfduplx = 1;
    lp->ctl_rspeed = 100;

    /* Grab the IRQ */
    Irq.request(lp->irq, "SMC9118", smc911x_interrupt, lp);

err_out:
    return retval;
}


static int
eth_open(struct file *file)
{
    int ret;

    struct smc911x_local *lp = Mem.alloc(sizeof(struct smc911x_local), 0);
    lastTag = 0;
    lp->base = ETH_BASE; /* Set base address of ethernet registers. */

    ret = smc911x_probe(lp);
    if (ret < 0) 
        return ret;
    smc911x_reset(lp);
    smc911x_phy_configure(lp);
    smc911x_enable(lp);

    file->arg = (void *)lp;

    /* Enable interrupt(Core). */
    /* Enable interrupt(EB). */
    //Io.writel(sys_base + REALVIEW_EB_GIC_DIST_OFFSET + GIC_DIST_ENABLE_SET + 4, 1<<28);

    return 0;
}

static int
eth_close(struct file *file)
{
    struct smc911x_local *lp = (struct smc911x_local*)file->arg;

    //netif_stop_queue(dev);
    //netif_carrier_off(dev);

    /* clear everything */
    smc911x_shutdown(lp);

    if (lp->phy_type != 0) {
        /* We need to ensure that no calls to
         * smc911x_phy_configure are pending.
         */
        //cancel_work_sync(&lp->phy_configure);
        smc911x_phy_powerdown(lp, lp->mii.phy_id);
    }

    if (lp->pending_tx_pkb) {
        //mm->free(lp->pending_tx_pkb);
        lp->pending_tx_pkb = NULL;
    }

    return 0;
}

static inline void 
smc911x_drop_pkt(struct smc911x_local *lp)
{
    unsigned int fifo_count, timeout, reg;

    fifo_count = SMC_GET_RX_FIFO_INF(lp) & 0xFFFF;
    if (fifo_count <= 4) {
        /* Manually dump the packet data */
        while (fifo_count--)
            SMC_GET_RX_FIFO(lp);
    } else	 {
        /* Fast forward through the bad packet */
        SMC_SET_RX_DP_CTRL(lp, RX_DP_CTRL_FFWD_BUSY_);
        timeout=50;
        do {
            udelay(10);
            reg = SMC_GET_RX_DP_CTRL(lp) & RX_DP_CTRL_FFWD_BUSY_;
        } while (--timeout && reg);

        if (timeout == 0) {
            DEBUGF("%s: timeout waiting for RX fast forward\r\n", CARDNAME);
        }
    }
}

static inline int
smc911x_rcv(struct smc911x_local *lp, char *data)
{
    unsigned int pkt_len, status;

    /*
       DBG(SMC_DEBUG_FUNC | SMC_DEBUG_RX, "%s: --> %s\n",
       dev->name, __func__);
       */
    status = SMC_GET_RX_STS_FIFO(lp);
    /*
       DBG(SMC_DEBUG_RX, "%s: Rx pkt len %d status 0x%08x\n",
       dev->name, (status & 0x3fff0000) >> 16, status & 0xc000ffff);
       */
    pkt_len = (status & RX_STS_PKT_LEN_) >> 16;
    if (status & RX_STS_ES_) {
        /* Deal with a bad packet */
        /*
           dev->stats.rx_errors++;
           if (status & RX_STS_CRC_ERR_)
           dev->stats.rx_crc_errors++;
           else {
           if (status & RX_STS_LEN_ERR_)
           dev->stats.rx_length_errors++;
           if (status & RX_STS_MCAST_)
           dev->stats.multicast++;
           }
           */
        /* Remove the bad packet data from the RX FIFO */
        smc911x_drop_pkt(lp);
    } else {
        /* Receive a valid packet */
        /* Alloc a buffer with extra room for DMA alignment */
#if 0
        pkb->payload = Mem.alloc(pkt_len+32);
        if (pkb->payload == NULL) {
            DEBUGF( "%s: Low memory, rcvd packet dropped.\r\n", CARDNAME);
            //dev->stats.rx_dropped++;
            smc911x_drop_pkt(lp);
            return;
        }
        /* Align IP header to 32 bits
         * Note that the device is configured to add a 2
         * byte padding to the packet start, so we really
         * want to write to the orignal data pointer */
        data = pkb->payload;
        //skb_reserve(skb, 2);
        //skb_put(skb,pkt_len-4);
#endif

        SMC_SET_RX_CFG(lp, RX_CFG_RX_END_ALGN4_ | ((2<<8) & RX_CFG_RXDOFF_));
        SMC_PULL_DATA(lp, data, pkt_len+2+3);

        PRINT_PKT(data, ((pkt_len - 4) <= 64) ? pkt_len - 4 : 64);
        //skb->protocol = eth_type_trans(skb, dev);
        //netif_rx(skb); /* TODO: 상위 프로토콜에 올려 줄것! */
        //dev->stats.rx_packets++;
        //dev->stats.rx_bytes += pkt_len-4;
        //mm->free(pkb->payload);
    }
    return pkt_len;
}

static int
eth_read(struct file *file, char *ptr, int len)
{
    struct smc911x_local *lp = (struct smc911x_local *)file->arg;
    unsigned int pkts;
    unsigned int fifo;
    int ret = 0;

    fifo = SMC_GET_RX_FIFO_INF(lp);
    pkts = (fifo & RX_FIFO_INF_RXSUSED_) >> 16;
    if (pkts != 0 )
        ret = smc911x_rcv(lp, ptr);
    return ret;
}

/*
 * This is called to actually send a packet to the chip.
 */
static void 
smc911x_hardware_send_pkt(struct smc911x_local *lp)
{
    struct pbuf *pkb;
    unsigned int cmdA, cmdB, len;
    unsigned char *buf;

    pkb = lp->pending_tx_pkb;
    lp->pending_tx_pkb = NULL;
    ++lastTag;

    /* cmdA {25:24] data alignment [20:16] start offset [10:0] buffer length */
    /* cmdB {31:16] pkt tag [10:0] length */
    buf = (char*)((unsigned int)pkb->payload & ~0x3);
    len = (pkb->len + 3 + ((unsigned int)pkb->payload & 3)) & ~0x3;
    cmdA = (((unsigned int)pkb->payload & 0x3) << 16) |
        //TX_CMD_A_INT_FIRST_SEG_ | TX_CMD_A_INT_LAST_SEG_ |
        0x00003000|
        pkb->len;

    /* tag is packet length so we can use this in stats update later */
    cmdB = (pkb->len  << 16) | (pkb->len & 0x7FF);

    SMC_SET_TX_FIFO(lp, cmdA);
    SMC_SET_TX_FIFO(lp, cmdB);

    PRINT_PKT(buf, len <= 64 ? len : 64);

    /* Send pkt via PIO */
    SMC_PUSH_DATA(lp, buf, len);
    //dev->trans_start = jiffies;
    //dev_kfree_skb_irq(skb);
    //mm->free(pkb->payload);

    if (!lp->tx_throttle) {
        //netif_wake_queue();
    }
    SMC_SET_TX_CFG(lp, TX_CFG_TX_ON_);
    //SMC_ENABLE_INT(lp, INT_EN_TDFA_EN_ | INT_EN_TSFL_EN_);
}

static int 
smc911x_hard_start_xmit(struct pbuf *pkb, struct smc911x_local *lp)
{
    unsigned int free;
    unsigned long flags;

    //spin_lock_irqsave(&lp->lock, flags);
    flags = Irq.save();
    free = SMC_GET_TX_FIFO_INF(lp) & TX_FIFO_INF_TDFREE_;

    /* Turn off the flow when running out of space in FIFO */
    if (free <= SMC911X_TX_FIFO_LOW_THRESHOLD) {
        DEBUGF("%s: Disabling data flow due to low FIFO space (%d)\r\n",
                CARDNAME, free);
        /* Reenable when at least 1 packet of size MTU present */
        SMC_SET_FIFO_TDA(lp, (SMC911X_TX_FIFO_LOW_THRESHOLD)/64);
        lp->tx_throttle = 1;
    }

    /* Drop packets when we run out of space in TX FIFO
     * Account for overhead required for:
     *
     *	  Tx command words			 8 bytes
     *	  Start offset				 15 bytes
     *	  End padding				 15 bytes
     */
    if (free < (pkb->len + 8 + 15 + 15)) {
        DEBUGF("%s: No Tx free space %d < %d\r\n",
                CARDNAME, free, pkb->len);
        lp->pending_tx_pkb = NULL;
        //dev->stats.tx_errors++;
        //dev->stats.tx_dropped++;
        //spin_unlock_irqrestore(&lp->lock, flags);
        Irq.restore(flags);
        //mm->free(pkb->payload);
        return 0;
    }

    lp->pending_tx_pkb = pkb;
    smc911x_hardware_send_pkt(lp);
    //spin_unlock_irqrestore(&lp->lock, flags);
    Irq.restore(flags);

    return 0;
}

static int
eth_write(struct file *file, const char *ptr, int len)
{
    struct smc911x_local *lp = (struct smc911x_local *)file->arg;
    struct pbuf pkt;
    unsigned int status;

    pkt.payload = (void *)ptr;
    pkt.len = len;

    smc911x_hard_start_xmit(&pkt, lp);
    while (1) {
        if ((SMC_GET_TX_FIFO_INF(lp) & TX_FIFO_INF_TSUSED_) != 0) {
            unsigned int tx_sts = SMC_GET_TX_STS_FIFO(lp);
            if (tx_sts & TX_STS_ES_) { /* send error. */
#if 0
                DEBUGF("eth send: error status: 0x%0.8x\r\n", tx_sts);
#endif
                return -1;
            } else {
                SMC_SET_TX_CFG(lp, TX_CFG_STOP_TX_);
                return len;	/* successful send */
            }
        }
    }
    return 0;
}

static int
eth_ioctl(struct file *file, const unsigned int cmd, void *data)
{
    struct smc911x_local *lp = (struct smc911x_local *)file->arg;

    switch (cmd) {
        case CMD_GET_HWADDR:
            memcpy((unsigned char*)data, lp->mac_addr, sizeof(lp->mac_addr));
            break;
deafult:
            break;
    }
    return 0;
}

struct file_op drv_smc9118 = {
    .open	= eth_open,
    .close	= eth_close,
    .read	= eth_read,
    .write	= eth_write,
    .ioctl	= eth_ioctl,
};

int eth_init(void)
{
    int fd;
    DEBUGF("SMC9118 Ethernet initialization.\n");
    DEBUGF("SMC9118 Ethernet base address = 0x%X.\n", ETH_BASE);
    fd = Drv.reg(&drv_smc9118, "/dev/eth");
    DEBUGF("Ethernet file descriptor = %d\n", fd);

    return 0;
}

module_init(eth_init);
