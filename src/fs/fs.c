/* fs/fs.c
 *
 * File System interface module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <string.h>
#include <errno.h>
#include <config.h>
#include <dinos/file.h>
#include <dinos/thread.h>

static struct file *files[CFG_MAX_FILES];
int file_count;

static int
vfs_init(void)
{
	file_count = 0;
	return 0;
}

/**
 * @brief		전역 파일 테이블에 파일 추가.
 * @param[in]	file 추가 할 파일.
 * @return		추가된 파일의 file descriptor.
 */
static int
vfs_add(struct file *file)
{
	int fd = file_count++;
	files[fd] = file;
	return fd;
}

static int
vfs_open(const char *name, int flags, int mode)
{
	int i, ret = 0;
	struct file *f;
	struct thread *t = Thread.cur();

	/*
	 * Find file from name.
	 * TODO: Optimize search algorithm.
	 */
	for (i = 0; i < CFG_MAX_FILES; ++i) {
		if (!strncmp(files[i]->name, name, strlen(name)))
			break;
	}

	/* Not found file. */
	if (i >= CFG_MAX_FILES)
		return -ENOENT;
	f = files[i];
	files[i]->ref_count++;

#if 0 /* TODO: open routine. */
	ret = task_m.open(t, f);
	if (ret < 0)
		return ret;
#endif 

	ret = f->f_op->open(f);
	if (ret >= 0)
		ret = i;
	return ret;
}

static int
vfs_close(int fd)
{
	int ret = 0;
	struct file *f = files[fd];
	struct thread *t = Thread.cur();

	if (!f->ref_count)
		return -EBADF;

	f->ref_count--;
#if 0 /* TODO: close routine. */
	ret = task_m.close(t, f);
#endif
	if (ret < 0)
		return ret;

	return f->f_op->close(f);
}

static int
vfs_read(int fd, char *ptr, int len)
{
	struct file *f = files[fd];
	return f->f_op->read(f, ptr, len);
}

static int
vfs_write(int fd, const char *ptr, int len)
{
	struct file *f = files[fd];
	return f->f_op->write(f, ptr, len);
}

static struct file *
vfs_get(int fd)
{
	return files[fd];
}

static int
vfs_ioctl(int fd, const unsigned int cmd, void *data)
{
	struct file *f = files[fd];
	return f->f_op->ioctl(f, cmd, data);
}

struct Vfs Vfs = {
	.init = vfs_init,
	.add = vfs_add,
	.get = vfs_get,
	.open = vfs_open,
	.close = vfs_close,
	.read = vfs_read,
	.write = vfs_write,
	.ioctl = vfs_ioctl,
};
