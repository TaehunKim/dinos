# Compiler options
CFLAGS += -mcpu=cortex-a9

# Linker script variables
KERNEL_START_ADDR = 0x60000000
PAGE_SIZE = 4096
export KERNEL_START_ADDR PAGE_SIZE

# Build configs for odroid-pc board.
CONFIG_SERIAL_S5P=y
