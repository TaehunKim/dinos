/* mach/odroid-pc/lib.c
 *
 * Board dependent libraries.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dinos/io.h>
#include <arch/types.h>
#include <mach/map-exynos4.h>

const int    DR    = 0x00;
const int    FR    = 0x18;
const int    IBRD  = 0x24;
const int    FBRD  = 0x28;
const int    LCR_H = 0x2c;
const int    CR    = 0x30;
const int    IFLS  = 0x34;
const int    IMSC  = 0x38;
const int    MIS   = 0x40;
const int    ICR   = 0x44;

volatile unsigned int *uart=(volatile unsigned int*)0x10009000;

void board_udelay(unsigned int usec)
{
}

static int serial_err_check(int op)
{
    u32 mask;

    if (op)
        mask = 0x8;
    else
        mask = 0xf;

    return *((volatile u32 *)(S5P_PA_UART1 + 0x14)) & mask;
}

void putcc(char c)
{
    uart[DR] = c;
    while ((uart[FR] & (1 << 7)));
}

int board_print(const char *s, int len)
{
    while (*s)
       putcc(*(s++)); 
    return len;
}
