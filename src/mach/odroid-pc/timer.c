/* mach/odriod-pc/timer.c
 *
 * Timer module for realview-eb-mpcore board.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dinos/timer.h>

#define MODULE "Timer"

static int timer_base = 0;

static void arch_timer_init(void)
{
}

static void timer_isr(void *data)
{
}

static int timer_init(void)
{
    return 0;
}

static int timer_start(void)
{
}

static int timer_stop(void)
{
    /* Stop timer. */	
}

struct timer timer = {
    .init	= timer_init,
    .start	= timer_start,
    .stop	= timer_stop,
};
