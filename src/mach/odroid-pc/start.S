/* mach/odroid-pc/start.S
 *
 * Entry point for ODROID-PC board.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/* Entry point */
.global	start
.type	start, %function
.section	.init
.align	4
/*
vector:
    ldr	pc, =ex_reset
	ldr	pc, =ex_undefined
	ldr	pc, =ex_swi
	ldr	pc, =ex_prefetch_abort
	ldr	pc, =ex_data_abort
	nop
	ldr	pc, =ex_irq
	ldr	pc, =ex_irq
*/

.macro init_bss
    ldr	    r0, =__bss_start
    ldr	    r1, =__bss_end
    mov	    r2, #0
bss_loop:
    cmp	    r0, r1
    strne   r2, [r0, #0x4]!
    bne     bss_loop
.endm

start:
    init_bss

    /* Stack initialize. */
	ldr     sp, =__stack_start
	msr     cpsr_c,0xc0|0x12
	ldr     sp, =__irq_stack_start
	msr     cpsr_c,0xc0|0x17
	ldr     sp, =__abt_stack_start
	msr     cpsr_c,0xc0|0x13

    /* Set STLR - Disable MMU. */
    mrc     p15, 0, r1, c1, c0, 0
   	bic	    r0, r0, #0x00002300
	bic	    r0, r0, #0x00000087
	orr	    r0, r0, #0x00000002
	orr	    r0, r0, #0x00001000
    mcr     p15, 0, r1, c1, c0, 0

    /* Invalidate TLB. */
    mcr     p15, 0, r1, c8, c7, 0

    /* Invalidate L1 Caches */
    mov     r1, #0
    mcr     p15, 0, r1, c7, c5, 0

    bl      board_init
    ldr     pc, =init
