/* mach/odroid-pc/board.c
 *
 * Board specific routine.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software FoundatIon; either versIon 2 of the License, or
 * (at your optIon) any later versIon.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <config.h>
#include <dinos/io.h>
#include <arch/types.h>
#include <arch/map.h>
#include <arch/dev.h>
#include <mach/map-exynos4.h>

#define MODULE "INIT" 

struct device_desc odroid_devices[] = {
    {"serial", S5P_PA_UART1},
};

struct map_desc board_mapdesc[] = {
    {0x10000000, 0x10000000, 0x04000000, MT_DEVICE}, /* SFR region */
    {0x40000000, 0x40000000, 0x10000000, MT_MEMORY}, /* Memory bank 1 */
    {0x50000000, 0x50000000, 0x10000000, MT_MEMORY}, /* Memory bank 2 */
    {0x60000000, 0x60000000, 0x10000000, MT_MEMORY}, /* Memory bank 3 */
    {0x70000000, 0x70000000, 0x10000000, MT_MEMORY}, /* Memory bank 4 */
};

struct mem_region board_region = {
    .count	= 5,
    .map	= board_mapdesc,
};

void board_init(void)
{
    log("Target: Samsung Exynos2410 ODROID-PC board.\n");
}
