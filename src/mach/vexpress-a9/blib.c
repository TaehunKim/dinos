/* mach/vexpress-a9/blib.c
 *
 * Board dependent libraries for board initialize routine.
 *
 * Copyright (C) 2015 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <arch/types.h>
#include <mach/board.h>
#include <mach/motherboard.h>

static const int DR         = 0x00;
static const int FR         = 0x18;
static const int IBRD       = 0x24;
static const int FBRD       = 0x28;
static const int LCR_H      = 0x2c;
static const int CR         = 0x30;
static const int IFLS       = 0x34;
static const int IMSC       = 0x38;
static const int MIS        = 0x40;
static const int ICR        = 0x44;
static const int FR_TXFF    = 0x20;   

static volatile u32 *uart = (volatile u32 *)V2M_UART0;

size_t slen(const char * s)
{
	const char *sc;

	for (sc = s; *sc != '\0'; ++sc)
		/* nothing */;
	return sc - s;
}

int board_print(const char *s, int len)
{
    int i;
	
    for (i = 0; i < len; ++i) {
        while (uart[FR] & FR_TXFF)
            ;
        uart[DR] = *(s+i);
    }

    return i;
}
