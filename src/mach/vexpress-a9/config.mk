# Compiler options
CFLAGS += -march=armv7-a -mcpu=cortex-a9 -mfpu=neon -ftree-vectorize -ffast-math
CFLAGS += -mfloat-abi=hard

# Linker script variables
KERNEL_START_ADDR = 0x80009000
PAGE_SIZE = 4096
export KERNEL_START_ADDR PAGE_SIZE

# Build configs for vexpress-a9 board.
CONFIG_PL101=y
CONFIG_PL110=y
CONFIG_PL180=y
CONFIG_SMC9118=y
