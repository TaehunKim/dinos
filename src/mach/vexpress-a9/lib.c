/* mach/odroid-pc/lib.c
 *
 * Board dependent libraries.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dinos/io.h>
#include <arch/types.h>
#include <mach/board.h>
#include <mach/motherboard.h>

static const int DR         = 0x00;
static const int FR         = 0x18;
static const int IBRD       = 0x24;
static const int FBRD       = 0x28;
static const int LCR_H      = 0x2c;
static const int CR         = 0x30;
static const int IFLS       = 0x34;
static const int IMSC       = 0x38;
static const int MIS        = 0x40;
static const int ICR        = 0x44;
static const int FR_TXFF    = 0x20;   

static u32 uart_base = V2M_UART0;

/* 
 * Caution! 
 * this function can be used after Timer.init() because this function need the timer1 count value.
 */
void board_udelay(unsigned int usec)
{
    u32 pre_val = Io.readl(CT_CA9X4_SP804_TIMER + 0x24);
    u32 expire_time = pre_val - usec;

    if (expire_time > pre_val) {
        /* Wait for reset value. */
        while (Io.readl(CT_CA9X4_SP804_TIMER + 0x24) > 0x10000000)
            ;
    }

    while (Io.readl(CT_CA9X4_SP804_TIMER + 0x24) > expire_time)
        ;
}

int board_print(const char *s, int len)
{
    int i;
	
    for (i = 0; i < len; ++i) {
        while (Io.readl(uart_base + FR) & FR_TXFF)
            ;
        Io.writel(uart_base + DR, *(s+i));
    }

    return i;
}
