/* mach/vexpress-a9/blib.h
 *
 * Board dependent libraries for board initialize routine.
 *
 * Copyright (C) 2015 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _BLIB_H_
#define _BLIB_H_

extern size_t slen(const char * s);
extern int board_print(const char *s, int len);

inline int printe(const char *s)
{
    board_print(s, slen(s));
}

inline void epanic(const char *s)
{
    board_print(s, slen(s));
    while(1);
}

inline void irq_disable(void)
{
    asm volatile ("cpsid i	/* IRQ disable. */":::"memory", "cc");
}

inline int is_align(int align, addr_t addr)
{
    return !(addr & (align - 1));
}

inline void io_writel(u32 addr, u32 val)
{
    *((volatile u32 *)addr) = val;
}

inline u32 io_readl(u32 addr)
{
    return *((volatile u32 *)addr);
}
#endif
