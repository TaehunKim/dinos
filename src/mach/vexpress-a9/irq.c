#include <dinos/irq.h>
#include <dinos/io.h>
#include <dinos/sched.h>
#include <mach/gic.h>
#include <mach/ct-ca9x4.h>
#include <stdio.h>

static void gic_init(void)
{
    u32 cbar;
    u32 cpu_base;
    u32 dist_base;
    u32 i, irqs;

    /* Initialize mmio addresses by CBAR. */
    asm("mrc p15, 4, %0, c15, c0, 0" : "=r" (cbar));
    DEBUGF("%s: CBAR = 0x%X\n", __func__, cbar);

    cpu_base = cbar + GIC_CPU_OFFSET;
    dist_base = cbar + GIC_DIST_OFFSET;

    DEBUGF("%s: GIC CPU base = 0x%X\n", __func__, cpu_base);
    DEBUGF("%s: GIC DIST base = 0x%X\n", __func__, dist_base);

    /* Disable interrupt(CPU). */
    Io.writel(cpu_base + GIC_CPU_CTRL, 0);

    /* Disable interrupt(DIST). */
    Io.writel(dist_base + GIC_DIST_CTRL, 0);
    
    /* Get the maximum irqs. */
    irqs = 32 * ((Io.readl(dist_base + GIC_DIST_CTR) & 0x1f) + 1);

    /* Set all global interrupts to be level triggered, active low. */
    for (i = 32; i < irqs; i += 16)
        Io.writel(dist_base + GIC_DIST_CONFIG + (i >> 4) * 4, 0x55555555);

    /* Disable all interrupts */
    for (i = 32; i < irqs; i += 32)
        Io.writel(dist_base + GIC_DIST_ENABLE_CLEAR + (i >> 5) * 4, 0xffffffff);

    for (i = 0; i < irqs; i += 4) {
        /* Set all interrupt priorities to highest. */
        Io.writel(dist_base + GIC_DIST_PRI + (i >> 2) * 4, 0);
        /* Set all CPU Target registers to 'CPU 0'. */
        Io.writel(dist_base + GIC_DIST_TARGET + (i >> 2) * 4, 0x01010101);
    }

    /* Set all the interrupts to be in Group 0 (secure) */
    for (i = 0; i < irqs; i += 32)
        Io.writel(dist_base + GIC_DIST_ICDISR + (i >> 5) * 4, 0);

    /* Enable CPU interface. */
    Io.writel(cpu_base + GIC_CPU_CTRL, 1);

    /* Set priority mask register. Only the interrupt which has a higher priority can be signalled to CPU. */
    Io.writel(cpu_base + GIC_CPU_PRIMASK, 0xff);

    /* Enable DIST interface. */
    Io.writel(dist_base + GIC_DIST_CTRL, 1);
}


void board_irq_init(void)
{
    gic_init();
}

/* Global IRQ handler. */
void board_irq_handler(int no)
{
    u32 int_id;
    struct intr *cur_irq;
    u32 flags;

    flags = Irq.save();

    int_id = Io.readl(GIC_CPU_BASE + GIC_CPU_INTACK) & GIC_INT_ACK_MASK;
    cur_irq = Irq.get(int_id);

    if (cur_irq->used)
        cur_irq->isr(cur_irq->arg);

    Io.writel(GIC_CPU_BASE + GIC_CPU_EOI, int_id);

    /* Expired tick of current thread.  */
    if ((Thread.cur()->tick == 0) && (int_id == IRQ_CT_CA9X4_TIMER0)) {
        Thread.cur()->tick = DEF_TICK;
        /* Call scheduler. */
        Sched.schedule();
    }

    Irq.restore(flags);
}
