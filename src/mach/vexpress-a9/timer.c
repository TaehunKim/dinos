/* mach/vexpress-a9/timer.c
 *
 * Timer module for vexpress board.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <dinos/timer.h>
#include <dinos/io.h>
#include <dinos/irq.h>
#include <dinos/thread.h>
#include <dinos/sched.h>
#include <mach/gic.h>
#include <mach/ct-ca9x4.h>

#define MODULE "Timer"

static volatile int timer_base = CT_CA9X4_SP804_TIMER;

void timer_isr(void *data)
{
    /* Clear timer interrupt. */
    Io.writel(timer_base + 0xc, 0x1);

    ++tick;
    --Thread.cur()->tick;
}

static int timer_init(void)
{
    /* (Period) | (Int. Enable) | (Pre Scale) | (Timer Size) | (OneShot) */
    u32 cntl = (0x1 << 6) | (0x1 << 5) | (0x0 << 2) | (0x1 << 1) | 0x0;
    u32 cntl2 = (0x1 << 7) | (0x0 << 2) | (0x1 << 1) | 0x0; /* Free mode */
    int i;

    Io.writel(timer_base + 0x8, 0);
    Io.writel(timer_base + 0x28, 0);

    /* Set Timer0 load register. */
    Io.writel(timer_base + 0x0, CFG_TIMER_HZ);
    /* Set Timer1 load register. */
    Io.writel(timer_base + 0x20, 0xffffffff);

    /* Clear timer interrupt. */
    Io.writel(timer_base + 0xc, 0x1);

    /* Set Timer0 Control register. */
    Io.writel(timer_base + 0x8, cntl);
    /* Set Timer1 Control register. */
    Io.writel(timer_base + 0x28, cntl2);

    return 0;
}

static int timer_start(void)
{
    u32 reg;

    Irq.request(IRQ_CT_CA9X4_TIMER0, "timer0", timer_isr, NULL);

    reg = Io.readl(timer_base + 0x8);
    reg |= (0x1 << 7);
    Io.writel(timer_base + 0x8, reg);
}

static int timer_stop(void)
{
    /* Stop timer. */	
    Io.writel(timer_base+0x8, 0);
}

struct timer timer = {
    .init	= timer_init,
    .start	= timer_start,
    .stop	= timer_stop,
};
