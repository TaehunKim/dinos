/* mach/vexpress-a9/board.c
 *
 * Board specific routine.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software FoundatIon; either versIon 2 of the License, or
 * (at your optIon) any later versIon.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <config.h>
#include <arch/paging.h>
#include <dinos/paging.h>
#include <arch/types.h>
#include <arch/map.h>
#include <arch/dev.h>
#include <mach/motherboard.h>
#include <mach/gic.h>

#include "blib.h"

#define PAGE_SIZE   CFG_PGSIZE

extern char __kernel_pt[];

struct map_desc board_mapdesc[] = {
    {0xD0000000, 0x10000000, 0x00020000, MT_DEVICE},    /* SMB CS7(mother board peripherals */
    {0xD0020000, 0x10020000, 0x0ffe0000, MT_DEVICE},    /* On-chip peripherals */
    {0xE0000000, 0x40000000, 0x04000000, MT_MEMORY},    /* SMB CS0 */
    {0xE4000000, 0x44000000, 0x04000000, MT_MEMORY},    /* SMB CS1 */
    {0xE8000000, 0x48000000, 0x04000000, MT_MEMORY},    /* SMB CS2 */
    {0x4C000000, 0x4C000000, 0x04000000, MT_DEVICE},    /* SMB CS3 */
    //{0xF0000000, 0x50000000, 0x04000000, MT_MEMORY},    /* SMB CS4 */
    //{0xF4000000, 0x54000000, 0x04000000, MT_MEMORY},    /* SMB CS5 */
    //{0xF8000000, 0x58000000, 0x04000000, MT_MEMORY},    /* SMB CS6 */
    {0x60000000, 0x60000000, 0x20000000, MT_MEMORY},    /* Local DDR2 */
    {0x80000000, 0x60000000, 0x20000000, MT_MEMORY},    /* Local DDR2 */
    {0xA4000000, 0x84000000, 0x1c000000, MT_MEMORY},    /* Local DDR2 */
    {0xF4000000, 0xE4000000, 0x1bffffff, MT_DEVICE},    /* External HSB AXI */
};

struct mem_region board_region = {
    .count	= 10,
    .map	= board_mapdesc,
};

static u32 balloc_pt2 = 0x62000000;

static void barch_pt_init(pte_t *pt, int count)
{
    int i;

    for (i = 0; i < count; ++i) {
        pt[i].e.domain = 0;
        pt[i].e.base = 0;
        pt[i].e.fixed = 0;
    }
}

/* Mapping virtual address to physical address in the ARM architecture. */
static int barch_addr_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag)
{
    l1_pte_t *entry1;
    l2_pte_t *entry2;
    int offset1, offset2;
    char *base1 = (char *)pt;   /* L1 page table base address. */
    char *base2;                /* L2 page table base address. */
    l2_pte_t *pt2;
    u32 ap = (flag >> 8) & 0xFF;/* Access permission. */

    /* L1 page table offset. */
    offset1 = vaddr >> 20;

    /* L1 Page table entry address. */
    entry1 = (l1_pte_t *)(base1 + (offset1 * sizeof(l1_pte_t)));

    entry1->e.fixed = 0x1;

    if (entry1->e.base == 0) {
        pt2 = (l2_pte_t *)(balloc_pt2);
        balloc_pt2 = balloc_pt2 + 1024;
        entry1->e.base = (addr_t)pt2 >> 10;
    } else {
        pt2 = (l2_pte_t *)(entry1->e.base << 10);
    }

    base2 = (char *)pt2;

    /* L2 page table offset. */
    offset2 = (vaddr >> 12) & 0xff;
    entry2 = (l2_pte_t *)(base2 + (offset2 * sizeof(l2_pte_t)));

    /* TODO: Set B,C field into the flag option. */
    entry2->e.fixed = 0x2;  /* Small page */
    entry2->e.b = 1;        /* Buffer */
    entry2->e.c = 1;        /* Cache */
    entry2->e.tex = 0x0;
    entry2->e.ap = ap;
    entry2->e.apx = 0;
    entry2->e.ng = 0;       /* Global */
    entry2->e.base = paddr >> 12; /* Physical memory base address. */

    return 0;
}

static int barch_addr_mapping2(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag)
{
    l1_pte_t *entry1;
    int offset1;
    char *base1 = (char *)pt;   /* L1 page table base address. */
    u32 ap = (flag >> 8) & 0xFF;/* Access permission. */

    /* L1 page table offset. */
    offset1 = vaddr >> 20;

    /* L1 Page table entry address. */
    entry1 = (l1_pte_t *)(base1 + (offset1 * sizeof(l1_pte_t)));

    entry1->se.fixed = 0x2;
    entry1->se.fixed2 = 0x0;
    entry1->se.xn = 1;
    entry1->se.b = 0;     
    entry1->se.c = 0;
    entry1->se.tex = 0;
    entry1->se.ap = ap;
    entry1->se.apx = 0;
    entry1->se.ng = 0;
    entry1->se.base = paddr >> 20; /* Physical memory base address. */

    return 0;
}

static void gic_init(void)
{
    u32 cbar;
    u32 cpu_base;
    u32 dist_base;
    u32 i, irqs;

    /* Initialize mmio addresses by CBAR. */
    asm("mrc p15, 4, %0, c15, c0, 0" : "=r" (cbar));

    cpu_base = cbar + GIC_CPU_OFFSET;
    dist_base = cbar + GIC_DIST_OFFSET;

    /* Disable interrupt(CPU). */
    io_writel(cpu_base + GIC_CPU_CTRL, 0);

    /* Disable interrupt(DIST). */
    io_writel(dist_base + GIC_DIST_CTRL, 0);
    
    /* Get the maximum irqs. */
    irqs = 32 * ((io_readl(dist_base + GIC_DIST_CTR) & 0x1f) + 1);

    /* Set all global interrupts to be level triggered, active low. */
    for (i = 32; i < irqs; i += 16)
        io_writel(dist_base + GIC_DIST_CONFIG + (i >> 4) * 4, 0x55555555);

    /* Disable all interrupts */
    for (i = 32; i < irqs; i += 32)
        io_writel(dist_base + GIC_DIST_ENABLE_CLEAR + (i >> 5) * 4, 0xffffffff);

    for (i = 0; i < irqs; i += 4) {
        /* Set all interrupt priorities to highest. */
        io_writel(dist_base + GIC_DIST_PRI + (i >> 2) * 4, 0);
        /* Set all CPU Target registers to 'CPU 0'. */
        io_writel(dist_base + GIC_DIST_TARGET + (i >> 2) * 4, 0x01010101);
    }

    /* Set all the interrupts to be in Group 0 (secure) */
    for (i = 0; i < irqs; i += 32)
        io_writel(dist_base + GIC_DIST_ICDISR + (i >> 5) * 4, 0);

    /* Enable CPU interface. */
    io_writel(cpu_base + GIC_CPU_CTRL, 1);

    /* Set priority mask register. Only the interrupt which has a higher priority can be signalled to CPU. */
    io_writel(cpu_base + GIC_CPU_PRIMASK, 0xff);

    /* Enable DIST interface. */
    io_writel(dist_base + GIC_DIST_CTRL, 1);
}

void board_init(void)
{
    u32 i, j;
    int ret;
    struct map_desc *map = board_mapdesc;
    u32 pt2_count;
    u32 pt2_spage;
    pte_t *pt = (pte_t *)__kernel_pt;

    irq_disable();
    printe("Target: ARM Coretile Express A9x4 board.\n");

    gic_init();

    if (!is_align(L1_PT_SIZE, (u32)pt))
        epanic("L1 page table was not aligned!\n");

    barch_pt_init(pt, L1_PT_ENTRIES);

    for (i = 0; i < board_region.count; ++i) {
        if ((map[i].type & 0xFF) == MT_MEMORY) {
            for (j = 0; j < map[i].size; j += PAGE_SIZE)
                ret = barch_addr_mapping(pt, map[i].virt + j, map[i].phys + j, map[i].type | P_KERN); 
            if (ret < 0) 
                epanic("Early paging table mapping failed!\n");
        } else if ((map[i].type & 0xFF) == MT_DEVICE) {
            for (j = 0; j < map[i].size; j += 0x100000) {
                ret = barch_addr_mapping2(pt, map[i].virt + j, map[i].phys + j, map[i].type | P_KERN); 
                if (ret < 0) 
                    epanic("Early paging table mapping failed!\n");
            }
        }
    }
    
    barch_tlb_clear();
    barch_set_pt(pt);
    printe("Page table setting is completed.\n");
    barch_paging_enable();
    printe("Paging enabled.\n");

    pt2_spage = (0x62000000 - CFG_PA_START) /  PAGE_SIZE;
    pt2_count = (balloc_pt2 - 0x62000000) / PAGE_SIZE;
    kernel_start(pt2_spage, pt2_count);
}
