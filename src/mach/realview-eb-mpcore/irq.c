#include <dinos/thread.h>
#include <dinos/sched.h>
#include <dinos/io.h>
#include <dinos/irq.h>

#include <arch/types.h>
#include <mach/board.h>
#include <mach/gic.h>

static void gic_core_init(void)
{
    unsigned int base = core_base + REALVIEW_EB11MP_GIC_CPU_OFFSET;
    unsigned int dist_base = core_base + REALVIEW_EB11MP_GIC_DIST_OFFSET;
    unsigned int i;

    /* Disable interrupt(CPU). */
    Io.writel(base + GIC_CPU_CTRL, 0);

    /* Disable interrupt(DIST). */
    Io.writel(dist_base + GIC_DIST_CTRL, 0);

    /* Disable all interrupt sources. */
    Io.writel(dist_base + GIC_DIST_ENABLE_CLEAR, 0xFFFFFFFF);
    Io.writel(dist_base + GIC_DIST_ENABLE_CLEAR + 4, 0xFFFFFFFF);

    /* Clear all pending interrupts. */
    Io.writel(dist_base + GIC_DIST_PENDING_CLEAR, 0xFFFFFFFF);
    Io.writel(dist_base + GIC_DIST_PENDING_CLEAR + 4, 0xFFFFFFFF);

    /*
     * Set all interrupt prIorities to highest.
     */
    for (i = GIC_DIST_PRI; i <= GIC_DIST_PRI + 60; i += 4) {
	    Io.writel(dist_base + i, 0x00000000);
    }

    /*
     * Set all CPU Target registers to 'CPU 0'.
     */
    for (i = GIC_DIST_TARGET; i <= GIC_DIST_TARGET + 60; i += 4) {
	    Io.writel(dist_base + i, 0x01010101);
    }

    /*
     * Set all interrupt sources to be level-sensotove and 1-N software model.
     */
    for (i = GIC_DIST_CONFIG; i <= GIC_DIST_CONFIG + 12; i += 4) {
	    Io.writel(dist_base + i, 0x55555555);
    }

    /* Enable all interrupt prIorities. */
    Io.writel(base + GIC_CPU_PRIMASK, 0xFF);

    /* Enable all interrupt prIorities. */
    Io.writel(base + GIC_CPU_BINPOINT, 3);

    /* Enable interrupt(CPU). */
    Io.writel(base + GIC_CPU_CTRL, 1);

    /* Enable interrupt(DIST). */
    Io.writel(dist_base + GIC_DIST_CTRL, 1);
}

static void gic_eb_init(void)
{
    unsigned int base = sys_base + REALVIEW_EB_GIC_CPU_OFFSET;
    unsigned int dist_base = sys_base + REALVIEW_EB_GIC_DIST_OFFSET;
    unsigned int i;
    unsigned int pldctrl;

    Io.writel(sys_base + REALVIEW_SYS_LOCK_OFFSET, 0x0000a05f);
    pldctrl = Io.readl(sys_base + REALVIEW_EB11MP_SYS_PLD_CTRL1);
    pldctrl = (pldctrl & ~(7<<22)) | (2<<22);
    Io.writel(sys_base + REALVIEW_EB11MP_SYS_PLD_CTRL1, pldctrl);
    Io.writel(sys_base + REALVIEW_SYS_LOCK_OFFSET, 1);

    /* Disable interrupt(CPU). */
    Io.writel(base + GIC_CPU_CTRL, 0);

    /* Disable interrupt(DIST). */
    Io.writel(dist_base + GIC_DIST_CTRL, 0);

    /* Disable all interrupt sources. */
    Io.writel(dist_base + GIC_DIST_ENABLE_CLEAR, 0xFFFFFFFF);
    Io.writel(dist_base + GIC_DIST_ENABLE_CLEAR + 4, 0xFFFFFFFF);
    Io.writel(dist_base + GIC_DIST_ENABLE_CLEAR + 8, 0xFFFFFFFF);

    /* Clear all pending interrupts. */
    Io.writel(dist_base + GIC_DIST_PENDING_CLEAR, 0xFFFFFFFF);
    Io.writel(dist_base + GIC_DIST_PENDING_CLEAR + 4, 0xFFFFFFFF);
    Io.writel(dist_base + GIC_DIST_PENDING_CLEAR + 8, 0xFFFFFFFF);

    /*
     * Set all interrupt prIorities to highest. The EB GICs do not use
     * interrupt IDs 0-31.
     */
    for (i = GIC_DIST_PRI + 32; i <= GIC_DIST_PRI + 92; i += 4) {
	Io.writel(dist_base + i, 0x00000000);
    }

    /*
     * Set all CPU Target registers to 'CPU 0'. The EB GICs only have one CPU
     * (CPU 0) and do not use interrupt IDs 0-31.
     */
    for (i = GIC_DIST_TARGET + 32; i <= GIC_DIST_TARGET + 92; i += 4) {
	Io.writel(dist_base + i, 0x01010101);
    }

    /*
     * Set all interrupt sources to be level-sensotove and 1-N software model.
     * The EB GICs do not use interrupt IDs 0-31.
     */
    for (i = GIC_DIST_CONFIG + 8; i <= GIC_DIST_CONFIG + 20; i += 4) {
	Io.writel(dist_base + i, 0x55555555);
    }

    /* Enable all interrupt prIorities. */
    Io.writel(base + GIC_CPU_PRIMASK, 0xFF);

    /* Enable all interrupt prIorities. */
    Io.writel(base + GIC_CPU_BINPOINT, 3);

    /* Enable interrupt(CPU). */
    Io.writel(base + GIC_CPU_CTRL, 1);

    /* Enable interrupt(DIST). */
    Io.writel(dist_base + GIC_DIST_CTRL, 1);
}

void board_irq_init(void)
{
    gic_core_init();
    gic_eb_init();
}

/* Global IRQ handler. */
void board_irq_handler(int no)
{
    u32 core_id;
    u32 eb_id;
    u32 flags = Irq.save();

    core_id = Io.readl(core_gic_base + GIC_CPU_INTACK);

    if ((core_id & GIC_INT_ACK_MASK) == EB_GIC_ID) {
        struct intr *cur_irq;

        eb_id = Io.readl(eb_gic_base + GIC_CPU_INTACK);
        cur_irq = Irq.get(eb_id & GIC_INT_ACK_MASK);

        /* Used IRQ? */
        if (cur_irq->used == 1) {
            cur_irq->isr(cur_irq->arg);
        }
        /* Clear base board interrupt. */
        Io.writel(eb_gic_base + GIC_CPU_EOI, eb_id);
    }

    /* Clear core interrupt. */
    Io.writel(core_gic_base + GIC_CPU_EOI, core_id);

    /* Expired tick of current thread.  */
    if ((Thread.cur()->tick == 0) && (eb_id == 36)) {
        Thread.cur()->tick = DEF_TICK;
        /* Call scheduler. */
        Sched.schedule();
    }
    Irq.restore(flags);
}
