/* mach/realview-eb-mpcore/board.c
 *
 * Board specific routine.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software FoundatIon; either versIon 2 of the License, or
 * (at your optIon) any later versIon.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <config.h>
#include <dinos/io.h>
#include <arch/types.h>
#include <arch/map.h>
#include <mach/board.h>

#define MODULE "INIT" 

#if 0 /* TODO: device list register. */
struct device_desc realview_devices[] = {
    {"serial", REALVIEW_SYS_BASE+REALVIEW_EB_UART0_OFFSET},
    {"timer", REALVIEW_SYS_BASE+REALVIEW_EB_TIMER0_1_OFFSET},
    {"gic", },
};
#endif

u32 sys_base = REALVIEW_SYS_BASE;
u32 core_base = REALVIEW_EB11MP_SCU_BASE;
u32 eth_base = REALVIEW_EB_ETH_BASE;
u32 eb_gic_base = REALVIEW_SYS_BASE + REALVIEW_EB_GIC_CPU_OFFSET;
u32 core_gic_base = REALVIEW_EB11MP_SCU_BASE + REALVIEW_EB11MP_GIC_CPU_OFFSET;

struct map_desc board_mapdesc[] = {
    {0x0, 0x0, 128*1024*1024, MT_MEMORY},
    {REALVIEW_SYS_BASE, REALVIEW_SYS_BASE, 0x100000, MT_DEVICE},
    {REALVIEW_EB11MP_SCU_BASE, REALVIEW_EB11MP_SCU_BASE, 0x100000, MT_DEVICE},
};

struct mem_region board_region = {
    .count	= 3,
    .map	= board_mapdesc,
};

void board_init(void)
{
    log("Target: ARM ARM11 REALVIEW-EB-MPCORE board.\n");
    /* NOTE: Board specific initialize routine. */
}
