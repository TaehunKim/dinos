# Compiler options
CFLAGS += -mcpu=mpcore

# Linker script variables
KERNEL_START_ADDR = 0x0
PAGE_SIZE = 4096
export KERNEL_START_ADDR PAGE_SIZE

# Build configs for realview-eb-mpcore board.
CONFIG_PL101=y
CONFIG_PL110=y
CONFIG_SMC9118=y
