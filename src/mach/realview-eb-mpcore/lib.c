/* mach/$(MACH)/lib.c
 *
 * Board dependent libraries.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dinos/io.h>
#include <mach/board.h>

enum {
    UART_FR_RXFE = 0x10,
    UART_FR_TXFF = 0x20,
    UART_FR_FLAG = 0x18,
};

void board_udelay(unsigned int usec)
{
    unsigned int pre_val = Io.readl(sys_base + REALVIEW_SYS_24MHz_OFFSET);	
    unsigned int expire_time = pre_val + (usec * 24);

    /* Overflow value of count register. */
    if (expire_time < pre_val) {
        /* Wait for reset value. */
        while (Io.readl(sys_base + REALVIEW_SYS_24MHz_OFFSET) > 0x10000000)
            ;
    }

    while (Io.readl(sys_base + REALVIEW_SYS_24MHz_OFFSET) < expire_time)
        ;
}


int board_print(const char *s, int len)
{
    int i;
	
    for (i=0; i<len; ++i) {
	while(*((volatile unsigned int *)(REALVIEW_EB_UART0_BASE+UART_FR_FLAG)) & UART_FR_TXFF);
	*((char *)REALVIEW_EB_UART0_BASE) = *(s+i);
    }
    return i;
}
