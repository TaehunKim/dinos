/* mach/realview-eb-mpcore/timer.c
 *
 * Timer module for realview-eb-mpcore board.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <config.h>

#include <dinos/irq.h>
#include <dinos/io.h>
#include <dinos/timer.h>
#include <dinos/thread.h>
#include <dinos/sched.h>

#include <mach/board.h>
#include <mach/gic.h>

#define MODULE "Timer"

static int timer_base = 0;

static void timer_init(void)
{
    unsigned int reg;
    unsigned int sctl_base = sys_base + REALVIEW_SCTL_OFFSET;
    timer_base = sys_base + REALVIEW_EB_TIMER0_1_OFFSET;

    log("Initialize Timer module.\n");

    /* Set timer clock 0. */
    reg = Io.readl(sctl_base);
    reg |= (REALVIEW_TIMCLK << REALVIEW_TIMER1_OFFSET);
    Io.writel(sctl_base, reg);

    /* Stop timer. */	
    Io.writel(timer_base+0x8, 0);

    /* Set timer load value. */
    Io.writel(timer_base, CFG_TIMER_HZ);
    Io.writel(timer_base+4, CFG_TIMER_HZ);
    Io.writel(timer_base+0xc, 1);
}

static void timer_isr(void *data)
{
    /* Clear timer interrupt. */
    Io.writel(timer_base+0x0C, 1);
    ++tick;
    --Thread.cur()->tick;
}

static int timer_start(void)
{
    unsigned int reg;
    unsigned int eb_dist = sys_base + REALVIEW_EB_GIC_DIST_OFFSET;
    unsigned int core_dist = core_base + REALVIEW_EB11MP_GIC_DIST_OFFSET;

    DEBUGF("Timer base = 0x%X, eb_dist p= 0x%X, sys_base = 0x%X, core_base = 0x%X\n", 
	   timer_base, eb_dist, sys_base, core_base);

    Irq.request(36, "timer", timer_isr, NULL);

    /* Enable timer. */
    reg = Io.readl(timer_base+0x8);
    reg |= 0xe2;
    Io.writel(timer_base+0x8, reg);

    /* Enable INT. */
    Io.writel(eb_dist + GIC_DIST_ENABLE_SET + 4, 1<<4);
    Io.writel(core_dist + GIC_DIST_ENABLE_SET + 4, 1<<10);
}

static int timer_stop(void)
{
    /* Stop timer. */	
    Io.writel(timer_base+0x8, 0);
}

struct timer timer = {
    .init	= timer_init,
    .start	= timer_start,
    .stop	= timer_stop,
};
