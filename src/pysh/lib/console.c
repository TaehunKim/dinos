/* lib/console.c
 *
 * Console module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#undef __HAVE_ARCH_MEMSET

char getchar(void)
{
    char c;

    asm volatile(
            "mov r0, #0\n"
            "mov r1, %0\n"
            "mov r2, #1\n"
            "swi 1\n"
            :: "r"(&c)
            );

    return c;
}

int putc(char c)
{
    asm volatile(
            "mov r0, #1\n"
            "mov r1, %0\n"
            "mov r2, #1\n"
            "swi 2\n"
            :: "r"(&c)
            );

    return 0;
}

int puts(const char *s)
{
    volatile int len = strlen(s);

    asm volatile(
            "mov r0, #1\n"
            "mov r1, %0\n"
            "mov r2, %1\n"
            "swi 2\n"
            ::"r"(s), "r"(len)
            );
   return 0;
}

int readline(const char *prompt, char *buf)
{
    char c;
    int len=0;

    buf[0] = '\0';

    puts(prompt);
    while ((c = getchar()) != '\r') {
        putc(c);
        *(buf+len) = c;
        len++;
        if (len > CFG_CBSIZE)
            break;
    }
    puts("\n");

    return len;
}

int printf(const char *format, ...)
{
    va_list args;
    int retval;
    //char print_buf[CFG_PBSIZE]={0,};
    char print_buf[CFG_PBSIZE];

    va_start(args, format);
    retval = vsprintf(print_buf, format, args);
    va_end(args);

    puts(print_buf);

    return retval;
}
