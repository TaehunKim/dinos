#ifndef _ASSERT_H_
#define _ASSERT_H_

#include <stdio.h>

#if defined(DEBUG)
#define assert(x) do {  \
        if (!(x)) {     \
		    printf("[DINOS] %s:%u: %s: Assertion '%s' failed.\n",   \
			    __FILE__, __LINE__, __func__, #x);          \
            while(1);   \
        }               \
} while (0)
#else
#define assert(x) 
#endif

#endif /* _ASSERT_H_ */
