#ifndef _TYPES_H_
#define _TYPES_H_

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned long size_t;
#endif

#ifndef _SSIZE_T
#define _SSIZE_T
typedef unsigned long ssize_t;
#endif

/* For linux types. */
typedef unsigned long __kernel_size_t;
typedef unsigned long __kernel_ssize_t;

#ifndef _TIME_T
#define _TIME_T
typedef long time_t;
#endif

#ifndef _CLOCK_T
#define _CLOCK_T
typedef long clock_t;
#endif

/* bsd */
typedef unsigned char	u_char;
typedef unsigned short	u_short;
typedef unsigned int	u_int;
typedef unsigned long	u_long;

/* sysv */
typedef unsigned char	unchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;

typedef	signed char		s8;
typedef	unsigned char	u8;
typedef	signed short	s16;
typedef	unsigned short	u16;
typedef	signed int		s32;
typedef	unsigned int	u32;
typedef	signed long long    s64;
typedef	unsigned long long  u64;

typedef	signed char		__s8;
typedef	unsigned char	__u8;
typedef	signed short	__s16;
typedef	unsigned short	__u16;
typedef	signed int		__s32;
typedef	unsigned int	__u32;
typedef	signed long long    __s64;
typedef	unsigned long long  __u64;


typedef	signed char		s8_t;
typedef	unsigned char	u8_t;
typedef	signed short	s16_t;
typedef	unsigned short	u16_t;
typedef	signed int		s32_t;
typedef	unsigned int	u32_t;
typedef	signed long long    s64_t;
typedef	unsigned long long  u64_t;

typedef	s8  int8_t;
typedef	s16 int16_t;
typedef s32 int32_t;
typedef s64 int64_t;

typedef	u8  uint8_t;
typedef	u16 uint16_t;
typedef u32 uint32_t;
typedef u64 uint64_t;

typedef u32 ptrdiff_t;

typedef u32 addr_t;

typedef unsigned long lbaint_t;

/*
 * Below are truly Linux-specific types that should never collide with
 * any application/library that wants linux/types.h.
 */
#ifdef __CHECKER__
#define __bitwise__ __attribute__((bitwise))
#else
#define __bitwise__
#endif
#ifdef __CHECK_ENDIAN__
#define __bitwise __bitwise__
#else
#define __bitwise
#endif

typedef __u16 __bitwise __le16;
typedef __u16 __bitwise __be16;
typedef __u32 __bitwise __le32;
typedef __u32 __bitwise __be32;
#if defined(__GNUC__)
typedef __u64 __bitwise __le64;
typedef __u64 __bitwise __be64;
#endif
typedef __u16 __bitwise __sum16;
typedef __u32 __bitwise __wsum;

#endif /* _TYPES_H_ */
