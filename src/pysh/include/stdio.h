#ifndef _STDIO_H
#define	_STDIO_H

#include <stdarg.h>
#include <string.h>

#define	BUFSIZ	4096
#define	EOF		(-1)

#define	FILENAME_MAX	4096

#define STDIN	0
#define STDOUT	1
#define STDERR	2

#ifndef _SIZE_T
#define	_SIZE_T
typedef unsigned int	size_t;	/* type returned by sizeof */
#endif /* _SIZE_T */

extern int sprintf(char *buf, const char *fmt, ...);
extern int vsprintf(char *buf, const char *fmt, va_list args);
extern int vscnprintf(char *buf, size_t size, const char *fmt, va_list args);

extern int printf(const char *fmt, ...);
extern int putc(char c);
extern int puts(const char *str);
extern char getchar(void);

int readline(const char *prompt, char *buf);

#endif /* _STDIO_H */
