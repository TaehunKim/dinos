#ifndef _DELAY_H_
#define _DELAY_H_

#include <dinos/thread.h>
#include <dinos/sched.h>
#include <dinos/irq.h>

extern void board_udelay(unsigned int usec);

/**
 * @brief		Microsecond delay.
 * @param[in]	Microsecond for delay.
 */
#define udelay(usec)	board_udelay(usec)

/**
 * @brief		Millisecond delay.
 * @param[in]	Millisecond for delay.
 */
#define mdelay(msec)	udelay(msec * 1000)

#define wait(cond) do {                     \
    unsigned int flag = Irq.save();         \
    Thread.cur()->status = THREAD_BLOCKED;	\
    while(cond) {                           \
        Sched.schedule();                   \
    }                                       \
    Thread.cur()->status = THREAD_RUNNING;  \
    Irq.restore(flag);                      \
} while(0)

#define sleep(sec) do {                             \
    unsigned int expire_tick = tick + (sec * 1000); \
    wait(tick < expire_tick);                       \
} while(0)

#define tsleep(val) do {                    \
    unsigned int expire_tick = tick + val;  \
    wait(tick < expire_tick);               \
} while(0)

#endif /* _DELAY_H_ */
