#ifndef _STDLIB_H_
#define	_STDLIB_H_

extern int simple_atoi(const char *nptr);
extern char *simple_itoa(unsigned long i);
extern unsigned long simple_strtoul(const char *cp, char **endp,
        unsigned int base);
extern long simple_strtol(const char *cp, char **endp, unsigned int base);

#endif /* _STDLIB_H_*/
