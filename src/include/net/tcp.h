/* ./include/net/tcp.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    tcp.h
 * @brief   TCP data structures & APIs.
 *
 * @author	Taehun Kim <kth3321@gmail.com>
 * @version $Id: tcp.h 207 2011-06-15 14:17:04Z brian $
 */

#ifndef _TCP_H_
#define _TCP_H_

#include <net/pbuf.h>
#include <net/opt.h>
#include <net/ip.h>
#include <net/icmp.h>

enum tcp_state {
    CLOSED      = 0,
    LISTEN      = 1,
    SYN_SENT    = 2,
    SYN_RCVD    = 3,
    ESTABLISHED = 4,
    FIN_WAIT_1  = 5,
    FIN_WAIT_2  = 6,
    CLOSE_WAIT  = 7,
    CLOSING     = 8,
    LAST_ACK    = 9,
    TIME_WAIT   = 10
};

/* the TCP protocol control block */
struct tcp_pcb {
    struct tcp_pcb *next;   /* for the linked list */
    enum tcp_state state;   /* TCP state */
    void *callback_arg;
    /* Function to call when a listener has been connected. */
    int (*accept)(void *arg, struct tcp_pcb *newpcb, int err);
    struct ip_addr local_ip;
    u16 local_port;
    struct ip_addr remote_ip;
    u16 remote_port;
    /* receiver varables */
    u32 rcv_nxt;   /* next seqno expected */
    u16 rcv_wnd;   /* receiver window */
    /* Timers */
    u16 tmr;
    /* Retransmission timer. */
    u8 rtime;
    u16 mss;   /* maximum segment size */
    u8 flags;
#define TF_ACK_DELAY 0x01   /* Delayed ACK. */
#define TF_ACK_NOW   0x02   /* Immediate ACK. */
#define TF_INFR      0x04   /* In fast recovery. */
#define TF_RESET     0x08   /* Connection was reset. */
#define TF_CLOSED    0x10   /* Connection was sucessfully closed. */
#define TF_GOT_FIN   0x20   /* Connection was closed by the remote end. */

    /* RTT estimation variables. */
    u16 rttest; /* RTT estimate in 500ms ticks */
    u32 rtseq;  /* sequence number being timed */
    s32 sa, sv;
    u16 rto;    /* retransmission time-out */
    u8 nrtx;    /* number of retransmissions */
    /* fast retransmit/recovery */
    u32 lastack; /* Highest acknowledged seqno. */
    u8 dupacks;
    /* congestion avoidance/control variables */
    u16 cwnd;  
    u16 ssthresh;
    /* sender variables */
    u32 snd_nxt,       /* next seqno to be sent */
        snd_max,       /* Highest seqno sent. */
        snd_wnd,       /* sender window */
        snd_wl1, snd_wl2,
        snd_lbb;      
    u16 snd_buf;   /* Avaliable buffer space for sending. */
    u8 snd_queuelen;

    /* Function to be called when more send buffer space is avaliable. */
    int (*sent)(void *arg, struct tcp_pcb *pcb, u16 space);
    u16 acked;

    /* Function to be called when (in-sequence) data has arrived. */
    int (*recv)(void *arg, struct tcp_pcb *pcb, struct pbuf *p, int err);
    struct pbuf *recv_data;

    /* Function to be called when a connection has been set up. */
    int (*connected)(void *arg, struct tcp_pcb *pcb, int err);

    /* Function which is called periodically. */
    int (*poll)(void *arg, struct tcp_pcb *pcb);

    /* Function to be called whenever a fatal error occurs. */
    void (*errf)(void *arg, int err);
    u8 polltmr, pollinterval;

    /* These are ordered by sequence number: */
    struct tcp_seg *unsent;   /* Unsent (queued) segments. */
    struct tcp_seg *unacked;  /* Sent but unacknowledged segments. */
#if TCP_QUEUE_OOSEQ  
    struct tcp_seg *ooseq;    /* Received out of sequence segments. */
#endif /* TCP_QUEUE_OOSEQ */
};

/* Functions for interfacing with TCP: */

/* Lower layer interface to TCP: */
void  tcp_init(void);  /* Must be called first to initialize TCP. */
void  tcp_tmr(void);  /* Must be called every TCP_TMR_INTERVAL ms. (Typically 100 ms). */

/* Application program's interface: */
struct tcp_pcb * tcp_new(void);
void tcp_arg(struct tcp_pcb *pcb, void *arg);
void tcp_accept(struct tcp_pcb *pcb, 
                int (*accept)(void *arg, struct tcp_pcb *newpcb, int err));
void tcp_recv(struct tcp_pcb *pcb, int (*recv)(void *arg, struct tcp_pcb *tpcb, 
              struct pbuf *p, int err));
void tcp_sent(struct tcp_pcb *pcb, int (*sent)(void *arg, struct tcp_pcb *tpcb, u16 len));
void tcp_poll(struct tcp_pcb *pcb, int (*poll)(void *arg, struct tcp_pcb *tpcb), u8 interval);
void tcp_err(struct tcp_pcb *pcb, void (*err)(void *arg, int err));

#define tcp_sndbuf(pcb)   ((pcb)->snd_buf)

void tcp_recved(struct tcp_pcb *pcb, u16 len);
int tcp_bind(struct tcp_pcb *pcb, struct ip_addr *ipaddr, u16 port);
int tcp_connect (struct tcp_pcb *pcb, struct ip_addr *ipaddr, 
                 u16 port, int (* connected)(void *arg, struct tcp_pcb *tpcb, int err));
struct tcp_pcb *tcp_listen(struct tcp_pcb *pcb);
void tcp_abort(struct tcp_pcb *pcb);
int tcp_close(struct tcp_pcb *pcb);
int tcp_write(struct tcp_pcb *pcb, const void *dataptr, u16 len, u8 copy);

/* It is also possible to call these two functions at the right
   intervals (instead of calling tcp_tmr()). */
void tcp_slowtmr(void);
void tcp_fasttmr(void);

/* Only used by IP to pass a TCP segment to TCP: */
void tcp_input(struct pbuf *p, struct netif *inp);
/* Used within the TCP code only: */
int tcp_output(struct tcp_pcb *pcb);

#define TCP_SEQ_LT(a,b)     ((s32)((a)-(b)) < 0)
#define TCP_SEQ_LEQ(a,b)    ((s32)((a)-(b)) <= 0)
#define TCP_SEQ_GT(a,b)     ((s32)((a)-(b)) > 0)
#define TCP_SEQ_GEQ(a,b)    ((s32)((a)-(b)) >= 0)

#define TCP_FIN 0x01
#define TCP_SYN 0x02
#define TCP_RST 0x04
#define TCP_PSH 0x08
#define TCP_ACK 0x10
#define TCP_URG 0x20

/* Length of the TCP header, excluding options. */
#define TCP_HLEN 20

#define TCP_TMR_INTERVAL        100  /* The TCP timer interval in milliseconds. */
#define TCP_FAST_INTERVAL       200  /* the fine grained timeout in milliseconds */
#define TCP_SLOW_INTERVAL       500  /* the coarse grained timeout in milliseconds */
#define TCP_FIN_WAIT_TIMEOUT    20000 /* milliseconds */
#define TCP_SYN_RCVD_TIMEOUT    20000 /* milliseconds */
#define TCP_OOSEQ_TIMEOUT       6 /* x RTO */
#define TCP_MSL                 60000 /* The maximum segment lifetime in microseconds */

struct tcp_hdr {
    u16 src;
    u16 dest;
    u32 seqno;
    u32 ackno;
    u16 _offset_flags;
    u16 wnd;
    u16 chksum;
    u16 urgp;
} __attribute__((packed));

#define TCPH_OFFSET(hdr) (ntohs((hdr)->_offset_flags) >> 8)
#define TCPH_FLAGS(hdr) (ntohs((hdr)->_offset_flags) & 0xff)

#define TCPH_OFFSET_SET(hdr, offset) (hdr)->_offset_flags = HTONS(((offset) << 8) | TCPH_FLAGS(hdr))
#define TCPH_FLAGS_SET(hdr, flags) (hdr)->_offset_flags = HTONS((TCPH_OFFSET(hdr) << 8) | (flags))

#define TCP_TCPLEN(seg) ((seg)->len + ((TCPH_FLAGS((seg)->tcphdr) & TCP_FIN || \
					    TCPH_FLAGS((seg)->tcphdr) & TCP_SYN)? 1: 0))



struct tcp_pcb_listen {  
    struct tcp_pcb_listen *next;   /* for the linked list */
    enum tcp_state state;   /* TCP state */
    void *callback_arg;

    /* Function to call when a listener has been connected. */
    void (*accept)(void *arg, struct tcp_pcb *newpcb);

    struct ip_addr local_ip;
    u16 local_port;
};

/* This structure is used to repressent TCP segments. */
struct tcp_seg {
    struct tcp_seg *next;    /* used when putting segements on a queue */
    struct pbuf *p;          /* buffer containing data + TCP header */
    void *dataptr;           /* pointer to the TCP data in the pbuf */
    u16 len;               /* the TCP length of this segment */
    struct tcp_hdr *tcphdr;  /* the TCP header */
};

/* Internal functions and global variables: */
struct tcp_pcb *tcp_pcb_copy(struct tcp_pcb *pcb);
void tcp_pcb_purge(struct tcp_pcb *pcb);
void tcp_pcb_remove(struct tcp_pcb **pcblist, struct tcp_pcb *pcb);

u8 tcp_segs_free(struct tcp_seg *seg);
u8 tcp_seg_free(struct tcp_seg *seg);
struct tcp_seg *tcp_seg_copy(struct tcp_seg *seg);

#define tcp_ack(pcb)     if((pcb)->flags & TF_ACK_DELAY) { \
                            (pcb)->flags |= TF_ACK_NOW; \
                            tcp_output(pcb); \
                         } else { \
                            (pcb)->flags |= TF_ACK_DELAY; \
                         }

#define tcp_ack_now(pcb) (pcb)->flags |= TF_ACK_NOW; \
                         tcp_output(pcb)

int tcp_send_ctrl(struct tcp_pcb *pcb, u8 flags);
int tcp_enqueue(struct tcp_pcb *pcb, void *dataptr, u16 len, u8 flags, 
                u8 copy, u8 *optdata, u8 optlen);

void tcp_rexmit_seg(struct tcp_pcb *pcb, struct tcp_seg *seg);

void tcp_rst(u32 seqno, u32 ackno, struct ip_addr *local_ip, 
             struct ip_addr *remote_ip, u16 local_port, u16 remote_port);

u32 tcp_next_iss(void);

extern u32 tcp_ticks;

#if TCP_DEBUG || TCP_INPUT_DEBUG || TCP_OUTPUT_DEBUG
void tcp_debug_print(struct tcp_hdr *tcphdr);
void tcp_debug_print_flags(u8 flags);
void tcp_debug_print_state(enum tcp_state s);
void tcp_debug_print_pcbs(void);
int tcp_pcbs_sane(void);
#else
#define tcp_pcbs_sane() 1
#endif /* TCP_DEBUG */


/* The TCP PCB lists. */
extern struct tcp_pcb_listen *tcp_listen_pcbs;  /* List of all TCP PCBs in LISTEN state. */
extern struct tcp_pcb *tcp_active_pcbs;  /* List of all TCP PCBs that are in a state 
                                            in which they accept or send data. */
extern struct tcp_pcb *tcp_tw_pcbs;      /* List of all TCP PCBs in TIME-WAIT. */
extern struct tcp_pcb *tcp_tmp_pcb;      /* Only used for temporary storage. */

/* Axoims about the above lists:   
   1) Every TCP PCB that is not CLOSED is in one of the lists.
   2) A PCB is only in one of the lists.
   3) All PCBs in the tcp_listen_pcbs list is in LISTEN state.
   4) All PCBs in the tcp_tw_pcbs list is in TIME-WAIT state.
*/

/* Define two macros, TCP_REG and TCP_RMV that registers a TCP PCB
   with a PCB list or removes a PCB from a list, respectively. */
#ifdef TCP_DEBUG
#define TCP_REG(pcbs, npcb) do {\
                            DEBUGF(TCP_DEBUG, ("TCP_REG %p local port %d\n", npcb, npcb->local_port)); \
                            for(tcp_tmp_pcb = *pcbs; \
				                tcp_tmp_pcb != NULL; \
				                tcp_tmp_pcb = tcp_tmp_pcb->next) { \
                                    if (tcp_tmp_pcb == npcb) { \
                                        printf("TCP_REG: already registered\r\n"); \
                                        while(1); \
                                    } \
                            } \
                            if (npcb->state == CLOSED) { \
                                printf("TCP_REG: pcb->state != CLOSED\r\n"); \
                                while(1); \
                            } \
                            npcb->next = *pcbs; \
                            if (npcb->next == npcb) { \
                                printf("TCP_REG: npcb->next != npcb\n"); \
                                while(1); \
                            } \
                            *pcbs = npcb; \
                            if (!tcp_pcbs_sane()) { \
                                printf("TCP_RMV: tcp_pcbs sane\r\n"); \
                                while(1); \
                            } \
                            } while(0)
#define TCP_RMV(pcbs, npcb) do { \
                            if (*pcbs == NULL) { \
                                printf("TCP_RMV: pcbs != NULL\r\n"); \
                                while(1); \
                            } \
                            DEBUGF(TCP_DEBUG, ("TCP_RMV: removing %p from %p\n", npcb, *pcbs)); \
                            if(*pcbs == npcb) { \
                               *pcbs = (*pcbs)->next; \
                            } else for(tcp_tmp_pcb = *pcbs; tcp_tmp_pcb != NULL; tcp_tmp_pcb = tcp_tmp_pcb->next) { \
                               if(tcp_tmp_pcb->next != NULL && tcp_tmp_pcb->next == npcb) { \
                                  tcp_tmp_pcb->next = npcb->next; \
                                  break; \
                               } \
                            } \
                            npcb->next = NULL; \
                            if (!tcp_pcbs_sane()) { \
                                printf("TCP_RMV: tcp_pcbs sane\r\n"); \
                                while(1); \
                            }\
                            DEBUGF(TCP_DEBUG, ("TCP_RMV: removed %p from %p\n", npcb, *pcbs)); \
                            } while(0)

#else /* TCP_DEBUG */
#define TCP_REG(pcbs, npcb) do { \
                            npcb->next = *pcbs; \
                            *pcbs = npcb; \
                            } while(0)
#define TCP_RMV(pcbs, npcb) do { \
                            if(*pcbs == npcb) { \
                               *pcbs = (*pcbs)->next; \
                            } else for(tcp_tmp_pcb = *pcbs; tcp_tmp_pcb != NULL; tcp_tmp_pcb = tcp_tmp_pcb->next) { \
                               if(tcp_tmp_pcb->next != NULL && tcp_tmp_pcb->next == npcb) { \
                                  tcp_tmp_pcb->next = npcb->next; \
                                  break; \
                               } \
                            } \
                            npcb->next = NULL; \
                            } while(0)
#endif /* TCP_DEBUG */
#endif /* _TCP_H_ */
