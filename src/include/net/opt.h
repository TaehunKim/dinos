/* ./include/net/opt.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    opt.h
 * @brief   Network option defines.
 *
 * @author	Taehun Kim <kth3321@gmail.com>
 * @version $Id: opt.h 210 2011-06-16 16:14:12Z brian $
 */

#ifndef _OPT_H_
#define _OPT_H_

/* Define some handy default values for configuration parameters. */

#ifndef ICMP_TTL
#define ICMP_TTL	255
#endif

#ifndef UDP_TTL
#define UDP_TTL	255
#endif

#ifndef TCP_TTL
#define TCP_TTL	255
#endif

#ifndef TCP_MSS
#define TCP_MSS	128 /* A *very* conservative default. */
#endif

#ifndef TCP_WND
#define TCP_WND	2048
#endif 

#ifndef TCP_MAXRTX
#define TCP_MAXRTX	12
#endif

#ifndef TCP_SYNMAXRTX
#define TCP_SYNMAXRTX	6
#endif

#ifndef MEM_ALIGNMENT
#define MEM_ALIGNMENT	4
#endif

#ifndef PBUF_POOL_SIZE
#define PBUF_POOL_SIZE	16
#endif

#ifndef PBUF_POOL_BUFSIZE
#define PBUF_POOL_BUFSIZE	1600
#endif

#ifndef PBUF_LINK_HLEN
#define PBUF_LINK_HLEN	14
#endif

#ifndef UDP
#define UDP	1
#endif

#ifndef TCP
#define TCP	0
#endif

#define MEM_ALIGN_SIZE(size) (size + \
                             ((((size) % MEM_ALIGNMENT) == 0)? 0 : \
                             (MEM_ALIGNMENT - ((size) % MEM_ALIGNMENT))))

#define MEM_ALIGN(addr) (void *)MEM_ALIGN_SIZE((u32)addr)

#endif /* _OPT_H_ */
