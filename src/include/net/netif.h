/* ./include/net/netif.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    netif.h
 * @brief   Network interface data structures & APIs.
 *
 * @author	Taehun Kim <kth3321@gmail.com>
 * @version $Id: netif.h 202 2011-06-14 18:06:59Z brian $
 */

#ifndef _NETIF_H_
#define _NETIF_H_

#include <net/ip_addr.h>
#include <net/inet.h>
#include <net/pbuf.h>

#define CMD_GET_HWADDR	0x0001

struct netif {
  struct netif *next;
  u8 num;
  struct ip_addr ip_addr;
  struct ip_addr netmask;  /* netmask in network byte order */
  struct ip_addr gw;
  char hwaddr[6];

  /* This function is called by the network device driver
     when it wants to pass a packet to the TCP/IP stack. */
  int (*input)(struct pbuf *p, struct netif *netif);

  /* The following two fields should be filled in by the
     initialization function for the device driver. */

  char name[8];

  /* This function is called by the IP module when it wants
     to send a packet on the interface. */
  int (*output)(struct netif *netif, struct pbuf *p,
		   struct ip_addr *ipaddr);
  int (*linkoutput)(struct netif *netif, struct pbuf *p);
  int netfd;

  /* This field can be set bu the device driver and could point
     to state information for the device. */
  void *state;
};

/* The list of network interfaces. */
extern struct netif *netif_list;
extern struct netif *netif_default;

/* netif_init() must be called first. */
void netif_init();

struct netif *netif_add(struct ip_addr *ipaddr, struct ip_addr *netmask,
			struct ip_addr *gw,
			void (* init)(struct netif *netif),
			int (* input)(struct pbuf *p, struct netif *netif));

/* Returns a network interface given its name. The name is of the form
   "et0", where the first two letters are the "name" field in the
   netif structure, and the digit is in the num field in the same
   structure. */
struct netif *netif_find(char *name);

void netif_set_default(struct netif *netif);

void netif_set_ipaddr(struct netif *netif, struct ip_addr *ipaddr);
void netif_set_netmask(struct netif *netif, struct ip_addr *netmast);
void netif_set_gw(struct netif *netif, struct ip_addr *gw);

#endif /* _NETIF_H_ */
