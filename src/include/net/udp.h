/* ./include/net/udp.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    udp.h
 * @brief   UDP data structures & APIs.
 *
 * @author	Taehun Kim <kth3321@gmail.com>
 * @version $Id: udp.h 202 2011-06-14 18:06:59Z brian $
 */

#ifndef _UDP_H_
#define _UDP_H_

#include <net/pbuf.h>
#include <net/inet.h>
#include <net/ip.h>

#define UDP_HLEN 8

struct udp_hdr {
    u16 src;
    u16 dest;  /* src/dest UDP ports */
    u16 len;
    u16 chksum;
} __attribute__((packed));

#define UDP_FLAGS_NOCHKSUM 0x01
#define UDP_FLAGS_UDPLITE  0x02

struct udp_pcb {
    struct udp_pcb *next;
    struct ip_addr local_ip, remote_ip;
    u16 local_port, remote_port;
    u8 flags;
    u16 chksum_len;
    void (*recv)(void *arg, struct udp_pcb *pcb, struct pbuf *p,
                  struct ip_addr *addr, u16 port);
    void *recv_arg;  
};

/* The following functions is the application layer interface to the
   UDP code. */
struct udp_pcb * udp_new(void);
void udp_remove(struct udp_pcb *pcb);
int udp_bind(struct udp_pcb *pcb, struct ip_addr *ipaddr, u16 port);
int udp_connect(struct udp_pcb *pcb, struct ip_addr *ipaddr, u16 port);
void udp_recv(struct udp_pcb *pcb, 
              void (*recv)(void *arg, struct udp_pcb *upcb, 
                           struct pbuf *p, struct ip_addr *addr, u16 port), 
              void *recv_arg);
int udp_send(struct udp_pcb *pcb, struct pbuf *p);

#define udp_flags(pcb) ((pcb)->flags)
#define udp_setflags(pcb, f)  ((pcb)->flags = (f))

/* The following functions is the lower layer interface to UDP. */
u8 udp_lookup(struct ip_hdr *iphdr, struct netif *inp);
void udp_input(struct pbuf *p, struct netif *inp);
void udp_init(void);

#endif /* _UDP_H_ */
