#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <arch/types.h>

struct in_addr {
  u32 s_addr;
};

struct sockaddr_in {
  u8 sin_len;
  u8 sin_family;
  u16 sin_port;
  struct in_addr sin_addr;
  char sin_zero[8];
};

struct sockaddr {
  u8 sa_len;
  u8 sa_family;
  char sa_data[14];
};

#define SOCK_STREAM     1
#define SOCK_DGRAM      2

#define AF_INET         2
#define PF_INET         AF_INET

#define IPPROTO_TCP     6
#define IPPROTO_UDP     17

#define INADDR_ANY      0
#define INADDR_BROADCAST 0xffffffff

int accept(int s, struct sockaddr *addr, int *addrlen);
int bind(int s, struct sockaddr *name, int namelen);
int close(int s);
int connect(int s, struct sockaddr *name, int namelen);
int listen(int s, int backlog);
int recv(int s, void *mem, int len, unsigned int flags);
int recvfrom(int s, void *mem, int len, unsigned int flags,
    		 struct sockaddr *from, int *fromlen);
int send(int s, void *dataptr, int size, unsigned int flags);
int sendto(int s, void *dataptr, int size, unsigned int flags,
		   struct sockaddr *to, int tolen);
int socket(int domain, int type, int protocol);

#endif /* _SOCKET_H_ */
