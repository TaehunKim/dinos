#ifndef _INET_H_
#define _INET_H_

#include <net/pbuf.h>
#include <net/ip_addr.h>
#include <arch/types.h>
#include <config.h>

/* htons() function. */
#if BYTE_ORDER == BIG_ENDIAN
	#define htons(n) (n)
#else /* little endian. */
	#define htons(n) (((((u16)(n) & 0x00ff)) << 8) | (((u16)(n) & 0xff00) >> 8))
#endif

/* htonl() function. */
#if BYTE_ORDER == BIG_ENDIAN
	#define htonl(n) (n)
#else /* little endian. */
	#define htonl(n) (((((u32)(n) & 0x000000ff)) << 24) | \
                        ((((u32)(n) & 0x0000ff00)) << 8) | \
                        ((((u32)(n) & 0x00ff0000)) >> 8) | \
                        ((((u32)(n) & 0xff000000)) >> 24))
#endif

#define ntohs htons
#define ntohl htonl

u16 iinet_chksum_pseudo(
        struct pbuf *p, struct ip_addr *src, struct ip_addr *dest, 
        u8 proto, u16 proto_len);
u16 inet_chksum(void *dataptr, u16 len);
u16 inet_chksum_pbuf(struct pbuf *p);

#endif /* _INET_H_ */
