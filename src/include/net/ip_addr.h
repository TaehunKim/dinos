#ifndef _IP_ADDR_H_
#define _IP_ADDR_H_

#include <arch/types.h>

#define IP_ADDR_ANY 0

#define IP_ADDR_BROADCAST (&ip_addr_broadcast)

struct ip_addr {
  u32 addr;
} __attribute__((packed));

extern struct ip_addr ip_addr_broadcast;

#define IP4_ADDR(ipaddr, a,b,c,d) (ipaddr)->addr = htonl(((u32)(a & 0xff) << 24) | ((u32)(b & 0xff) << 16) | \
                                                         ((u32)(c & 0xff) << 8) | (u32)(d & 0xff))

#define ip_addr_set(dest, src) (dest)->addr = \
                               ((src) == IP_ADDR_ANY? IP_ADDR_ANY:\
				((struct ip_addr *)src)->addr)
#define ip_addr_maskcmp(addr1, addr2, mask) (((addr1)->addr & \
                                              (mask)->addr) == \
                                             ((addr2)->addr & \
                                              (mask)->addr))
#define ip_addr_cmp(addr1, addr2) ((addr1)->addr == (addr2)->addr)

#define ip_addr_isany(addr1) ((addr1) == NULL || (addr1)->addr == 0)

#define ip_addr_isbroadcast(addr1, mask) (((((addr1)->addr) & ~((mask)->addr)) == \
					 (0xffffffff & ~((mask)->addr))) || \
                                         ((addr1)->addr == 0xffffffff) || \
                                         ((addr1)->addr == 0x00000000))


#define ip_addr_ismulticast(addr1) (((addr1)->addr & ntohl(0xf0000000)) == ntohl(0xe0000000))
				   

#define ip_addr_debug_print(ipaddr) printf("%d.%d.%d.%d", \
		    (u8)(ntohl((ipaddr)->addr) >> 24) & 0xff, \
		    (u8)(ntohl((ipaddr)->addr) >> 16) & 0xff, \
		    (u8)(ntohl((ipaddr)->addr) >> 8) & 0xff, \
		    (u8)ntohl((ipaddr)->addr) & 0xff)


#define ip4_addr1(ipaddr) ((u8)(ntohl((ipaddr)->addr) >> 24) & 0xff)
#define ip4_addr2(ipaddr) ((u8)(ntohl((ipaddr)->addr) >> 16) & 0xff)
#define ip4_addr3(ipaddr) ((u8)(ntohl((ipaddr)->addr) >> 8) & 0xff)
#define ip4_addr4(ipaddr) ((u8)(ntohl((ipaddr)->addr)) & 0xff)

#endif /* _IP_ADDR_H_ */
