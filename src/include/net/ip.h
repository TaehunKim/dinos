/* ./include/net/ip.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    ip.h
 * @brief   Internet protocol data structures & APIs.
 *
 * @author	Taehun Kim <kth3321@gmail.com>
 * @version $Id: ip.h 191 2011-06-06 22:14:22Z brian $
 */

#ifndef _IP_H_
#define _IP_H_

#include <net/pbuf.h>
#include <net/ip_addr.h>
#include <net/netif.h>

void ip_init(void);
u8 ip_lookup(void *header, struct netif *inp);
struct netif *ip_route(struct ip_addr *dest);
int ip_input(struct pbuf *p, struct netif *inp);
int ip_output(struct pbuf *p, struct ip_addr *src, struct ip_addr *dest,
		u8 ttl, u8 proto);
int ip_output_if(struct pbuf *p, struct ip_addr *src, struct ip_addr *dest,
		   u8 ttl, u8 proto,
		   struct netif *netif);

#define IP_HLEN 20
#define IP_PROTO_ICMP 1
#define IP_PROTO_UDP 17
#define IP_PROTO_UDPLITE 170
#define IP_PROTO_TCP 6

/* This is passed as the destination address to ip_output_if (not
   to ip_output), meaning that an IP header already is constructed
   in the pbuf. This is used when TCP retransmits. */
#ifdef IP_HDRINCL
#undef IP_HDRINCL
#endif /* IP_HDRINCL */
#define IP_HDRINCL  NULL

struct ip_hdr {
	/* version / header length / type of service */
	u16 _v_hl_tos;
	/* total length */
	u16 _len;
	/* identification */
	u16 _id;
	/* fragment offset field */
	u16 _offset;
#define IP_RF 0x8000        /* reserved fragment flag */
#define IP_DF 0x4000        /* dont fragment flag */
#define IP_MF 0x2000        /* more fragments flag */
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
	/* time to live / protocol*/
	u16 _ttl_proto;
	/* checksum */
	u16 _chksum;
	/* source and destination IP addresses */
	struct ip_addr src;
	struct ip_addr dest; 
} __attribute__((packed));

#define IPH_V(hdr)  (ntohs((hdr)->_v_hl_tos) >> 12)
#define IPH_HL(hdr) ((ntohs((hdr)->_v_hl_tos) >> 8) & 0x0f)
#define IPH_TOS(hdr) htons((ntohs((hdr)->_v_hl_tos) & 0xff))
#define IPH_LEN(hdr) ((hdr)->_len)
#define IPH_ID(hdr) ((hdr)->_id)
#define IPH_OFFSET(hdr) ((hdr)->_offset)
#define IPH_TTL(hdr) (ntohs((hdr)->_ttl_proto) >> 8)
#define IPH_PROTO(hdr) (ntohs((hdr)->_ttl_proto) & 0xff)
#define IPH_CHKSUM(hdr) ((hdr)->_chksum)

#define IPH_VHLTOS_SET(hdr, v, hl, tos) (hdr)->_v_hl_tos = htons(((v) << 12) | ((hl) << 8) | (tos))
#define IPH_LEN_SET(hdr, len) (hdr)->_len = (len)
#define IPH_ID_SET(hdr, id) (hdr)->_id = (id)
#define IPH_OFFSET_SET(hdr, off) (hdr)->_offset = (off)
#define IPH_TTL_SET(hdr, ttl) (hdr)->_ttl_proto = htons(IPH_PROTO(hdr) | ((ttl) << 8))
#define IPH_PROTO_SET(hdr, proto) (hdr)->_ttl_proto = htons((proto) | (IPH_TTL(hdr) << 8))
#define IPH_CHKSUM_SET(hdr, chksum) (hdr)->_chksum = (chksum)

#endif /* _IP_H_ */
