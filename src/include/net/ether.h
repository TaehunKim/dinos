#ifndef _ARP_H_
#define _ARP_H_

#include <arch/types.h>
#include <net/pbuf.h>
#include <net/ip_addr.h>
#include <net/netif.h>

struct eth_addr {
  u8 addr[6];
} __attribute__((packed));
  
struct eth_hdr {
  struct eth_addr dest;
  struct eth_addr src;
  u16 type;
} __attribute__((packed));

struct ethernetif {
	struct eth_addr *ethaddr;
};

#define ARP_TMR_INTERVAL 10000

#define ETHTYPE_ARP 0x0806
#define ETHTYPE_IP  0x0800

/* Initializes ARP. */
void arp_init(void);

/* The arp_tmr() function should be called every ARP_TMR_INTERVAL
   microseconds (10 seconds). This function is responsible for
   expiring old entries in the ARP table. */
void arp_tmr(void);

/* Should be called for all incoming packets of IP kind. The function
   does not alter the packet in any way, it just updates the ARP
   table. After this function has been called, the normal TCP/IP stack
   input function should be called. */
void arp_ip_input(struct netif *netif, struct pbuf *p);

/* Should be called for incoming ARP packets. The pbuf in the argument
   is freed by this function. If the function returns a pbuf (i.e.,
   returns non-NULL), that pbuf constitutes an ARP reply and should be
   sent out on the Ethernet. */
struct pbuf *arp_arp_input(struct netif *netif, struct eth_addr *ethaddr,
			   struct pbuf *p);

/* arp_loopup() is called to do an IP address -> Ethernet address
   translation. If the function returns NULL, there is no mapping and
   the arp_query() function should be called. */
struct eth_addr *arp_lookup(struct ip_addr *ipaddr);

/* Constructs an ARP query packet for the given IP address. The
   function returns a pbuf that contains the reply and that should be
   sent out on the Ethernet. */
struct pbuf *arp_query(struct netif *netif, struct eth_addr *ethaddr,
		       struct ip_addr *ipaddr);

extern void ethernetif_init(struct netif *netif);
extern void  ethernetif_input(struct netif *netif);

#endif /* _ARP_H_ */
