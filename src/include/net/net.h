#ifndef _NET_H_
#define _NET_H_

struct Net {
	int (*init)(void);
	/**< Initialize network module. */
};

extern struct Net net;

#endif /* _NET_H_ */
