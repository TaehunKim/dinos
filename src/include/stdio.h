#ifndef _STDIO_H
#define	_STDIO_H

#include <stdarg.h>
#include <string.h>
#include <dinos/file.h>
#include <dinos/io.h>

#define log(x...)	printf("[DINOS] " MODULE ": " x)

#ifdef DEBUG 
#define DEBUGF(x...)	printf("[DINOS] [DBG] " __FILE__ ": " x)
#else
#define DEBUGF(x...)
#endif

#define	BUFSIZ	4096
#define	EOF		(-1)

#define	FILENAME_MAX	4096

#define STDIN	0
#define STDOUT	1
#define STDERR	2

#ifndef _SIZE_T
#define	_SIZE_T
typedef unsigned int	size_t;	/* type returned by sizeof */
#endif /* _SIZE_T */

extern int sprintf(char *buf, const char *fmt, ...);
extern int vsprintf(char *buf, const char *fmt, va_list args);
extern int vscnprintf(char *buf, size_t size, const char *fmt, va_list args);

extern int printf(const char *fmt, ...);
extern int putc(char c);
extern int puts(const char *str);
extern char getchar(void);

int readline(const char *prompt, char *buf);
void panic(const char *fmt, ...);

struct Console {
    int (*init)(void);
};

extern struct Console Console;

#endif /* _STDIO_H */
