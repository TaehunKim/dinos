#ifndef _CONTEXT_H_
#define _CONTEXT_H_

struct context {
	unsigned int cpsr;
	unsigned int r0;
	unsigned int r1;
	unsigned int r2;
	unsigned int r3;
	unsigned int r4;
	unsigned int r5;
	unsigned int r6;
	unsigned int r7;
	unsigned int r8;
	unsigned int r9;
	unsigned int r10;
	unsigned int r11;
	unsigned int r12;
	/* unsigned int sp; */
	unsigned int lr;
	unsigned int pc;
};

void arch_save_context(void);
void arch_load_context(void);

#endif /* _CONTEXT_H_ */
