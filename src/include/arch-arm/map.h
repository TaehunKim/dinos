#ifndef _MAP_H_
#define _MAP_H_

#include <arch/types.h>

struct map_desc {
    u32 virt;
    u32 phys;
    u32 size;
    u32 type;
};

struct mem_region {
    int count;
    struct map_desc *map;
};

#define MT_DEVICE           0
#define MT_DEVICE_NONSHARED 1
#define MT_DEVICE_CACHED    2
#define MT_DEVICE_WC        3
#define MT_UNCACHED         4
#define MT_CACHECLEAN       5
#define MT_MINICLEAN        6
#define MT_LOW_VECTORS      7
#define MT_HIGH_VECTORS     8
#define MT_MEMORY           9
#define MT_ROM              10
#define MT_MEMORY_NONCACHED	11
#define MT_MEMORY_DTCM      12
#define MT_MEMORY_ITCM      13
#define MT_MEMORY_SO        14

#endif /* _MAP_H_*/
