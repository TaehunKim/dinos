/* PL110 register offsets */
#define CLCD_TIM0       (0x0)
#define CLCD_TIM1       (0x4)
#define CLCD_TIM2       (0x8)
#define CLCD_TIM3       (0xC)
#define CLCD_UPBASE     (0x10)
#define CLCD_LPBASE     (0x14)
#define CLCD_CTRL       (0x18)
#define CLCD_IMSC       (0x1C)
#define CLCD_RIS        (0x20)
#define CLCD_MIS        (0x24)
#define CLCD_ICR        (0x28)
#define CLCD_UPCURR     (0x2C)
#define CLCD_LPCURR     (0x30)
#define CLCD_PALETTE(n) (0x200 + 4 * n)
#define CLCD_CURIMG     (0x800)
#define CLCD_CURCTRL    (0xC00)
#define CLCD_CURCONF    (0xC04)
#define CLCD_CURPAL0    (0xC08)
#define CLCD_CURPAL1    (0xC0C)
#define CLCD_CURXY      (0xC10)
#define CLCD_CURCLIP    (0xC14)
#define CLCD_CURIMSC    (0xC20)
#define CLCD_CURICR     (0xC24)
#define CLCD_CURRIS     (0xC28)
#define CLCD_CURMIS     (0xC2C)
#define CLCD_PERIID0    (0xFE0)
#define CLCD_PERIID1    (0xFE4)
#define CLCD_PERIID2    (0xFE8)
#define CLCD_PERIID3    (0xFEC)
#define CLCD_CELLID0    (0xFF0) 
#define CLCD_CELLID1    (0xFF4) 
#define CLCD_CELLID2    (0xFF8) 
#define CLCD_CELLID3    (0xFFC) 

/* CLCD Control Register Values */
#define CNTL_LCDEN          (1 << 0)
#define CNTL_LCDBPP1        (0 << 1)
#define CNTL_LCDBPP2        (1 << 1)
#define CNTL_LCDBPP4        (2 << 1)
#define CNTL_LCDBPP8        (3 << 1)
#define CNTL_LCDBPP16       (4 << 1)
#define CNTL_LCDBPP16_565   (6 << 1)
#define CNTL_LCDBPP16_444   (7 << 1)
#define CNTL_LCDBPP24       (5 << 1)
#define CNTL_LCDBW          (1 << 4)
#define CNTL_LCDTFT         (1 << 5)
#define CNTL_LCDMONO8       (1 << 6)
#define CNTL_LCDDUAL        (1 << 7)
#define CNTL_BGR            (1 << 8)
#define CNTL_BEBO           (1 << 9)
#define CNTL_BEPO           (1 << 10)
#define CNTL_LCDPWR         (1 << 11)
#define CNTL_LCDVCOMP(x)    ((x) << 12)
#define CNTL_LDMAFIFOTIME   (1 << 15)
#define CNTL_WATERMARK      (1 << 16)

/* Timming 2 values */
#define TIM2_IVS (1 << 11)
#define TIM2_IHS (1 << 12)
#define TIM2_IPC (1 << 13) 
#define TIM2_BCD (1 << 26) 

/* IOCTL commands */
#define CMD_SET_FB  0x0001
#define CMD_GET_FB  0x0002

struct clcd_local {
    int size;
    u32 fb_base;
};

