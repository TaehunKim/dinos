#ifndef _DEV_H_
#define _DEV_H_

#include <arch/types.h>

struct device_desc {
    char *name;
    u32 base_addr;
};

#endif /* _DEV_H_*/
