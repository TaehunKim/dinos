#ifndef _PAGETABLE_H_
#define _PAGETABLE_H_

#ifndef __ASSEM__
#include <arch/types.h>
#include <config.h>
#endif

#define L1_PT_ENTRIES   (4096)
#define L1_PT_SIZE      (16*1024)
#define L1_PT_ALIGN     (4)
#define L2_PT_ENTRIES   (256)
#define L2_PT_SIZE      (1024)

/* Access flags. */
#define ARCH_PERM_KERN       0x100
#define ARCH_PERM_USER_RO    0x200
#define ARCH_PERM_ALL        0x300

#ifndef __ASSEM__

#if CFG_ARM_VERSION >= 7
/* L1 entries */
typedef union {
    /* Page table entry. */
    struct {
        u32 fixed:5;    /* {0,0,0,0,1} */
        u32 domain:4;   /* Memory protection domain info. */
        u32 p:1;        /* ECC enable. Should be zero. */
        u32 base:22;    /* L2 page table base address. */
    }__attribute__((__packed__)) e;

    /* Section entry. */
    struct {
        u32 fixed:2;    /* {1,0} */
        u32 b:1;        /* Buffer */ 
        u32 c:1;        /* Cache */
        u32 xn:1;
        u32 domain:4;
        u32 p:1;
        u32 ap:2;
        u32 tex:3;
        u32 apx:1;
        u32 s:1;
        u32 ng:1;
        u32 fixed2:2;   /* {0,0} */
        u32 base:12;    /* Base address. */
    }__attribute__((__packed__)) se;

    /* Super section entry */
    struct {
        u32 fixed:2;    /* {1,0} */
        u32 b:1;        /* Buffer */ 
        u32 c:1;        /* Cache */
        u32 xn:1;
        u32 domain:4;
        u32 p:1;
        u32 ap:2;
        u32 tex:3;
        u32 apx:1;
        u32 s:1;
        u32 ng:1;
        u32 fixed2:6;   /* {0,0,0,0,0,1} */
        u32 base:8;     /* Base address. */
    }__attribute__((__packed__)) sse;

    u32 entry;
} l1_pte_t;

/* Small page table(L2) entry. */
typedef union {
    struct {
        u32 fixed:2;    /* {1,0} */
        u32 b:1;        /* Buffer */ 
        u32 c:1;        /* Cache */
        u32 ap:2;       /* Access Permission */
        u32 tex:3;      /* Type Extension */
        u32 apx:1;      /* Access Permission 2*/
        u32 s:1;        /* Shareable */
        u32 ng:1;       /* Global */
        u32 base:20;    /* Base address of target physical address. */
    }__attribute__((__packed__)) e;
    u32 entry;
} l2_pte_t;

#else /* Legacy page table */

/* Coarse page table(L1) entry. */
typedef union {
    struct {
        u32 fixed:5;    /* {0,0,0,0,1} */
        u32 domain:4;   /* Memory protection domain info. */
        u32 p:1;        /* ECC enable. Should be zero. */
        u32 base:22;    /* L2 page table base address. */
    }__attribute__((__packed__)) e;
    u32 entry;
} l1_pte_t;

/* Small page) page table(L2) entry. */
typedef union {
    struct {
        u32 fixed:2;    /* {1,0} */
        u32 b:1;        /* Buffer */ 
        u32 c:1;        /* Cache */
        u32 ap0:2;      /* Access Permission */
        u32 ap1:2;      /* Access Permission */
        u32 ap2:2;      /* Access Permission */
        u32 ap3:2;      /* Access Permission */
        u32 base:20;    /* Base address of target physical address. */
    }__attribute__((__packed__)) e;
    u32 entry;
} l2_pte_t;

#endif /* CFG_ARM_VERSION >= 7 */

/* If low 2 bit is 0/0 in L2 page table entry, it is caused of page fault. */

typedef l1_pte_t pte_t;

extern pte_t boot_pt[L1_PT_ENTRIES];

void arch_pt_init(l1_pte_t *pt, int count);
/* Per-page mapping physical address to virtual address. */
int arch_super_section_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag);
int arch_section_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag);
int arch_page_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag);
extern pte_t *arch_cur_pt(void);
extern void arch_set_pt(pte_t *pt);
extern void arch_paging_enable(void);
extern void arch_tlb_clear(void);

#endif /* __ASSEM__ */

#endif
