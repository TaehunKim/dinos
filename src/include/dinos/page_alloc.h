#ifndef _PAGE_ALLOC_H_
#define _PAGE_ALLOC_H_

void page_alloc_init(int fstart_pfn, int rpage_start, int rpage_count);
void *alloc_pages(int pages);
void *alloc_pages_align(int pages, int align);
void free_pages(void *addr);

#endif /* _PAGE_ALLOC_H_ */
