#ifndef _DRIVER_H
#define _DRIVER_H

/**
 * @brief Driver control module.
 */ 
struct Drv {
	int (*init)(void);
	/**< Initilize driver manager module. */
	int (*reg)(struct file_op *f_op, const char *name);
	/**< Register driver. */
};

extern struct Drv Drv;

#endif /* _DRIVER_H */
