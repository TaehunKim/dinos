#ifndef __IRQ_H__
#define __IRQ_H__

#include <config.h>
#include <arch/types.h>

/**
 * @brief The interrupt descript data.
 */ 
struct intr {
	char name[30];
	/**< Interrupt name. */
	void (*isr)(void *arg);
	/**< ISR(Interrupt Service Routine). */
	int used;
	/**< Interrupt used. */
	void *arg;
	/**< Interrupt handler argument. */
};

/**
 * @brief The IRQ module private data.
 */
struct irq_priv {
	int irq_count;
	struct intr irq_tlb[CFG_NR_IRQS];
} irq_priv;

/**
 * @brief The IRQ(Interrupt Request) module.
 */
struct Irq {
	int (*init)(void);
	/**< Initilize irq module. */
	u32 (*save)(void);
	/**< Save flag register & Disable interrupt. */
	void (*restore)(u32 flags);
	/**< Restore flag register & Enable interrupt. */
	void (*disable)(void);
	/**< Disable interrupt. */
	void (*enable)(void);
	/**< Enable interrupt. */
	void (*request)(int irq, const char *name, void (*isr)(void *), void *arg);
	/**< Request a interrupt for run ISR. */
	void (*free)(int irq);
	/**< Free irq. */
	struct intr *(*get)(int irq);
	/**< Get a irq infomation. */
};

extern struct Irq Irq;
#endif /* __IRQ_H__ */
