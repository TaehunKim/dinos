#ifndef _SCHED_H_
#define _SCHED_H_

#include <list.h>
#include <dinos/thread.h>

/**
 * @brief Sched Module.
 */
struct Sched {
    /* Methods. */
    int (*init)(void);
    void (*start)(void);
    void (*schedule)(void);
    void (*add)(struct thread *t);
    void (*del)(struct thread *t);
    struct list_item *(*get_list)(void);
};
extern struct Sched Sched;

/* Depends on architecture. */
void arch_thread_load(struct thread *t);
void arch_context_switch(struct thread *cur, struct thread *next);

/* Global kernel tick. */
extern unsigned int tick;

#endif
