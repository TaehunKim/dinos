#ifndef _TIMER_H_
#define _TIMER_H_

struct timer {
	int (*init)(void);
	int (*start)(void);
	int (*stop)(void);
};

extern struct timer timer;

#endif
