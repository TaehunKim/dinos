#ifndef _PAGING_H_
#define _PAGING_H_

#include <arch/types.h>
#include <arch/paging.h>

/* Access permissions */
#define P_KERN      ARCH_PERM_KERN
#define P_USR_RO    ARCH_PERM_USER_RO
#define P_ALL       ARCH_PERM_ALL

struct Paging {
    void (*init)(void);
    pte_t *(*cur_pt)(void);
    int (*set_pt)(pte_t *pt);
    pte_t *(*alloc_pt)(void);
    int (*enable)(pte_t *pt);
    void (*tlb_clear)(void);
    int (*addr_mapping)(l1_pte_t *pt, addr_t paddr, addr_t vaddr, size_t size, u32 flag, size_t pg_size);
};

extern struct Paging Paging;

#endif /* _PAGING_H_ */
