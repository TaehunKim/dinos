#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <list.h>
#include <dinos/mem.h>

struct thread;

/**
 * @brief Process Control Block(PCB)
 */
struct process {
    int     pid;                /**< Process id */
	char    name[32];           /**< Process name */
	struct  list_item threads;  /**< Thread list head */
	struct  list_item all_item; /**< Process list item */
    struct  vaddr_area vaddr;   /**< Virtual address area */
    pte_t   *pt;                /**< Page table */
    /* TODO: Include the information about the list of open files. */
};

struct Process {
    int (*init)(struct thread *);
    struct process *(*create)(const char *name, int prio, u32 load_addr,
            void *(*main_thread)(void *), void *data);
    struct process *(*cur)(void);
    struct process *(*get)(int pid);
    int (*add_thread)(int pid, struct thread *t);
    int (*exit)(void);
    int (*exit_pid)(int pid);

    int (*exec)(const char *file_name);
};

extern struct Process Process;

#endif /* _PROCESS_H_ */
