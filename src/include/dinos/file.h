#ifndef _FILE_H
#define _FILE_H
#include <config.h>

struct file {
	int ref_count;
	struct file_op *f_op;
	char name[256]; /* TODO: fixed max file name. */
	void *arg;
};

struct file_op {
	int (*open)(struct file *file);
	/**< 파일 열기. */
	int (*close)(struct file *file);
	/**< 파일 닫기. */
	int (*read)(struct file *file, char *ptr, int len);
	/**< 파일 읽기. */
	int (*write)(struct file *file, const char *ptr, int len);
	/**< 파일 쓰기. */
	int (*ioctl)(struct file *file, const unsigned int cmd, void *data);
	/**< I/O 제어. */
};

/**
 * @brief File control module.
 */
struct Vfs {
	int (*init)(void);
	/**< Initilize file manager module. */
	int (*add)(struct file *file);
	/**< 파일 테이블에 새로운 파일 추가. */
	struct file *(*get)(int fd);
	/**< 파일 테이블에서 fd에 해당되는 파일을 얻어옴. */
	int (*open)(const char *name, int flags, int mode);
	/**< 파일 열기. */
	int (*close)(int fd);
	/**< 파일 닫기. */
	int (*read)(int fd, char *ptr, int len);
	/**< 파일 읽기. */
	int (*write)(int fd, const char *ptr, int len);
	/**< 파일 쓰기. */
	int (*ioctl)(int fd, const unsigned int cmd, void *data);
	/**< I/O 제어. */
};

extern struct Vfs Vfs;

#endif /* no _FILE_H */
