#ifndef _IO_H
#define _IO_H

struct Io {
	unsigned char (*readb)(unsigned int addr);
	unsigned short (*readw)(unsigned int addr);
	unsigned int (*readl)(unsigned int addr);
	void (*writeb)(unsigned int addr, unsigned char val);
	void (*writew)(unsigned int addr, unsigned short val);
	void (*writel)(unsigned int addr, unsigned int val);
	void (*readsb)(unsigned int addr, void *buf, int len);
	void (*readsw)(unsigned int addr, void *buf, int len);
	void (*readsl)(unsigned int addr, void *buf, int len);
	void (*writesb)(unsigned int addr, const void *buf, int len);
	void (*writesw)(unsigned int addr, const void *buf, int len);
	void (*writesl)(unsigned int addr, const void *buf, int len);
};
extern struct Io Io;

extern unsigned int sys_base;
extern unsigned int core_base;
extern unsigned int eth_base;

#endif /* _IO_H */
