/* ./include/idt.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    idt.h
 * @license Commercial / GPL dual license
 * @brief   TODO brief documentation here.
 *
 * @author  Myungjun Kim <niduss@gmail.com>
 * @version $Id: idt.h 200 2011-06-13 05:54:59Z brian $
 */

#ifndef _IDT_H
#define _IDT_H

#include <arch/types.h>

struct idt{
    void (*fault)(u32 addr, u32 instr);
    void (*reset)(int no);
    void (*swi)(int no);
    void (*irq)(int no);
    void (*undef)(int no);
    void (*unused)(int no);
};

extern struct idt* idt;

#endif /* no _IDT_H */
