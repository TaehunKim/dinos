#ifndef _SEMA_H_
#define _SEMA_H_

#include <list.h>
#include <dinos/thread.h>

/**
 * @brief	semaphore data structure.
 */
struct semaphore {
    unsigned int count;		/**< Semaphore count. */
    struct list_item wait_head; /**< Semaphore wait list head. */
};

struct semaphore_waiter {
    struct list_item wait_item;
    struct thread *thread;
    int wake;
};

void sema_init(struct semaphore *sem, int count);
void sema_down(struct semaphore *sem);
void sema_up(struct semaphore *sem);

#endif /** _SEMA_H_ */
