#ifndef _THREAD_H_
#define _THREAD_H_

#include <list.h>
#include <dinos/paging.h>

#define DEF_TICK 8
#define DEF_USR_PRIO	100
#define DEF_KER_PRIO	150

struct process;

enum thread_status
{
	THREAD_RUNNING,
	THREAD_READY,
	THREAD_BLOCKED
};

/**
 * @brief Thread Control Block(TCB)
 */
struct thread {
    int cpu_id;                 /**< Current CPU id(for MultiCPU). */
    int tid;                    /**< Thread id. */
    char name[32];              /**< Thread name */
    int prio;                   /**< Priority. */
    unsigned char *stack_top;   /**< Stack top. */
    unsigned int tick;          /**< Alloc tick. */
    enum thread_status status;  /**< Thread status. */
    struct list_item rq_item;   /**< Run queue list item. */
    struct list_item all_item;  /**< All thread list item. */
    void *(*func)(void *);      /**< Kernel Thread. */
    char *heap;
    struct process *proc;    /**< Process of thread. */
};

struct Thread {
    struct thread *(*init)(void);
    struct thread *(*create)(const char *name, int prio, struct process *p,
		  void *(*func)(void *), void *data);
    struct thread *(*cur)(void);
    struct thread *(*get)(int tid);
    int (*exit)(void);
    int (*exit_tid)(int tid);
};
extern struct Thread Thread;

/* Depends on architecture. */
void arch_context_set(struct thread *t, void *(*func)(void *), void *data);
unsigned int arch_get_sp(void);

#endif
