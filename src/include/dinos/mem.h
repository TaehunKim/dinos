#ifndef _MEM_H_
#define _MEM_H_

#include <list.h>
#include <config.h>
#include <rbtree.h>
#include <arch/types.h>
#include <arch/paging.h>

/* System Memory Information. */
#define MEM_SIZE	(CFG_MEM_SIZE)
#define PAGE_SIZE	(CFG_PGSIZE)
#define PAGE_SHIFT	(CFG_PGSHIFT)
#define MAX_PFN		(MEM_SIZE >> PAGE_SHIFT)

/* Flags in memory module. */
#define MEM_KERNEL 0x01 

struct page {
    int owner;
    struct list_item it;    /* List for Memory allocator. */
    int size;               /* Allocatable(Free) size */
};

/* Virtual address area descriptor for each process. */
struct vaddr_area {
    u32 vaddr_start;
    u32 vaddr_end;
    u32 flags;
    struct rb_node vaddr_rb;
};

/* Physical memory area descriptor for the system(global). */
struct mem_area {
    char name[32];
    u32 start_pfn;
    u32 end_pfn;
    u32 auth;
    struct list_item it;
};

#define KERNEL_AUTH_RO 0x0
#define KERNEL_AUTH_RW 0x1
#define USER_AUTH_RO 0x2
#define USER_AUTH_RW 0x3

struct list_item marea_list;

struct mem_info {
    int free_pages;
    int used_pages;
    int fragment_byte;
};

struct Mem {
    struct mem_info info;
    struct list_item mem_areas;

    void (*init)(int rpage_start, int count);
    void *(*alloc_pages)(int order);
    void *(*alloc_pages_align)(int order, int align);
    void (*free_pages)(void *addr);
    void *(*alloc)(int sz, u32 flag);
    void *(*alloc_align)(int sz, int align, u32 flag);
    void (*free)(void *addr);
    void (*free_heap)(void *addr);
    struct mem_info (*status)(void);
};

extern struct Mem Mem;

static inline int is_align(int align, addr_t addr)
{
    return !(addr & (align - 1));
}

static inline u32 p2v(u32 paddr)
{
    return paddr + (CFG_VA_START - CFG_PA_START);
}

static inline u32 v2p(u32 vaddr)
{
    return vaddr + (CFG_PA_START - CFG_VA_START);
}

#endif /* _MEM_H_ */
