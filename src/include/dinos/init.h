#ifndef _INIT_H_
#define _INIT_H_

typedef int (*initcall_t)(void);

#define module_init(fn)                             \
    static initcall_t __initcall_##fn               \
    __attribute__((used, __section__(".initcalls"))) = fn

#endif /* _INIT_H_ */
