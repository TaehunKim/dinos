/* include/list.h
 *
 * Doubly linked list data structures & APIs.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _LIST_H
#define _LIST_H

#include <stddef.h>
#include <arch/types.h>

/**
 * @brief 이중 연결 리스트 아이템.
 */
struct list_item {
	struct list_item *prev, *next;
};

/**
 * @brief 		리스트 초기화.
 * @param[in]	head 리스트 헤드 아이템.
 */
static inline void
list_init(struct list_item *head)
{
	head->next = head;
	head->prev = head;
}

/**
 * @brief		리스트 앞에 새로운 아이템 추가.
 * @param[in]	new 추가 할 아이템.
 * @param[in]	head 리스트 헤드 아이템.
 */
static inline void
list_add(struct list_item *new, struct list_item *head)
{
	new->next = head->next;
	new->prev = head;
	head->next->prev = new;
	head->next = new;
}

/**
 * @brief		리스트 뒤에 새로운 아이템 추가.
 * @param[in]	new 추가 할 아이템.
 * @param[in]	head 리스트 헤드 아이템.
 */
static inline void
list_add_tail(struct list_item *new, struct list_item *head)
{
	new->next = head;
	new->prev = head->prev;
	head->prev->next = new;
	head->prev = new;
}

/**
 * @brief		리스트에서 아이템 삭제.
 * @param[in]	item 삭제 할 아이템.
 */
static inline void
list_del(struct list_item *item)
{
	item->next->prev = item->prev;
	item->prev->next = item->next;
	item->prev = 0;
	item->next = 0;
}

/**
 * @brief		리스트의 앞으로 아이템 이동.
 * @param[in]	item 이동 할 아이템.
 * @param[in]	head 리스트 헤드 아이템.
 */
static inline void
list_move(struct list_item *item, struct list_item *head)
{
	item->next->prev = item->prev;
	item->prev->next = item->next;
	list_add(item, head);
}

/**
 * @brief		리스트의 뒤로 아이템 이동.
 * @param[in]	item 이동 할 아이템.
 * @param[in]	head 리스트 헤드 아이템.
 */
static inline void
list_move_tail(struct list_item *item, struct list_item *head)
{
	item->next->prev = item->prev;
	item->prev->next = item->next;
	list_add_tail(item, head);
}

/**
 * @brief		리스트 아이템을 담고있는 컨테이너를 가져옴.
 * @param[in]	item 컨테이너를 가져올 리스트 아이템 포인터.
 * @param[in]	type 리스트 아이템을 담고 있는 컨테이너 타입.
 * @param[in]	member 컨테이너의 리스트 아이템 멤버이름.
 */
#define list_get_entry(item, type, member)	\
		((type *) ((char *) &(item)->next - offsetof(type, member.next)))

/**
 * @brief		리스트 루프.
 * @param[in]	item 리스트 아이템 포인터.
 * @param[in]	head 헤드 리스트 아이템.
 */
#define list_for_each(item, head)	\
		for (item = (head).next; item != &(head); item = item->next)

/**
 * @brief		리스트 비었는지 여부.
 * @param[in]	head 리스트 아이템 포인터.
 */
#define list_empty(head)	\
		(head == (head)->next)

#endif /* _LIST_H */
