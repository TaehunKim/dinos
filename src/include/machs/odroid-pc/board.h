#ifndef _BOARD_H_
#define _BOARD_H_

#include <arch/types.h>
#include <arch/map.h>

extern struct mem_region board_region;

void board_irq_init(void);

#endif	/* _BOARD_H_ */
