/* ./include/arch-arm/system.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _SYSTEM_H
#define _SYSTEM_H

#define dmb() asm volatile("mcr p15, 0, %0, c7, c10, 5"::"r"(0):"memory");

unsigned int get_phymem_size(void);

/*
 * FIXME: use SCU_CONFIG (SCU_BASE + 0x4)
 *
 * e.g. (copied from linux kernel)
 * unsigned int __init scu_get_core_count(void __iomem *scu_base)
 * {
 *     unsigned int ncores = __raw_readl(scu_base + SCU_CONFIG);
 *     return (ncores & 0x03) + 1;
 * }
 */
#define TOTAL_CPUS      4

#define PRIMARY_CPU     0

static inline int get_ncpu(void)
{
    return TOTAL_CPUS;
}

static inline int get_cpuid(void)
{
    int id;
    asm volatile ("MRC   p15, 0, %0, c0, c0, 5" : "=r" (id));
    return id & 0xf;
}

static inline int is_primary_cpu(void)
{
    return (get_cpuid() == PRIMARY_CPU);
}

#endif /* no _SYSTEM_H */
