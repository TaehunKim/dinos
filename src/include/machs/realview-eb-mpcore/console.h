#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <dinos/file.h>

extern struct file_op drv_pl01_op;

#define drv_console_op drv_pl01_op

#endif	/* _SERIAL_H_ */
