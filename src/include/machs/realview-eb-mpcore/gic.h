/* ./include/arch-arm/gic.h
 *
 * (Write file descrition)
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/**
 * @file    gic.h
 * @brief   General interrupt conteroller register.
 *
 * @author	Taehun Kim <kth3321@gmail.com>
 * @version $Id: gic.h 77 2011-03-28 08:38:47Z brian $
 * @section Source Source:
 * - Source:	Linux(./arch/arm/include/asm/hardware/gic.h)
 * - License:	GPLv2
 */

#ifndef _GIC_H
#define _GIC_H

#define GIC_CPU_CTRL		0x00
#define GIC_CPU_PRIMASK		0x04
#define GIC_CPU_BINPOINT	0x08
#define GIC_CPU_INTACK		0x0c
#define GIC_CPU_EOI			0x10
#define GIC_CPU_RUNNINGPRI	0x14
#define GIC_CPU_HIGHPRI		0x18

#define GIC_DIST_CTRL			0x000
#define GIC_DIST_CTR			0x004
#define GIC_DIST_ENABLE_SET		0x100
#define GIC_DIST_ENABLE_CLEAR	0x180
#define GIC_DIST_PENDING_SET	0x200
#define GIC_DIST_PENDING_CLEAR	0x280
#define GIC_DIST_ACTIVE_BIT		0x300
#define GIC_DIST_PRI			0x400
#define GIC_DIST_TARGET			0x800
#define GIC_DIST_CONFIG			0xc00
#define GIC_DIST_SOFTINT		0xf00

#define GIC_INT_ACK_MASK		0x000003FF
#define EB_GIC_ID				(10 + 32)

#endif /* _GIC_H */
