/* include/machs/realview-eb-mpcore/board.h
 *
 * Realview emulation baseboard memory map.
 *
 * Copyright (C) 2007 ARM Limited
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * From: Linux Kernel V3.1 (arch/arm/mach-realview/include/mach/board-eb.h)
 */

#ifndef _BOARD_H_
#define _BOARD_H_

#include <mach/platform.h>
#include <arch/types.h>
#include <arch/map.h>

/*
 * RealView EB + ARM11MPCore peripheral addresses
 */
#define REALVIEW_EB_UART0_BASE		0x10009000	/* UART 0 */
#define REALVIEW_EB_UART1_BASE		0x1000A000	/* UART 1 */
#define REALVIEW_EB_UART2_BASE		0x1000B000	/* UART 2 */
#define REALVIEW_EB_UART3_BASE		0x1000C000	/* UART 3 */
#define REALVIEW_EB_SSP_BASE		0x1000D000	/* Synchronous Serial Port */
#define REALVIEW_EB_WATCHDOG_BASE	0x10010000	/* watchdog interface */
#define REALVIEW_EB_TIMER0_1_BASE	0x10011000	/* Timer 0 and 1 */
#define REALVIEW_EB_TIMER2_3_BASE	0x10012000	/* Timer 2 and 3 */
#define REALVIEW_EB_GPIO0_BASE		0x10013000	/* GPIO port 0 */
#define REALVIEW_EB_RTC_BASE		0x10017000	/* Real Time Clock */
#define REALVIEW_EB_CLCD_BASE		0x10020000	/* CLCD */
#define REALVIEW_EB_GIC_CPU_BASE	0x10040000	/* Generic interrupt controller CPU interface */
#define REALVIEW_EB_GIC_DIST_BASE	0x10041000	/* Generic interrupt controller distributor */
#define REALVIEW_EB_SMC_BASE		0x10080000	/* Static memory controller */

#define REALVIEW_EB_GIC_CPU_OFFSET	0x00040000
#define REALVIEW_EB_GIC_DIST_OFFSET	0x00041000
#define REALVIEW_EB_TIMER0_1_OFFSET	0x00011000
#define REALVIEW_EB_UART0_OFFSET	0x00009000

#define REALVIEW_EB_FLASH_BASE		0x40000000
#define REALVIEW_EB_FLASH_SIZE		0x04000000
#define REALVIEW_EB_ETH_BASE		0x4E000000	/* Ethernet */
#define REALVIEW_EB_USB_BASE		0x4F000000	/* USB */

#ifdef REALVIEW
#define REALVIEW_EB11MP_SCU_BASE	0x1F000000	/* SCU registers */
#define REALVIEW_EB11MP_GIC_CPU_BASE	0x1F000100	/* Generic interrupt controller CPU interface */
#define REALVIEW_EB11MP_TWD_BASE	0x1F000600
#define REALVIEW_EB11MP_GIC_DIST_BASE	0x1F001000	/* Generic interrupt controller distributor */
#define REALVIEW_EB11MP_L220_BASE	0x1F002000	/* L220 registers */
#define REALVIEW_EB11MP_SYS_PLD_CTRL1	0x74		/* Register offset for MPCore sysctl */
#else /* The register map for QEMU. */
#define REALVIEW_EB11MP_SCU_BASE	0x10100000	/* SCU registers */
#define REALVIEW_EB11MP_GIC_CPU_BASE	0x10100100	/* Generic interrupt controller CPU interface */
#define REALVIEW_EB11MP_TWD_BASE	0x10100600
#define REALVIEW_EB11MP_GIC_DIST_BASE	0x10101000	/* Generic interrupt controller distributor */
#define REALVIEW_EB11MP_L220_BASE	0x10102000	/* L220 registers */
#define REALVIEW_EB11MP_SYS_PLD_CTRL1	0x74		/* Register offset for MPCore sysctl */
#endif

#define REALVIEW_EB11MP_GIC_CPU_OFFSET	0x00000100
#define REALVIEW_EB11MP_GIC_DIST_OFFSET	0x00001000

/*
 * Core tile identification (REALVIEW_SYS_PROCID)
 */
#define REALVIEW_EB_PROC_MASK		0xFF000000
#define REALVIEW_EB_PROC_ARM7TDMI	0x00000000
#define REALVIEW_EB_PROC_ARM9		0x02000000
#define REALVIEW_EB_PROC_ARM11		0x04000000
#define REALVIEW_EB_PROC_ARM11MP	0x06000000
#define REALVIEW_EB_PROC_A9MP		0x0C000000

#define check_eb_proc(proc_type)						\
	((readl(__io_address(REALVIEW_SYS_PROCID)) & REALVIEW_EB_PROC_MASK)	\
	 == proc_type)

#ifdef CONFIG_REALVIEW_EB_ARM11MP
#define core_tile_eb11mp()	check_eb_proc(REALVIEW_EB_PROC_ARM11MP)
#else
#define core_tile_eb11mp()	0
#endif

#ifdef CONFIG_REALVIEW_EB_A9MP
#define core_tile_a9mp()	check_eb_proc(REALVIEW_EB_PROC_A9MP)
#else
#define core_tile_a9mp()	0
#endif

#define machine_is_realview_eb_mp() \
	(machine_is_realview_eb() && (core_tile_eb11mp() || core_tile_a9mp()))

extern u32 sys_base;
extern u32 core_base;
extern u32 eth_base;
extern u32 eb_gic_base;
extern u32 core_gic_base;
extern struct mem_region board_region;

void board_irq_init(void);

#endif	/* _BOARD_H_ */
