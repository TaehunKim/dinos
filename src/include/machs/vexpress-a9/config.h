#ifndef _CONFIG_H_
#define _CONFIG_H_

#define CFG_ARM_VERSION         7               /* ARMv7 */
#define CFG_MEM_SIZE            (128*1024*1024)
#define CFG_KSTACK_SIZE         0x1000
#define CFG_PA_START            0x60000000      /* Memory start physical address. */
#define CFG_VA_START            0x80000000      /* Kernel start virtual address. */
#define CFG_PA_KSTART           0x60000000      /* Kernal start physical address. */

#define CFG_PROMPT              "ksh #"
//#define CFG_CBSIZE              256
#define CFG_CBSIZE              1024 
#define CFG_PBSIZE              (CFG_CBSIZE+sizeof(CFG_PROMPT)+16)

#define CFG_PGSIZE              4096            /* Memory 1 page size. */
#define CFG_PGSHIFT             12

#define CFG_MAX_OPEN            32
#define CFG_MAX_FILES           128

#define CFG_NR_IRQS             255

#define CFG_HZ                  1000000         /* One second clock. */
//#define CFG_TIMER_HZ          100             /* Timer HZ(HZ/100). */
#define CFG_TIMER_HZ            1000            /* Timer HZ(HZ/1000). */

#define CFG_IO_START_ADDR       0xF8000000      /* IO mapping range start. */
#define CFG_IO_END_ADDR         0xFFFFFFFF      /* IO mapping range end. */
#define CFG_IO_MAX              1024

#define BIG_ENDIAN              4321
#define LITTLE_ENDIAN           1234
#undef	BYTE_ORDER
#define BYTE_ORDER              LITTLE_ENDIAN

/* Network configuration. */
#define CFG_IPADDR              0xC0A80102      /* 192.168.1.2 */
#define CFG_GWADDR              0xC0A80101      /* 192.168.1.1 */
#define CFG_NETMASK             0xFFFFFF00      /* 255.255.255.0 */

/* Paging. */
#define CFG_PAGING_STEP         2

#endif /* _CONFIG_H_*/
