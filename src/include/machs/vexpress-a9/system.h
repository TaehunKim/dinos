/* include/machs/odroid-pc/system.h
 *
 * 
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _SYSTEM_H
#define _SYSTEM_H

#define dmb() asm volatile("mcr p15, 0, %0, c7, c10, 5"::"r"(0):"memory");

#define TOTAL_CPUS      2
#define PRIMARY_CPU     0

static inline int get_ncpu(void)
{
    return TOTAL_CPUS;
}

static inline int get_cpuid(void)
{
    int id;
    asm volatile ("MRC   p15, 0, %0, c0, c0, 5" : "=r" (id));
    return id & 0xf;
}

static inline int is_primary_cpu(void)
{
    return (get_cpuid() == PRIMARY_CPU);
}

#endif /* no _SYSTEM_H */
