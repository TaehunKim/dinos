#ifndef _BOARD_H_
#define _BOARD_H_

#include <arch/types.h>
#include <arch/map.h>
#include <mach/motherboard.h>
#include <mach/ct-ca9x4.h>

extern struct mem_region board_region;

void board_irq_init(void);

/* Board dependent device base address */
#define ETH_BASE    V2M_LAN9118
#define CLCD_BASE   CT_CA9X4_CLCDC

#define ETH_IRQ		IRQ_V2M_LAN9118

#endif	/* _BOARD_H_ */
