#!/bin/sh
SZ=$(stat -c %s $1.bin)
printf "0: %.8x" $SZ | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r -g0 >> $1.exe
cat $1.bin >> $1.exe
