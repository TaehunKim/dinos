/* net/tcp.c
 *
 * Transmission Control Protocol.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <memory.h>
#include <net/tcp.h>

#ifdef TCP_DEBUG
#define DEBUGF printf
#else
#define DEBUGF
#endif

u32 tcp_ticks;
const u8 tcp_backoff[13] = { 1, 2, 4, 8, 16, 32, 64, 64, 64, 64, 64, 64, 64 };

/* The TCP PCB lists. */
struct tcp_pcb_listen *tcp_listen_pcbs;  /* List of all TCP PCBs in LISTEN state. */
struct tcp_pcb *tcp_active_pcbs;  /* List of all TCP PCBs that are in a
                                     state in which they accept or send
                                     data. */
struct tcp_pcb *tcp_tw_pcbs;      /* List of all TCP PCBs in TIME-WAIT. */
struct tcp_pcb *tcp_tmp_pcb;

#define MIN(x,y) (x) < (y)? (x): (y)

static u8 tcp_timer;

void
tcp_init(void)
{
    /* Clear globals. */
    tcp_listen_pcbs = NULL;
    tcp_active_pcbs = NULL;
    tcp_tw_pcbs = NULL;
    tcp_tmp_pcb = NULL;

    /* initialize timer */
    tcp_ticks = 0;
    tcp_timer = 0;
}

void
tcp_tmr()
{
    ++tcp_timer;
    if(tcp_timer == 10) {
        tcp_timer = 0;
    }

    if(tcp_timer & 1) {
        /* Call tcp_fasttmr() every 200 ms, i.e., every other timer
           tcp_tmr() is called. */
        tcp_fasttmr();
    }
    if(tcp_timer == 0 || tcp_timer == 5) {
        /* Call tcp_slowtmr() every 500 ms, i.e., every fifth timer
           tcp_tmr() is called. */
        tcp_slowtmr();
    }
}

int
tcp_close(struct tcp_pcb *pcb)
{
    int err;

#if TCP_DEBUG
    DEBUGF("tcp_close: closing in state ");
    tcp_debug_print_state(pcb->state);
    DEBUGF("\r\n");
#endif /* TCP_DEBUG */
    switch(pcb->state) {
        case LISTEN:
            err = 0;
            tcp_pcb_remove((struct tcp_pcb **)&tcp_listen_pcbs, pcb);
            mm->free(pcb);
            pcb = NULL;
            break;
        case SYN_SENT:
            err = 0;
            tcp_pcb_remove(&tcp_active_pcbs, pcb);
            mm->free(pcb);
            pcb = NULL;
            break;
        case SYN_RCVD:
            err = tcp_send_ctrl(pcb, TCP_FIN);
            if(err == 0) {
                pcb->state = FIN_WAIT_1;
            }
            break;
        case ESTABLISHED:
            err = tcp_send_ctrl(pcb, TCP_FIN);
            if(err == 0) {
                pcb->state = FIN_WAIT_1;
            }
            break;
        case CLOSE_WAIT:
            err = tcp_send_ctrl(pcb, TCP_FIN);
            if(err == 0) {
                pcb->state = LAST_ACK;
            }
            break;
        default:
            /* Has already been closed, do nothing. */
            err = 0;
            pcb = NULL;
            break;
    }

    if(pcb != NULL && err == 0) {
        err = tcp_output(pcb);
    }
    return err;
}

void
tcp_abort(struct tcp_pcb *pcb)
{
    u32 seqno, ackno;
    u16 remote_port, local_port;
    struct ip_addr remote_ip, local_ip;
    void (*errf)(void *arg, int err);
    void *errf_arg;

    /* Figure out on which TCP PCB list we are, and remove us. If we
       are in an active state, call the receive function associated with
       the PCB with a NULL argument, and send an RST to the remote end. */
    if(pcb->state == TIME_WAIT) {
        tcp_pcb_remove(&tcp_tw_pcbs, pcb);
        mm->free(pcb);
    } else if(pcb->state == LISTEN) {
        tcp_pcb_remove((struct tcp_pcb **)&tcp_listen_pcbs, pcb);
        mm->free(pcb);
    } else {
        seqno = pcb->snd_nxt;
        ackno = pcb->rcv_nxt;
        ip_addr_set(&local_ip, &(pcb->local_ip));
        ip_addr_set(&remote_ip, &(pcb->remote_ip));
        local_port = pcb->local_port;
        remote_port = pcb->remote_port;
        errf = pcb->errf;
        errf_arg = pcb->callback_arg;
        tcp_pcb_remove(&tcp_active_pcbs, pcb);
        mm->free(pcb);
        if(errf != NULL) {
            //errf(errf_arg, ERR_ABRT);
            errf(errf_arg, -1);
        }
        DEBUGF("tcp_abort: sending RST\r\n");
        tcp_rst(seqno, ackno, &local_ip, &remote_ip, local_port, remote_port);
    }
}

int
tcp_bind(struct tcp_pcb *pcb, struct ip_addr *ipaddr, u16 port)
{
    struct tcp_pcb *cpcb;

    /* Check if the address already is in use. */
    for(cpcb = (struct tcp_pcb *)tcp_listen_pcbs;
        cpcb != NULL; cpcb = cpcb->next) {
        if(cpcb->local_port == port) {
            if(ip_addr_isany(&(cpcb->local_ip)) ||
               ip_addr_isany(ipaddr) ||
               ip_addr_cmp(&(cpcb->local_ip), ipaddr)) {
                //return ERR_USE;
                return -1;
            }
        }
    }

    for(cpcb = tcp_active_pcbs;
        cpcb != NULL; cpcb = cpcb->next) {
        if(cpcb->local_port == port) {
            if(ip_addr_isany(&(cpcb->local_ip)) ||
               ip_addr_isany(ipaddr) ||
               ip_addr_cmp(&(cpcb->local_ip), ipaddr)) {
                //return ERR_USE;
                return -1;
            }
        }
    }

    if(!ip_addr_isany(ipaddr)) {
        pcb->local_ip = *ipaddr;
    }
    pcb->local_port = port;
    DEBUGF("tcp_bind: bind to port %d\r\n", port);
    return 0;
}

struct tcp_pcb *
tcp_listen(struct tcp_pcb *pcb)
{
    pcb->state = LISTEN;
    DEBUGF("%s: need reallocation.\r\n");
    /* TODO: realloc 구현. */
#if 0
    pcb = memp_realloc(MEMP_TCP_PCB, MEMP_TCP_PCB_LISTEN, pcb);
    if(pcb == NULL) {
        return NULL;
    }
#endif
    TCP_REG((struct tcp_pcb **)&tcp_listen_pcbs, pcb);
    return pcb;
}

void
tcp_recved(struct tcp_pcb *pcb, u16 len)
{
    pcb->rcv_wnd += len;
    if(pcb->rcv_wnd > TCP_WND) {
       pcb->rcv_wnd = TCP_WND;
    }
    if(!(pcb->flags & TF_ACK_DELAY) ||
       !(pcb->flags & TF_ACK_NOW)) {
        tcp_ack(pcb);
    }
    DEBUGF("tcp_recved: recveived %d bytes, wnd %u (%u).\r\n",
            len, pcb->rcv_wnd, TCP_WND - pcb->rcv_wnd);
}

static u16
tcp_new_port(void)
{
    struct tcp_pcb *pcb;
    static u16 port = 4096;

again:
    if(++port > 0x7fff) {
        port = 4096;
    }

    for(pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next) {
        if(pcb->local_port == port) {
            goto again;
        }
    }
    for(pcb = tcp_tw_pcbs; pcb != NULL; pcb = pcb->next) {
        if(pcb->local_port == port) {
            goto again;
        }
    }
    for(pcb = (struct tcp_pcb *)tcp_listen_pcbs; pcb != NULL; pcb = pcb->next) {
        if(pcb->local_port == port) {
            goto again;
        }
    }
    return port;
}

int
tcp_connect(struct tcp_pcb *pcb, struct ip_addr *ipaddr, u16 port,
            int (* connected)(void *arg, struct tcp_pcb *tpcb, int err))
{
    u32 optdata;
    int ret;
    u32 iss;

    DEBUGF("tcp_connect to port %d\r\n", port);
    if(ipaddr != NULL) {
        pcb->remote_ip = *ipaddr;
    } else {
        //return ERR_VAL;
        return -1;
    }
    pcb->remote_port = port;
    if(pcb->local_port == 0) {
        pcb->local_port = tcp_new_port();
    }
    iss = tcp_next_iss();
    pcb->rcv_nxt = 0;
    pcb->snd_nxt = iss;
    pcb->lastack = iss - 1;
    pcb->snd_lbb = iss - 1;
    pcb->rcv_wnd = TCP_WND;
    pcb->snd_wnd = TCP_WND;
    pcb->mss = TCP_MSS;
    pcb->cwnd = 1;
    pcb->ssthresh = pcb->mss * 10;
    pcb->state = SYN_SENT;
    pcb->connected = connected;
    TCP_REG(&tcp_active_pcbs, pcb);

    /* Build an MSS option */
    optdata = htonl(((u32)2 << 24) | 
            ((u32)4 << 16) | 
            (((u32)pcb->mss / 256) << 8) |
            (pcb->mss & 255));

    ret = tcp_enqueue(pcb, NULL, 0, TCP_SYN, 0, (u8 *)&optdata, 4);
    if(ret == 0) { 
        tcp_output(pcb);
    }
    return ret;
} 

void
tcp_slowtmr(void)
{
    static struct tcp_pcb *pcb, *pcb2, *prev;
    static struct tcp_seg *seg, *useg;
    static u32 eff_wnd;
    static u8 pcb_remove;   /* flag if a PCB should be removed */

    ++tcp_ticks;

    /* Steps through all of the active PCBs. */
    prev = NULL;
    pcb = tcp_active_pcbs;
    while(pcb != NULL) {
        //ASSERT("tcp_timer_coarse: active pcb->state != CLOSED", pcb->state != CLOSED);
        //ASSERT("tcp_timer_coarse: active pcb->state != LISTEN", pcb->state != LISTEN);
        //ASSERT("tcp_timer_coarse: active pcb->state != TIME-WAIT", pcb->state != TIME_WAIT);

        pcb_remove = 0;

        if(pcb->state == SYN_SENT && pcb->nrtx == TCP_SYNMAXRTX) {
            ++pcb_remove;
        } else if(pcb->nrtx == TCP_MAXRTX) {
            ++pcb_remove;
        } else {
            ++pcb->rtime;
            seg = pcb->unacked;
            if(seg != NULL && pcb->rtime >= pcb->rto) {

                DEBUGF("tcp_timer_coarse: rtime %ld pcb->rto %d\r\n",
                        tcp_ticks - pcb->rtime, pcb->rto);

                /* Double retransmission time-out unless we are trying to
                   connect to somebody (i.e., we are in SYN_SENT). */
                if(pcb->state != SYN_SENT) {
                    pcb->rto = ((pcb->sa >> 3) + pcb->sv) << tcp_backoff[pcb->nrtx];
                }

                /* Move all other unacked segments to the unsent queue. */
                if(seg->next != NULL) {
                    for(useg = seg->next; useg->next != NULL; useg = useg->next);
                    /* useg now points to the last segment on the unacked queue. */
                    useg->next = pcb->unsent;
                    pcb->unsent = seg->next;
                    seg->next = NULL;
                    pcb->snd_nxt = ntohl(pcb->unsent->tcphdr->seqno);
                }

                /* Do the actual retransmission. */
                tcp_rexmit_seg(pcb, seg);

                /* Reduce congestion window and ssthresh. */
                eff_wnd = MIN(pcb->cwnd, pcb->snd_wnd);
                pcb->ssthresh = eff_wnd >> 1;
                if(pcb->ssthresh < pcb->mss) {
                    pcb->ssthresh = pcb->mss * 2;
                }
                pcb->cwnd = pcb->mss;

                DEBUGF("tcp_rexmit_seg: cwnd %u ssthresh %u\r\n",
                        pcb->cwnd, pcb->ssthresh);
            }
        }

        /* Check if this PCB has stayed too long in FIN-WAIT-2 */
        if(pcb->state == FIN_WAIT_2) {
            if((u32_t)(tcp_ticks - pcb->tmr) >
               TCP_FIN_WAIT_TIMEOUT / TCP_SLOW_INTERVAL) {
                ++pcb_remove;
            }
        }

        /* If this PCB has queued out of sequence data, but has been
           inactive for too long, will drop the data (it will eventually
           be retransmitted). */
#if TCP_QUEUE_OOSEQ    
        if(pcb->ooseq != NULL &&
           (u32_t)tcp_ticks - pcb->tmr >=
           pcb->rto * TCP_OOSEQ_TIMEOUT) {
            tcp_segs_free(pcb->ooseq);
            pcb->ooseq = NULL;
        }
#endif /* TCP_QUEUE_OOSEQ */

        /* Check if this PCB has stayed too long in SYN-RCVD */
        if(pcb->state == SYN_RCVD) {
            if((u32)(tcp_ticks - pcb->tmr) > TCP_SYN_RCVD_TIMEOUT / TCP_SLOW_INTERVAL) {
                ++pcb_remove;
            }
        }

        /* If the PCB should be removed, do it. */
        if(pcb_remove) {
            tcp_pcb_purge(pcb);      
            /* Remove PCB from tcp_active_pcbs list. */
            if(prev != NULL) {
                //ASSERT("tcp_timer_coarse: middle tcp != tcp_active_pcbs", pcb != tcp_active_pcbs);
                prev->next = pcb->next;
            } else {
                /* This PCB was the first. */
                //ASSERT("tcp_timer_coarse: first pcb == tcp_active_pcbs", tcp_active_pcbs == pcb);
                tcp_active_pcbs = pcb->next;
            }

            if(pcb->errf != NULL) {
                //pcb->errf(pcb->callback_arg, ERR_ABRT);
                pcb->errf(pcb->callback_arg, -1);
            }

            pcb2 = pcb->next;
            mm->free(pcb);
            pcb = pcb2;
        } else {
            /* We check if we should poll the connection. */
            ++pcb->polltmr;
            if(pcb->polltmr >= pcb->pollinterval &&
               pcb->poll != NULL) {
                pcb->polltmr = 0;
                pcb->poll(pcb->callback_arg, pcb);
                tcp_output(pcb);
            }
            prev = pcb;
            pcb = pcb->next;
        }
    }

    /* Steps through all of the TIME-WAIT PCBs. */
    prev = NULL;    
    pcb = tcp_tw_pcbs;
    while(pcb != NULL) {
        //ASSERT("tcp_timer_coarse: TIME-WAIT pcb->state == TIME-WAIT", pcb->state == TIME_WAIT);
        pcb_remove = 0;

        /* Check if this PCB has stayed long enough in TIME-WAIT */
        if((u32)(tcp_ticks - pcb->tmr) > 2 * TCP_MSL / TCP_SLOW_INTERVAL) {
            ++pcb_remove;
        }

        /* If the PCB should be removed, do it. */
        if(pcb_remove) {
            tcp_pcb_purge(pcb);      
            /* Remove PCB from tcp_tw_pcbs list. */
            if(prev != NULL) {
                //ASSERT("tcp_timer_coarse: middle tcp != tcp_tw_pcbs", pcb != tcp_tw_pcbs);
                prev->next = pcb->next;
            } else {
                /* This PCB was the first. */
                //ASSERT("tcp_timer_coarse: first pcb == tcp_tw_pcbs", tcp_tw_pcbs == pcb);
                tcp_tw_pcbs = pcb->next;
            }
            pcb2 = pcb->next;
            mm->free(pcb);
            pcb = pcb2;
        } else {
            prev = pcb;
            pcb = pcb->next;
        }
    }
}

void
tcp_fasttmr(void)
{
    struct tcp_pcb *pcb;

    /* send delayed ACKs */  
    for(pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next) {
        if(pcb->flags & TF_ACK_DELAY) {
            DEBUGF("tcp_timer_fine: delayed ACK\r\n");
            tcp_ack_now(pcb);
            pcb->flags &= ~(TF_ACK_DELAY | TF_ACK_NOW);
        }
    }
}

u8
tcp_segs_free(struct tcp_seg *seg)
{
    u8 count = 0;
    struct tcp_seg *next;
again:  
    if(seg != NULL) {
        next = seg->next;
        count += tcp_seg_free(seg);
        seg = next;
        goto again;
    }
    return count;
}

u8
tcp_seg_free(struct tcp_seg *seg)
{
    u8 count = 0;

    if(seg != NULL) {
        if(seg->p == NULL) {
            mm->free(seg);
        } else {
            count = pbuf_free(seg->p);
#if TCP_DEBUG
            seg->p = NULL;
#endif /* TCP_DEBUG */
            mm->free(seg);
        }
    }
    return count;
}

struct tcp_seg *
tcp_seg_copy(struct tcp_seg *seg)
{
    struct tcp_seg *cseg;

    cseg = mm->malloc(sizeof(struct tcp_seg));
    if(cseg == NULL) {
        return NULL;
    }
    bcopy(seg, cseg, sizeof(struct tcp_seg));
    pbuf_ref(cseg->p);
    return cseg;
}

struct tcp_pcb *
tcp_new(void)
{
    struct tcp_pcb *pcb;
    u32 iss;

    pcb = mm->malloc(sizeof(struct tcp_pcb));
    if(pcb != NULL) {
        bzero(pcb, sizeof(struct tcp_pcb));
        pcb->snd_buf = TCP_SND_BUF;
        pcb->snd_queuelen = 0;
        pcb->rcv_wnd = TCP_WND;
        pcb->mss = TCP_MSS;
        pcb->rto = 3000 / TCP_SLOW_INTERVAL;
        pcb->sa = 0;
        pcb->sv = 3000 / TCP_SLOW_INTERVAL;
        pcb->rtime = 0;
        pcb->cwnd = 1;
        iss = tcp_next_iss();
        pcb->snd_wl2 = iss;
        pcb->snd_nxt = iss;
        pcb->snd_max = iss;
        pcb->lastack = iss;
        pcb->snd_lbb = iss;   
        pcb->tmr = tcp_ticks;

        pcb->polltmr = 0;

        return pcb;
    }
    return NULL;
}

void
tcp_arg(struct tcp_pcb *pcb, void *arg)
{  
    pcb->callback_arg = arg;
}

void
tcp_recv(struct tcp_pcb *pcb,
         int (*recv)(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, int err))
{
    pcb->recv = recv;
}

void
tcp_sent(struct tcp_pcb *pcb,
         int (* sent)(void *arg, struct tcp_pcb *tpcb, u16 len))
{
    pcb->sent = sent;
}

void
tcp_err(struct tcp_pcb *pcb,
        void (*errf)(void *arg, int err))
{
    pcb->errf = errf;
}

void
tcp_poll(struct tcp_pcb *pcb,
         int (* poll)(void *arg, struct tcp_pcb *tpcb), u8 interval)
{
    pcb->poll = poll;
    pcb->pollinterval = interval;
}

void
tcp_accept(struct tcp_pcb *pcb,
           int (*accept)(void *arg, struct tcp_pcb *newpcb, int err))
{
    pcb->accept = accept;
}

void
tcp_pcb_purge(struct tcp_pcb *pcb)
{
    if(pcb->state != CLOSED &&
       pcb->state != TIME_WAIT &&
       pcb->state != LISTEN) {

#if TCP_DEBUG
        if(pcb->unsent != NULL) {    
            DEBUGF("tcp_pcb_purge: not all data sent\r\n");
        }
        if(pcb->unacked != NULL) {    
            DEBUGF("tcp_pcb_purge: data left on ->unacked\r\n");
        }
        if(pcb->ooseq != NULL) {    
            DEBUGF("tcp_pcb_purge: data left on ->ooseq\r\n");
        }
#endif /* TCP_DEBUG */
        tcp_segs_free(pcb->unsent);
#if TCP_QUEUE_OOSEQ
        tcp_segs_free(pcb->ooseq);
#endif /* TCP_QUEUE_OOSEQ */
        tcp_segs_free(pcb->unacked);
        pcb->unacked = pcb->unsent =
#if TCP_QUEUE_OOSEQ
            pcb->ooseq =
#endif /* TCP_QUEUE_OOSEQ */
        NULL;
    }
}

void
tcp_pcb_remove(struct tcp_pcb **pcblist, struct tcp_pcb *pcb)
{
    TCP_RMV(pcblist, pcb);

    tcp_pcb_purge(pcb);

    /* if there is an outstanding delayed ACKs, send it */
    if(pcb->state != TIME_WAIT &&
            pcb->state != LISTEN &&
            pcb->flags & TF_ACK_DELAY) {
        pcb->flags |= TF_ACK_NOW;
        tcp_output(pcb);
    }  
    pcb->state = CLOSED;

    //ASSERT("tcp_pcb_remove: tcp_pcbs_sane()", tcp_pcbs_sane());
}

u32
tcp_next_iss(void)
{
    static u32 iss = 6510;

    iss += tcp_ticks;       /* XXX */
    return iss;
}

#if TCP_DEBUG
void
tcp_debug_print(struct tcp_hdr *tcphdr)
{
    DEBUGF("TCP header:\r\n");
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|      %04x     |      %04x     | (src port, dest port)\r\n",
            tcphdr->src, tcphdr->dest);
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|            %08lu           | (seq no)\r\n",
            tcphdr->seqno);
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|            %08lu           | (ack no)\r\n",
            tcphdr->ackno);
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("| %2d |    |%d%d%d%d%d|    %5d      | (offset, flags (",
            TCPH_FLAGS(tcphdr) >> 4 & 1,
            TCPH_FLAGS(tcphdr) >> 4 & 1,
            TCPH_FLAGS(tcphdr) >> 3 & 1,
            TCPH_FLAGS(tcphdr) >> 2 & 1,
            TCPH_FLAGS(tcphdr) >> 1 & 1,
            TCPH_FLAGS(tcphdr) & 1,
            tcphdr->wnd);
    tcp_debug_print_flags(TCPH_FLAGS(tcphdr));
    DEBUGF("), win)\r\n");
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|    0x%04x     |     %5d     | (chksum, urgp)\r\n",
            ntohs(tcphdr->chksum), ntohs(tcphdr->urgp));
    DEBUGF("+-------------------------------+\r\n");
}

void
tcp_debug_print_state(enum tcp_state s)
{
    DEBUGF("State: ");
    switch(s) {
        case CLOSED:
            DEBUGF("CLOSED\r\n");
            break;
        case LISTEN:
            DEBUGF("LISTEN\r\n");
            break;
        case SYN_SENT:
            DEBUGF("SYN_SENT\r\n");
            break;
        case SYN_RCVD:
            DEBUGF("SYN_RCVD\r\n");
            break;
        case ESTABLISHED:
            DEBUGF("ESTABLISHED\r\n");
            break;
        case FIN_WAIT_1:
            DEBUGF("FIN_WAIT_1\r\n");
            break;
        case FIN_WAIT_2:
            DEBUGF("FIN_WAIT_2\r\n");
            break;
        case CLOSE_WAIT:
            DEBUGF("CLOSE_WAIT\r\n");
            break;
        case CLOSING:
            DEBUGF("CLOSING\r\n");
            break;
        case LAST_ACK:
            DEBUGF("LAST_ACK\r\n");
            break;
        case TIME_WAIT:
            DEBUGF("TIME_WAIT\r\n");
            break;
    }
}

void
tcp_debug_print_flags(u8 flags)
{
    if(flags & TCP_FIN) {
        DEBUGF("FIN ");
    }
    if(flags & TCP_SYN) {
        DEBUGF("SYN ");
    }
    if(flags & TCP_RST) {
        DEBUGF("RST ");
    }
    if(flags & TCP_PSH) {
        DEBUGF("PSH ");
    }
    if(flags & TCP_ACK) {
        DEBUGF("ACK ");
    }
    if(flags & TCP_URG) {
        DEBUGF("URG ");
    }
}

void
tcp_debug_print_pcbs(void)
{
    struct tcp_pcb *pcb;
    DEBUGF("Active PCB states:\r\n");
    for(pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next) {
        DEBUGF("Local port %d, foreign port %d snd_nxt %lu rcv_nxt %lu ",
                pcb->local_port, pcb->remote_port,
                pcb->snd_nxt, pcb->rcv_nxt);
        tcp_debug_print_state(pcb->state);
    }    
    DEBUGF("Listen PCB states:\r\n");
    for(pcb = (struct tcp_pcb *)tcp_listen_pcbs; pcb != NULL; pcb = pcb->next) {
        DEBUGF("Local port %d, foreign port %d snd_nxt %lu rcv_nxt %lu ",
                pcb->local_port, pcb->remote_port,
                pcb->snd_nxt, pcb->rcv_nxt);
        tcp_debug_print_state(pcb->state);
    }    
    DEBUGF("TIME-WAIT PCB states:\r\n");
    for(pcb = tcp_tw_pcbs; pcb != NULL; pcb = pcb->next) {
        DEBUGF("Local port %d, foreign port %d snd_nxt %lu rcv_nxt %lu ",
                pcb->local_port, pcb->remote_port,
                pcb->snd_nxt, pcb->rcv_nxt);
        tcp_debug_print_state(pcb->state);
    }    
}

int
tcp_pcbs_sane(void)
{
    struct tcp_pcb *pcb;
    for(pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next) {
        //ASSERT("tcp_pcbs_sane: active pcb->state != CLOSED", pcb->state != CLOSED);
        //ASSERT("tcp_pcbs_sane: active pcb->state != LISTEN", pcb->state != LISTEN);
        //ASSERT("tcp_pcbs_sane: active pcb->state != TIME-WAIT", pcb->state != TIME_WAIT);
    }
    for(pcb = tcp_tw_pcbs; pcb != NULL; pcb = pcb->next) {
        //ASSERT("tcp_pcbs_sane: tw pcb->state == TIME-WAIT", pcb->state == TIME_WAIT);
    }
    for(pcb = (struct tcp_pcb *)tcp_listen_pcbs; pcb != NULL; pcb = pcb->next) {
        //ASSERT("tcp_pcbs_sane: listen pcb->state == LISTEN", pcb->state == LISTEN);
    }
    return 1;
}
#endif /* TCP_DEBUG */
