/* net/ether.c
 *
 * Ethernet protocol.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <errno.h>
#include <delay.h>
#include <dinos/mem.h>
#include <dinos/file.h>
#include <dinos/thread.h>
#include <net/pbuf.h>
#include <net/ether.h>
#include <net/inet.h>

static const struct eth_addr ethbroadcast = {
	.addr = {0xff,0xff,0xff,0xff,0xff,0xff},
};

/* Forward declarations. */
//static void  ethernetif_input(struct netif *netif);
static int ethernetif_output(struct netif *netif, struct pbuf *p,
		struct ip_addr *ipaddr);

static void
low_level_init(struct netif *netif)
{
	struct ethernetif *ethernetif;

	ethernetif = netif->state;

	netif->netfd = Vfs.open("/dev/eth", 0, 0);
	if (netif->netfd < 0) return;

	/* Obtain MAC address from network interface. */
	Vfs.ioctl(netif->netfd, CMD_GET_HWADDR, ethernetif->ethaddr->addr);

	DEBUGF("%s: ethernet addr = %.2X:%.2X:%.2X:%.2X:%.2X:%.2X\r\n", 
			__func__,
			ethernetif->ethaddr->addr[0],
			ethernetif->ethaddr->addr[1],
			ethernetif->ethaddr->addr[2],
			ethernetif->ethaddr->addr[3],
			ethernetif->ethaddr->addr[4],
			ethernetif->ethaddr->addr[5]
			);
}

/*
 * low_level_output():
 *
 * Should do the actual transmission of the packet. The packet is
 * contained in the pbuf that is passed to the function. This pbuf
 * might be chained.
 *
 */
static int
low_level_output(struct netif *netif, struct pbuf *p)
{
	struct pbuf *q;
	int len = 0;
	int ret;

	for(q = p; q != NULL; q = q->next) {
		ret = Vfs.write(netif->netfd, q->payload, q->len);
		if (ret < 0)
			return -1;
		len += ret;
	}

	return len;
}

/* Ethernet packet max size. */
#define MAX_PKT_SIZE 1600

/*
 * low_level_input():
 *
 * Should allocate a pbuf and transfer the bytes of the incoming
 * packet from the interface into the pbuf.
 *
 */
static struct pbuf *
low_level_input(struct netif *netif)
{
	struct pbuf *p, *q;
	u16 len = MAX_PKT_SIZE;

	/* We allocate a pbuf chain of pbufs from the pool. */
	p = pbuf_alloc(PBUF_LINK, len, PBUF_POOL);

	if(p != NULL) {
		for(q = p; q != NULL; q = q->next) {
			wait((q->len = Vfs.read(netif->netfd, q->payload, len)) <= 0);
		}
	} 

	return p;  
}

/*
 * ethernetif_output():
 *
 * This function is called by the TCP/IP stack when an IP packet
 * should be sent. It calls the function called low_level_output() to
 * do the actuall transmission of the packet.
 *
 */
static int
ethernetif_output(struct netif *netif, struct pbuf *p, struct ip_addr *ipaddr)
{
	struct ethernetif *ethernetif;
	struct pbuf *q;
	struct eth_hdr *ethhdr;
	struct eth_addr *dest, mcastaddr;
	struct ip_addr *queryaddr;
	int err;
	u8 i;

	ethernetif = netif->state;

	/* Make room for Ethernet header. */
	if(pbuf_header(p, 14) != 0) {
		/* The pbuf_header() call shouldn't fail, but we allocate an extra
		   pbuf just in case. */
		q = pbuf_alloc(PBUF_LINK, 14, PBUF_RAM);
		if(q == NULL) {
			return -ENOMEM;
		}
		pbuf_chain(q, p);
		p = q;
	}

	/* Construct Ethernet header. Start with looking up deciding which
	   MAC address to use as a destination address. Broadcasts and
	   multicasts are special, all other addresses are looked up in the
	   ARP table. */
	queryaddr = ipaddr;
	if(ip_addr_isany(ipaddr) ||
			ip_addr_isbroadcast(ipaddr, &(netif->netmask))) {
		dest = (struct eth_addr *)&ethbroadcast;
	} else if(ip_addr_ismulticast(ipaddr)) {
		/* Hash IP multicast address to MAC address. */
		mcastaddr.addr[0] = 0x01;
		mcastaddr.addr[1] = 0x0;
		mcastaddr.addr[2] = 0x5e;
		mcastaddr.addr[3] = ip4_addr2(ipaddr) & 0x7f;
		mcastaddr.addr[4] = ip4_addr3(ipaddr);
		mcastaddr.addr[5] = ip4_addr4(ipaddr);
		dest = &mcastaddr;
	} else {

		if(ip_addr_maskcmp(ipaddr, &(netif->ip_addr), &(netif->netmask))) {
			/* Use destination IP address if the destination is on the same
			   subnet as we are. */
			queryaddr = ipaddr;
		} else {
			/* Otherwise we use the default router as the address to send
			   the Ethernet frame to. */
			queryaddr = &(netif->gw);
		}
		dest = arp_lookup(queryaddr);
	}


	/* If the arp_lookup() didn't find an address, we send out an ARP
	   query for the IP address. */
	if(dest == NULL) {
		q = arp_query(netif, ethernetif->ethaddr, queryaddr);
		if(q != NULL) {
			err = low_level_output(netif, q);
			pbuf_free(q);
			return err;
		}
		return -ENOMEM;
	}
	ethhdr = p->payload;

	for(i = 0; i < 6; i++) {
		ethhdr->dest.addr[i] = dest->addr[i];
		ethhdr->src.addr[i] = ethernetif->ethaddr->addr[i];
	}

	ethhdr->type = htons(ETHTYPE_IP);

	return low_level_output(netif, p);

}

/*
 * ethernetif_input():
 *
 * This function should be called when a packet is ready to be read
 * from the interface. It uses the function low_level_input() that
 * should handle the actual reception of bytes from the network
 * interface.
 *
 */
void
ethernetif_input(struct netif *netif)
{
	struct ethernetif *ethernetif;
	struct eth_hdr *ethhdr;
	struct pbuf *p;

	int i;

	ethernetif = netif->state;

	p = low_level_input(netif);
	p->payload += 2; /* Ethernet packet stack point. */

	if(p != NULL) {
		ethhdr = p->payload;
		switch(htons(ethhdr->type)) {
			case ETHTYPE_IP:
				DEBUGF("ip packet receive!\r\n");
				arp_ip_input(netif, p);
				pbuf_header(p, -14);
				netif->input(p, netif);
				break;
			case ETHTYPE_ARP:
				p = arp_arp_input(netif, ethernetif->ethaddr, p);
				if(p != NULL) {
					low_level_output(netif, p);
					pbuf_free(p);
				}
				break;
			default:
				pbuf_free(p);
				break;
		}
	}
}

static void
arp_timer(void *arg)
{
	arp_tmr();
	/* TODO: timer API 구현. */
	//sys_timeout(ARP_TMR_INTERVAL, (sys_timeout_handler)arp_timer, NULL); /*
}

/*
 * ethernetif_init():
 *
 * Should be called at the beginning of the program to set up the
 * network interface. It calls the function low_level_init() to do the
 * actual setup of the hardware.
 *
 */
void
ethernetif_init(struct netif *netif)
{
	struct ethernetif *ethernetif;

	ethernetif = Mem.alloc(sizeof(struct ethernetif), 0);
	netif->state = ethernetif;
	strcpy(netif->name, "eth");
	netif->output = ethernetif_output;
	netif->linkoutput = low_level_output;

	ethernetif->ethaddr = (struct eth_addr *)&(netif->hwaddr[0]);

	low_level_init(netif);
	arp_init();

	/* TODO: timer API 구현. */
	//sys_timeout(ARP_TMR_INTERVAL, (sys_timeout_handler)arp_timer, NULL);
}
