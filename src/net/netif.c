/* net/netif.c
 *
 * Network Infetface.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <net/netif.h>
#include <dinos/mem.h>
#include <arch/types.h>
#include <stdio.h>

struct netif *netif_list = NULL;
struct netif *netif_default = NULL;

struct netif *
netif_add(struct ip_addr *ipaddr, struct ip_addr *netmask,
	  struct ip_addr *gw,
	  void (*init)(struct netif *netif),
	  int (*input)(struct pbuf *p, struct netif *netif))
{
	struct netif *netif;
	static int netifnum = 0;

	netif = Mem.alloc(sizeof(struct netif), 0);

	if(netif == NULL) {
		return NULL;
	}

	netif->num = netifnum++;
	netif->input = input;
	ip_addr_set(&(netif->ip_addr), ipaddr);
	ip_addr_set(&(netif->netmask), netmask);
	ip_addr_set(&(netif->gw), gw);

	init(netif);

	netif->next = netif_list;
	netif_list = netif;
#ifdef NETIF_DEBUG
	printf("netif: added interface %c%c IP addr ", netif->name[0], netif->name[1]);
	ip_addr_debug_print(ipaddr);
	printf(" netmask ");
	ip_addr_debug_print(netmask);
	printf(" gw ");  
	ip_addr_debug_print(gw);
	printf("\n");
#endif 
	return netif;
}

struct netif *
netif_find(char *name)
{
	struct netif *netif;
	u8_t num;

	if(name == NULL) {
		return NULL;
	}

	num = name[2] - '0';

	for(netif = netif_list; netif != NULL; netif = netif->next) {
		if(num == netif->num &&
				name[0] == netif->name[0] &&
				name[1] == netif->name[1]) {
			printf("%s: found %s\n", __func__, name);
			return netif;
		}    
	}
	printf("%s: didn't find %s\n", __func__, name);
	return NULL;
}

void
netif_set_ipaddr(struct netif *netif, struct ip_addr *ipaddr)
{
	ip_addr_set(&(netif->ip_addr), ipaddr);
	printf("%s: setting IP address of interface %c%c to %d.%d.%d.%d\n",
				__func__, netif->name[0], netif->name[1],
				(u8_t)(ntohl(ipaddr->addr) >> 24 & 0xff),
				(u8_t)(ntohl(ipaddr->addr) >> 16 & 0xff),
				(u8_t)(ntohl(ipaddr->addr) >> 8 & 0xff),
				(u8_t)(ntohl(ipaddr->addr) & 0xff));
}

void
netif_set_gw(struct netif *netif, struct ip_addr *gw)
{
	ip_addr_set(&(netif->gw), gw);
}

void
netif_set_netmask(struct netif *netif, struct ip_addr *netmask)
{
	ip_addr_set(&(netif->netmask), netmask);
}

void
netif_set_default(struct netif *netif)
{
	netif_default = netif;
	printf("%s: setting default interface %s\n", __func__, netif->name);
}

void
netif_init(void)
{
	netif_list = netif_default = NULL;
}
