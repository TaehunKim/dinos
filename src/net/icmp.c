/* net/icmp.c
 *
 * Internet Contorl Message Protocol.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <net/icmp.h>
#include <net/ip.h>
#include <net/inet.h>

void
icmp_input(struct pbuf *p, struct netif *inp)
{
    unsigned char type;
    struct icmp_echo_hdr *iecho;
    struct ip_hdr *iphdr;
    struct ip_addr tmpaddr;
    u16_t hlen;

#ifdef ICMP_STATS
    ++stats.icmp.recv;
#endif /* ICMP_STATS */


    iphdr = p->payload;
    hlen = IPH_HL(iphdr) * 4/sizeof(u8_t);
    pbuf_header(p, -hlen);

    type = *((u8_t *)p->payload);

    switch(type) {
        case ICMP_ECHO:
            if(ip_addr_isbroadcast(&iphdr->dest, &inp->netmask) ||
                    ip_addr_ismulticast(&iphdr->dest)) {
                DEBUGF("Smurf.\r\n");
#ifdef ICMP_STATS
                ++stats.icmp.err;
#endif /* ICMP_STATS */
                pbuf_free(p);
                return;
            }
            DEBUGF("icmp_input: ping\r\n");
            DEBUGF("Pong!\r\n");
            if(p->tot_len < sizeof(struct icmp_echo_hdr)) {
                DEBUGF("icmp_input: bad ICMP echo received\r\n");
                pbuf_free(p);
#ifdef ICMP_STATS
                ++stats.icmp.lenerr;
#endif /* ICMP_STATS */

                return;      
            }
            iecho = p->payload;    
            if(inet_chksum_pbuf(p) != 0) {
                DEBUGF("icmp_input: checksum failed for received ICMP echo\r\n");
                pbuf_free(p);
#ifdef ICMP_STATS
                ++stats.icmp.chkerr;
#endif /* ICMP_STATS */
                return;
            }
            tmpaddr.addr = iphdr->src.addr;
            iphdr->src.addr = iphdr->dest.addr;
            iphdr->dest.addr = tmpaddr.addr;
            ICMPH_TYPE_SET(iecho, ICMP_ER);
            /* adjust the checksum */
            if(iecho->chksum >= htons(0xffff - (ICMP_ECHO << 8))) {
                iecho->chksum += htons(ICMP_ECHO << 8) + 1;
            } else {
                iecho->chksum += htons(ICMP_ECHO << 8);
            }
#ifdef ICMP_STATS
            ++stats.icmp.xmit;
#endif /* ICMP_STATS */

            pbuf_header(p, hlen);
            ip_output_if(p, &(iphdr->src), IP_HDRINCL,
                    IPH_TTL(iphdr), IP_PROTO_ICMP, inp);
            break; 
        default:
            DEBUGF("icmp_input: ICMP type not supported.\r\n");
#ifdef ICMP_STATS
            ++stats.icmp.proterr;
            ++stats.icmp.drop;
#endif /* ICMP_STATS */
    }
    pbuf_free(p);
}

void
icmp_dest_unreach(struct pbuf *p, enum icmp_dur_type t)
{
    struct pbuf *q;
    struct ip_hdr *iphdr;
    struct icmp_dur_hdr *idur;

    q = pbuf_alloc(PBUF_TRANSPORT, 8 + IP_HLEN + 8, PBUF_RAM);
    /* ICMP header + IP header + 8 bytes of data */

    iphdr = p->payload;

    idur = q->payload;
    ICMPH_TYPE_SET(idur, ICMP_DUR);
    ICMPH_CODE_SET(idur, t);

    bcopy(p->payload, (char *)q->payload + 8, IP_HLEN + 8);

    /* calculate checksum */
    idur->chksum = 0;
    idur->chksum = inet_chksum(idur, q->len);
#ifdef ICMP_STATS
    ++stats.icmp.xmit;
#endif /* ICMP_STATS */

    ip_output(q, NULL, &(iphdr->src),
            ICMP_TTL, IP_PROTO_ICMP);
    pbuf_free(q);
}

void
icmp_time_exceeded(struct pbuf *p, enum icmp_te_type t)
{
    struct pbuf *q;
    struct ip_hdr *iphdr;
    struct icmp_te_hdr *tehdr;

    q = pbuf_alloc(PBUF_TRANSPORT, 8 + IP_HLEN + 8, PBUF_RAM);

    iphdr = p->payload;
#if ICMP_DEBUG
    DEBUGF("icmp_time_exceeded from ");
    ip_addr_debug_print(&(iphdr->src));
    DEBUGF(" to ");
    ip_addr_debug_print(&(iphdr->dest));
    DEBUGF("\r\n");
#endif /* ICMP_DEBNUG */

    tehdr = q->payload;
    ICMPH_TYPE_SET(tehdr, ICMP_TE);
    ICMPH_CODE_SET(tehdr, t);

    /* copy fields from original packet */
    bcopy((char *)p->payload, (char *)q->payload + 8, IP_HLEN + 8);

    /* calculate checksum */
    tehdr->chksum = 0;
    tehdr->chksum = inet_chksum(tehdr, q->len);
#ifdef ICMP_STATS
    ++stats.icmp.xmit;
#endif /* ICMP_STATS */
    ip_output(q, NULL, &(iphdr->src),
            ICMP_TTL, IP_PROTO_ICMP);
    pbuf_free(q);
}
