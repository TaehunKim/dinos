/* net/inet.c
 *
 * INET checksum.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <net/inet.h>
#include <stdio.h>

static u32
chksum(void *dataptr, int len)
{
    u32 acc;
    u16 *ptr = (u16*)dataptr;

    for(acc = 0; len > 1; len -= 2) {
        acc += *ptr++;
    }

    /* add up any odd byte */
    if(len == 1) {
        acc += htons((u16)((*(u8 *)dataptr) & 0xff) << 8);
        DEBUGF("inet: chksum: odd byte %d\r\n", *(u8 *)dataptr);
    }

    return acc;
}

u16
inet_chksum_pseudo(struct pbuf *p,
        struct ip_addr *src, struct ip_addr *dest,
        u8 proto, u16 proto_len)
{
    u32 acc;
    struct pbuf *q;
    u8 swapped;

    acc = 0;
    swapped = 0;
    for(q = p; q != NULL; q = q->next) {    
        acc += chksum(q->payload, q->len);
        while(acc >> 16) {
            acc = (acc & 0xffff) + (acc >> 16);
        }
        if(q->len % 2 != 0) {
            swapped = 1 - swapped;
            acc = ((acc & 0xff) << 8) | ((acc & 0xff00) >> 8);
        }
    }

    if(swapped) {
        acc = ((acc & 0xff) << 8) | ((acc & 0xff00) >> 8);
    }
    acc += (src->addr & 0xffff);
    acc += ((src->addr >> 16) & 0xffff);
    acc += (dest->addr & 0xffff);
    acc += ((dest->addr >> 16) & 0xffff);
    acc += (u32)htons((u16)proto);
    acc += (u32)htons(proto_len);  

    while(acc >> 16) {
        acc = (acc & 0xffff) + (acc >> 16);
    }    
    return ~(acc & 0xffff);
}

u16
inet_chksum(void *dataptr, u16 len)
{
    u32 acc;

    acc = chksum(dataptr, len);
    while(acc >> 16) {
        acc = (acc & 0xffff) + (acc >> 16);
    }    
    return ~(acc & 0xffff);
}

u16
inet_chksum_pbuf(struct pbuf *p)
{
    u32 acc;
    struct pbuf *q;
    u8 swapped;

    acc = 0;
    swapped = 0;
    for(q = p; q != NULL; q = q->next) {
        acc += chksum(q->payload, q->len);
        while(acc >> 16) {
            acc = (acc & 0xffff) + (acc >> 16);
        }    
        if(q->len % 2 != 0) {
            swapped = 1 - swapped;
            acc = (acc & 0xff << 8) | (acc & 0xff00 >> 8);
        }
    }

    if(swapped) {
        acc = ((acc & 0xff) << 8) | ((acc & 0xff00) >> 8);
    }
    return ~(acc & 0xffff);
}
