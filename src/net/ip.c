/* net/ip.c
 *
 * Internet Protocol.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <net/ip.h>
#include <net/inet.h>
#include <net/netif.h>
#include <net/icmp.h>
#include <net/udp.h>
//#include <net/tcp.h>

struct ip_addr ip_addr_broadcast = {0xffffffff};

void
ip_init(void)
{
}

#ifdef IP_DEBUG
u8
ip_lookup(void *header, struct netif *inp)
{
    struct ip_hdr *iphdr;

    iphdr = header;

    /* Refuse anything that isn't IPv4. */
    if(IPH_V(iphdr) != 4) {
        return 0;
    }

    /* Immediately accept/decline packets that are fragments or has
       options. */
#if IP_REASSEMBLY == 0
    /*  if((IPH_OFFSET(iphdr) & htons(IP_OFFMASK | IP_MF)) != 0) {
        return 0;
        }*/
#endif /* IP_REASSEMBLY == 0 */

#if IP_OPTIONS == 0
    if(IPH_HL(iphdr) != 5) {
        return 0;
    }
#endif /* IP_OPTIONS == 0 */

    switch(IPH_PROTO(iphdr)) {
#if UDP > 0
        case IP_PROTO_UDP:
            return udp_lookup(iphdr, inp);
            break;
#endif /* UDP */
#if TCP > 0    
        case IP_PROTO_TCP:
            return 1;
#endif /* TCP */
        case IP_PROTO_ICMP:
            return 1;
            break;
        default:
            return 0;
    }
}
#endif /* IP_DEBUG */

struct netif *
ip_route(struct ip_addr *dest)
{
    struct netif *netif;

    for(netif = netif_list; netif != NULL; netif = netif->next) {
        if(ip_addr_maskcmp(dest, &(netif->ip_addr), &(netif->netmask))) {
            return netif;
        }
    }

    return netif_default;
}

#if IP_FORWARD

static void
ip_forward(struct pbuf *p, struct ip_hdr *iphdr, struct netif *inp)
{
    static struct netif *netif;

    if((netif = ip_route((struct ip_addr *)&(iphdr->dest))) == NULL) {

        DEBUGF("ip_forward: no forwarding route for 0x%.4x found\r\n",
                    iphdr->dest.addr);
        return;
    }

    /* Don't forward packets onto the same network interface on which
       they arrived. */
    if(netif == inp) {
        DEBUGF("ip_forward: not forward packets back on incoming interface.\r\n");
        return;
    }

    /* Decrement TTL and send ICMP if ttl == 0. */
    IPH_TTL_SET(iphdr, IPH_TTL(iphdr) - 1);
    if(IPH_TTL(iphdr) == 0) {
        /* Don't send ICMP messages in response to ICMP messages */
        if(IPH_PROTO(iphdr) != IP_PROTO_ICMP) {
            icmp_time_exceeded(p, ICMP_TE_TTL);
        }
        return;       
    }

    /* Incremental update of the IP checksum. */
    if(IPH_CHKSUM(iphdr) >= htons(0xffff - 0x100)) {
        IPH_CHKSUM_SET(iphdr, IPH_CHKSUM(iphdr) + htons(0x100) + 1);
    } else {
        IPH_CHKSUM_SET(iphdr, IPH_CHKSUM(iphdr) + htons(0x100));
    }

    DEBUGF("ip_forward: forwarding packet to 0x%.4x\r\n",
                iphdr->dest.addr);

#ifdef IP_STATS
    ++stats.ip.fw;
    ++stats.ip.xmit;
#endif /* IP_STATS */

    netif->output(netif, p, (struct ip_addr *)&(iphdr->dest));
}
#endif /* IP_FORWARD */

#define IP_REASSEMBLY 0
#define IP_REASS_BUFSIZE 5760
#define IP_REASS_MAXAGE 10

#if IP_REASSEMBLY
static u8 ip_reassbuf[IP_HLEN + IP_REASS_BUFSIZE];
static u8 ip_reassbitmap[IP_REASS_BUFSIZE / (8 * 8)];
static const u8 bitmap_bits[8] = {0xff, 0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01};
static u16 ip_reasslen;
static u8 ip_reassflags;
#define IP_REASS_FLAG_LASTFRAG 0x01
static u8 ip_reasstmr;

static struct pbuf *
ip_reass(struct pbuf *p)
{
    struct pbuf *q;
    struct ip_hdr *fraghdr, *iphdr;
    u16 offset, len;
    u16 i;

    iphdr = (struct ip_hdr *)ip_reassbuf;
    fraghdr = (struct ip_hdr *)p->payload;

    /* If ip_reasstmr is zero, no packet is present in the buffer, so we
       write the IP header of the fragment into the reassembly
       buffer. The timer is updated with the maximum age. */
    if(ip_reasstmr == 0) {
        DEBUGF("ip_reass: new packet\r\n");
        bcopy(fraghdr, iphdr, IP_HLEN);
        ip_reasstmr = IP_REASS_MAXAGE;
        ip_reassflags = 0;
        /* Clear the bitmap. */
        bzero(ip_reassbitmap, sizeof(ip_reassbitmap));
    }

    /* Check if the incoming fragment matches the one currently present
       in the reasembly buffer. If so, we proceed with copying the
       fragment into the buffer. */
    if(ip_addr_cmp(&iphdr->src, &fraghdr->src) &&
            ip_addr_cmp(&iphdr->dest, &fraghdr->dest) &&
            IPH_ID(iphdr) == IPH_ID(fraghdr)) {
        DEBUGF("ip_reass: matching old packet\r\n");
        /* Find out the offset in the reassembly buffer where we should
           copy the fragment. */
        len = ntohs(IPH_LEN(fraghdr)) - IPH_HL(fraghdr) * 4;
        offset = (ntohs(IPH_OFFSET(fraghdr)) & IP_OFFMASK) * 8;

        /* If the offset or the offset + fragment length overflows the
           reassembly buffer, we discard the entire packet. */
        if(offset > IP_REASS_BUFSIZE ||
                offset + len > IP_REASS_BUFSIZE) {
            DEBUGF("ip_reass: fragment outside of buffer (%d:%d/%d).\r\n",
                    offset, offset + len, IP_REASS_BUFSIZE);
            ip_reasstmr = 0;
            goto nullreturn;
        }

        /* Copy the fragment into the reassembly buffer, at the right
           offset. */
        DEBUGF("ip_reass: copying with offset %d into %d:%d\r\n",
                offset, IP_HLEN + offset, IP_HLEN + offset + len);
        bcopy((u8 *)fraghdr + IPH_HL(fraghdr) * 4,
                &ip_reassbuf[IP_HLEN + offset], len);

        /* Update the bitmap. */
        if(offset / (8 * 8) == (offset + len) / (8 * 8)) {
            DEBUGF("ip_reass: updating single byte in bitmap.\r\n");
            /* If the two endpoints are in the same byte, we only update
               that byte. */
            ip_reassbitmap[offset / (8 * 8)] |=
                bitmap_bits[(offset / 8 ) & 7] &
                ~bitmap_bits[((offset + len) / 8 ) & 7];
        } else {
            /* If the two endpoints are in different bytes, we update the
               bytes in the endpoints and fill the stuff inbetween with
               0xff. */
            ip_reassbitmap[offset / (8 * 8)] |= bitmap_bits[(offset / 8 ) & 7];
            DEBUGF("ip_reass: updating many bytes in bitmap (%d:%d).\r\n",
                    1 + offset / (8 * 8), (offset + len) / (8 * 8));
            for(i = 1 + offset / (8 * 8); i < (offset + len) / (8 * 8); ++i) {
                ip_reassbitmap[i] = 0xff;
            }      
            ip_reassbitmap[(offset + len) / (8 * 8)] |= ~bitmap_bits[((offset + len) / 8 ) & 7];
        }

        /* If this fragment has the More Fragments flag set to zero, we
           know that this is the last fragment, so we can calculate the
           size of the entire packet. We also set the
           IP_REASS_FLAG_LASTFRAG flag to indicate that we have received
           the final fragment. */

        if((ntohs(IPH_OFFSET(fraghdr)) & IP_MF) == 0) {
            ip_reassflags |= IP_REASS_FLAG_LASTFRAG;
            ip_reasslen = offset + len;
            DEBUGF("ip_reass: last fragment seen, total len %d\r\n", ip_reasslen);
        }

        /* Finally, we check if we have a full packet in the buffer. We do
           this by checking if we have the last fragment and if all bits
           in the bitmap are set. */
        if(ip_reassflags & IP_REASS_FLAG_LASTFRAG) {
            /* Check all bytes up to and including all but the last byte in
               the bitmap. */
            for(i = 0; i < ip_reasslen / (8 * 8) - 1; ++i) {
                if(ip_reassbitmap[i] != 0xff) {
                    DEBUGF("ip_reass: last fragment seen, bitmap %d/%d failed (%.4x)\r\n", 
                            i, ip_reasslen / (8 * 8) - 1, ip_reassbitmap[i]);
                    goto nullreturn;
                }
            }
            /* Check the last byte in the bitmap. It should contain just the
               right amount of bits. */
            if(ip_reassbitmap[ip_reasslen / (8 * 8)] !=
                    (u8)~bitmap_bits[ip_reasslen / 8 & 7]) {
                DEBUGF("ip_reass: last fragment seen, bitmap %d didn't contain %.4x (%.4x)\r\n",
                        ip_reasslen / (8 * 8), ~bitmap_bits[ip_reasslen / 8 & 7],
                        ip_reassbitmap[ip_reasslen / (8 * 8)]);
                goto nullreturn;
            }

            /* Pretend to be a "normal" (i.e., not fragmented) IP packet
               from now on. */
            IPH_OFFSET_SET(iphdr, 0);
            IPH_CHKSUM_SET(iphdr, 0);
            IPH_CHKSUM_SET(iphdr, inet_chksum(iphdr, IP_HLEN));

            /* If we have come this far, we have a full packet in the
               buffer, so we allocate a pbuf and copy the packet into it. We
               also reset the timer. */
            ip_reasstmr = 0;
            pbuf_free(p);
            p = pbuf_alloc(PBUF_LINK, ip_reasslen, PBUF_POOL);
            if(p != NULL) {
                i = 0;
                for(q = p; q != NULL; q = q->next) {
                    /* Copy enough bytes to fill this pbuf in the chain. The
                       avaliable data in the pbuf is given by the q->len
                       variable. */
                    DEBUGF("ip_reass: bcopy from %p (%d) to %p, %d bytes\r\n",
                            &ip_reassbuf[i], i, q->payload, q->len > ip_reasslen - i? ip_reasslen - i: q->len);
                    bcopy(&ip_reassbuf[i], q->payload,
                            q->len > ip_reasslen - i? ip_reasslen - i: q->len);
                    i += q->len;
                }
            }
            DEBUGF("ip_reass: p %p\r\n", p);
            return p;
        }
    }

nullreturn:
    pbuf_free(p);
    return NULL;
}
#endif /* IP_REASSEMBLY */

int
ip_input(struct pbuf *p, struct netif *inp) {
    static struct ip_hdr *iphdr;
    static struct netif *netif;
    static u8 hl;

#ifdef IP_STATS
    ++stats.ip.recv;
#endif /* IP_STATS */
    DEBUGF("%s: processing.\r\n", __func__);

    /* identify the IP header */
    iphdr = p->payload;
    if(IPH_V(iphdr) != 4) {
        DEBUGF("IP packet dropped due to bad version number %d\n", IPH_V(iphdr));
#if IP_DEBUG
        ip_debug_print(p);
#endif /* IP_DEBUG */
        pbuf_free(p);
#ifdef IP_STATS
        ++stats.ip.err;
        ++stats.ip.drop;
#endif /* IP_STATS */
        return 0;
    }

    hl = IPH_HL(iphdr);

    if(hl * 4 > p->len) {
        DEBUGF("IP packet dropped due to too short packet %d\n", p->len);

        pbuf_free(p);
#ifdef IP_STATS
        ++stats.ip.lenerr;
        ++stats.ip.drop;
#endif /* IP_STATS */
        return 0;
    }

    /* verify checksum */
    if(inet_chksum(iphdr, hl * 4) != 0) {

        DEBUGF("IP packet dropped due to failing checksum 0x%.4x\r\n", inet_chksum(iphdr, hl * 4));
#if IP_DEBUG
        ip_debug_print(p);
#endif /* IP_DEBUG */
        pbuf_free(p);
#ifdef IP_STATS
        ++stats.ip.chkerr;
        ++stats.ip.drop;
#endif /* IP_STATS */
        return 0;
    }

    /* Trim pbuf. This should have been done at the netif layer,
       but we'll do it anyway just to be sure that its done. */
    pbuf_realloc(p, ntohs(IPH_LEN(iphdr)));

    /* is this packet for us? */
    for(netif = netif_list; netif != NULL; netif = netif->next) {

        DEBUGF("ip_input: iphdr->dest 0x%.4x netif->ip_addr 0x%.4x (0x%.4x, 0x%.4x, 0x%.4x)\r\n",
                iphdr->dest.addr, netif->ip_addr.addr,
                iphdr->dest.addr & netif->netmask.addr,
                netif->ip_addr.addr & netif->netmask.addr,
                iphdr->dest.addr & ~(netif->netmask.addr));

        if(ip_addr_isany(&(netif->ip_addr)) ||
                ip_addr_cmp(&(iphdr->dest), &(netif->ip_addr)) ||
                (ip_addr_isbroadcast(&(iphdr->dest), &(netif->netmask)) &&
                 ip_addr_maskcmp(&(iphdr->dest), &(netif->ip_addr), &(netif->netmask))) ||
                ip_addr_cmp(&(iphdr->dest), IP_ADDR_BROADCAST)) {
            break;
        }
    }

    if(netif == NULL) {
        /* packet not for us, route or discard */
        DEBUGF("ip_input: packet not for us.\r\n");
#if IP_FORWARD
        if(!ip_addr_isbroadcast(&(iphdr->dest), &(inp->netmask))) {
            ip_forward(p, iphdr, inp);
        }
#endif /* IP_FORWARD */
        pbuf_free(p);
        return 0;
    }

#if IP_REASSEMBLY
    if((IPH_OFFSET(iphdr) & htons(IP_OFFMASK | IP_MF)) != 0) {
        p = ip_reass(p);
        if(p == NULL) {
            return 0;
        }
        iphdr = p->payload;
    }
#else /* IP_REASSEMBLY */
    if((IPH_OFFSET(iphdr) & htons(IP_OFFMASK | IP_MF)) != 0) {
        pbuf_free(p);
        DEBUGF("IP packet dropped since it was fragmented (0x%.4x).\r\n",
                ntohs(IPH_OFFSET(iphdr)));
#ifdef IP_STATS
        ++stats.ip.opterr;
        ++stats.ip.drop;
#endif /* IP_STATS */
        return 0;
    }
#endif /* IP_REASSEMBLY */

#if IP_OPTIONS == 0
    if(hl * 4 > IP_HLEN) {
        DEBUGF("IP packet dropped since there were IP options.\r\n");

        pbuf_free(p);    
#ifdef IP_STATS
        ++stats.ip.opterr;
        ++stats.ip.drop;
#endif /* IP_STATS */
        return 0;
    }  
#endif /* IP_OPTIONS == 0 */


    /* send to upper layers */
#if IP_DEBUG
    DEBUGF("ip_input: \r\n");
    ip_debug_print(p);
    DEBUGF("ip_input: p->len %d p->tot_len %d\r\n", p->len, p->tot_len);
#endif /* IP_DEBUG */   

    switch(IPH_PROTO(iphdr)) {
#if UDP > 0    
        case IP_PROTO_UDP:
            udp_input(p, inp);
            break;
#endif /* UDP */
#if TCP > 0    
        case IP_PROTO_TCP:
            //tcp_input(p, inp);
            break;
#endif /* TCP */
        case IP_PROTO_ICMP:
            icmp_input(p, inp);
            break;
        default:
            /* send ICMP destination protocol unreachable unless is was a broadcast */
            if(!ip_addr_isbroadcast(&(iphdr->dest), &(inp->netmask)) &&
                    !ip_addr_ismulticast(&(iphdr->dest))) {
                p->payload = iphdr;
                icmp_dest_unreach(p, ICMP_DUR_PROTO);
            }
            pbuf_free(p);

            DEBUGF("Unsupported transportation protocol %d\r\n", IPH_PROTO(iphdr));

#ifdef IP_STATS
            ++stats.ip.proterr;
            ++stats.ip.drop;
#endif /* IP_STATS */

    }
    return 0;
}

int
ip_output_if(struct pbuf *p, struct ip_addr *src, struct ip_addr *dest,
        u8 ttl,
        u8 proto, struct netif *netif)
{
    static struct ip_hdr *iphdr;
    static u16 ip_id = 0;



    if(dest != IP_HDRINCL) {
        if(pbuf_header(p, IP_HLEN)) {
            DEBUGF("ip_output: not enough room for IP header in pbuf\r\n");

#ifdef IP_STATS
            ++stats.ip.err;
#endif /* IP_STATS */
            pbuf_free(p);
            return -1;
        }

        iphdr = p->payload;

        IPH_TTL_SET(iphdr, ttl);
        IPH_PROTO_SET(iphdr, proto);

        ip_addr_set(&(iphdr->dest), dest);

        IPH_VHLTOS_SET(iphdr, 4, IP_HLEN / 4, 0);
        IPH_LEN_SET(iphdr, htons(p->tot_len));
        IPH_OFFSET_SET(iphdr, htons(IP_DF));
        IPH_ID_SET(iphdr, htons(++ip_id));

        if(ip_addr_isany(src)) {
            ip_addr_set(&(iphdr->src), &(netif->ip_addr));
        } else {
            ip_addr_set(&(iphdr->src), src);
        }

        IPH_CHKSUM_SET(iphdr, 0);
        IPH_CHKSUM_SET(iphdr, inet_chksum(iphdr, IP_HLEN));
    } else {
        iphdr = p->payload;
        dest = &(iphdr->dest);
    }

#ifdef IP_STATS
    stats.ip.xmit++;
#endif /* IP_STATS */
    DEBUGF("ip_output_if: %s\n", netif->name);
#if IP_DEBUG
    ip_debug_print(p);
#endif /* IP_DEBUG */


    return netif->output(netif, p, dest);  
}

int
ip_output(struct pbuf *p, struct ip_addr *src, struct ip_addr *dest,
        u8 ttl, u8 proto)
{
    static struct netif *netif;


    if((netif = ip_route(dest)) == NULL) {
        DEBUGF("ip_output: No route to 0x%.4x\r\n", dest->addr);

#ifdef IP_STATS
        ++stats.ip.rterr;
#endif /* IP_STATS */
        pbuf_free(p);
        return -1; /* routing error. */
    }

    return ip_output_if(p, src, dest, ttl, proto, netif);
}

#if IP_DEBUG
void
ip_debug_print(struct pbuf *p)
{
    struct ip_hdr *iphdr = p->payload;
    u8 *payload;

    payload = (u8 *)iphdr + IP_HLEN/sizeof(u8);

    DEBUGF("IP header:\r\n");
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|%2d |%2d |   %2d  |      %4d     | (v, hl, tos, len)\r\n",
            IPH_V(iphdr),
            IPH_HL(iphdr),
            IPH_TOS(iphdr),
            ntohs(IPH_LEN(iphdr)));
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|    %5d      |%d%d%d|    %4d   | (id, flags, offset)\r\n",
            ntohs(IPH_ID(iphdr)),
            ntohs(IPH_OFFSET(iphdr)) >> 15 & 1,
            ntohs(IPH_OFFSET(iphdr)) >> 14 & 1,
            ntohs(IPH_OFFSET(iphdr)) >> 13 & 1,
            ntohs(IPH_OFFSET(iphdr)) & IP_OFFMASK);
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|   %2d  |   %2d  |    0x%04x     | (ttl, proto, chksum)\r\n",
            IPH_TTL(iphdr),
            IPH_PROTO(iphdr),
            ntohs(IPH_CHKSUM(iphdr)));
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|  %3ld  |  %3ld  |  %3ld  |  %3ld  | (src)\r\n",
            ntohl(iphdr->src.addr) >> 24 & 0xff,
            ntohl(iphdr->src.addr) >> 16 & 0xff,
            ntohl(iphdr->src.addr) >> 8 & 0xff,
            ntohl(iphdr->src.addr) & 0xff);
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|  %3ld  |  %3ld  |  %3ld  |  %3ld  | (dest)\r\n",
            ntohl(iphdr->dest.addr) >> 24 & 0xff,
            ntohl(iphdr->dest.addr) >> 16 & 0xff,
            ntohl(iphdr->dest.addr) >> 8 & 0xff,
            ntohl(iphdr->dest.addr) & 0xff);
    DEBUGF("+-------------------------------+\r\n");
}
#endif /* IP_DEBUG */
