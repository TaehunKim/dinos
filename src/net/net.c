/* net/net.c
 *
 * Network module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <config.h>
#include <net/net.h>
#include <net/netif.h>
#include <net/ether.h>
#include <net/ip.h>
#include <dinos/file.h>
#include <dinos/thread.h>

/* Network Tx/Rx threads. */
extern void *netrx_thread(void *data);
extern void *nettx_thread(void *data);

static int
net_init(void)
{
	struct netif *netif;
	struct ip_addr ipaddr = {.addr = ntohl(CFG_IPADDR)};
	struct ip_addr netmask = {.addr = ntohl(CFG_NETMASK)};
	struct ip_addr gw = {.addr = ntohl(CFG_GWADDR)};

	pbuf_init();
	netif_init();
	netif = netif_add(&ipaddr, &netmask, &gw, ethernetif_init, ip_input);

	if (netif->netfd < 0) {
		DEBUGF("%s: Do not support network.\r\n", __func__);
		return -1;
	}
	DEBUGF("%s: network interface name = %s\r\n", __func__, netif->name);
	
	Thread.create("netrx", 200, Thread.cur()->proc, netrx_thread, (void *)netif);
	Thread.create("nettx", 200, Thread.cur()->proc, nettx_thread, (void *)netif);
}

struct Net net = {
	.init = net_init,
};
