/* net/arp.c
 *
 * Address Resolution Protocol.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <net/inet.h>
#include <net/ether.h>
#include <net/ip.h>

#define ARP_MAXAGE 2  /* 120 * 10 seconds = 20 minutes. */
#define HWTYPE_ETHERNET 1
#define ARP_REQUEST 1
#define ARP_REPLY 2
#define ARP_TABLE_SIZE 256

struct arp_hdr {
	struct eth_hdr ethhdr;
	u16 hwtype;
	u16 proto;
	u16 _hwlen_protolen;
	u16 opcode;
	struct eth_addr shwaddr;
	struct ip_addr sipaddr;
	struct eth_addr dhwaddr;
	struct ip_addr dipaddr;
} __attribute__((packed));

#define ARPH_HWLEN(hdr) (ntohs((hdr)->_hwlen_protolen) >> 8)
#define ARPH_PROTOLEN(hdr) (ntohs((hdr)->_hwlen_protolen) & 0xff)
#define ARPH_HWLEN_SET(hdr, len) \
	(hdr)->_hwlen_protolen = htons(ARPH_PROTOLEN(hdr) | ((len) << 8))
#define ARPH_PROTOLEN_SET(hdr, len) \
	(hdr)->_hwlen_protolen = htons((len) | (ARPH_HWLEN(hdr) << 8))

struct ethip_hdr {
	struct eth_hdr eth;
	struct ip_hdr ip;
} __attribute__((packed));

struct arp_entry {
	struct ip_addr ipaddr;
	struct eth_addr ethaddr;
	u8 ctime;
};

static struct arp_entry arp_table[ARP_TABLE_SIZE];
static u8 ctime;

void
arp_init(void)
{
	int i;

	for(i = 0; i < ARP_TABLE_SIZE; ++i) {
		ip_addr_set(&(arp_table[i].ipaddr), IP_ADDR_ANY);
	}
}

void
arp_tmr(void)
{
	int i;

	++ctime;
	for(i = 0; i < ARP_TABLE_SIZE; ++i) {
		if(!ip_addr_isany(&arp_table[i].ipaddr) &&       
				ctime - arp_table[i].ctime >= ARP_MAXAGE) {
			DEBUGF("arp_timer: expired entry %d.\r\n", i);
			ip_addr_set(&(arp_table[i].ipaddr),
					IP_ADDR_ANY);
		}
	}  
}

static void
add_arp_entry(struct ip_addr *ipaddr, struct eth_addr *ethaddr)
{
	int i, j, k;
	u8 maxtime;

	/* Walk through the ARP mapping table and try to find an entry to
	   update. If none is found, the IP -> MAC address mapping is
	   inserted in the ARP table. */
	for(i = 0; i < ARP_TABLE_SIZE; ++i) {

		/* Only check those entries that are actually in use. */
		if(!ip_addr_isany(&arp_table[i].ipaddr)) {
			/* Check if the source IP address of the incoming packet matches
			   the IP address in this ARP table entry. */
			if(ip_addr_cmp(ipaddr, &arp_table[i].ipaddr)) {
				/* An old entry found, update this and return. */
				for(k = 0; k < 6; ++k) {
					arp_table[i].ethaddr.addr[k] = ethaddr->addr[k];
				}
				arp_table[i].ctime = ctime;
				return;
			}
		}
	}

	/* If we get here, no existing ARP table entry was found, so we
	   create one. */

	/* First, we try to find an unused entry in the ARP table. */
	for(i = 0; i < ARP_TABLE_SIZE; ++i) {
		if(ip_addr_isany(&arp_table[i].ipaddr)) {
			break;
		}
	}

	/* If no unused entry is found, we try to find the oldest entry and
	   throw it away. */
	if(i == ARP_TABLE_SIZE) {
		maxtime = 0;
		j = 0;
		for(i = 0; i < ARP_TABLE_SIZE; ++i) {
			if(ctime - arp_table[i].ctime > maxtime) {
				maxtime = ctime - arp_table[i].ctime;
				j = i;
			}
		}
		i = j;
	}

	/* Now, i is the ARP table entry which we will fill with the new
	   information. */
	ip_addr_set(&arp_table[i].ipaddr, ipaddr);
	for(k = 0; k < 6; ++k) {
		arp_table[i].ethaddr.addr[k] = ethaddr->addr[k];
	}
	arp_table[i].ctime = ctime;
	return;
}

void
arp_ip_input(struct netif *netif, struct pbuf *p)
{
	struct ethip_hdr *hdr;

	hdr = p->payload;

	/* Only insert/update an entry if the source IP address of the
	   incoming IP packet comes from a host on the local network. */
	if(!ip_addr_maskcmp(&(hdr->ip.src), &(netif->ip_addr), &(netif->netmask))) {
		return;
	}
	DEBUGF("arp_ip_input: updating ARP table.\r\n");
	add_arp_entry(&(hdr->ip.src), &(hdr->eth.src));
}

struct pbuf *
arp_arp_input(struct netif *netif, struct eth_addr *ethaddr, struct pbuf *p)
{
	struct arp_hdr *hdr;
	int i;

	if(p->tot_len < sizeof(struct arp_hdr)) {
		DEBUGF("%s: packet too short (%d/%d)\r\n", __func__,
				p->tot_len, sizeof(struct arp_hdr));
		pbuf_free(p);
		return NULL;
	}

	hdr = p->payload;

	switch(htons(hdr->opcode)) {
		case ARP_REQUEST:
			/* ARP request. If it asked for our address, we send out a
			   reply. */
			if(ip_addr_cmp(&(hdr->dipaddr), &(netif->ip_addr))) {
				hdr->opcode = htons(ARP_REPLY);

				ip_addr_set(&(hdr->dipaddr), &(hdr->sipaddr));
				ip_addr_set(&(hdr->sipaddr), &(netif->ip_addr));

				for(i = 0; i < 6; ++i) {
					hdr->dhwaddr.addr[i] = hdr->shwaddr.addr[i];
					hdr->shwaddr.addr[i] = ethaddr->addr[i];
					hdr->ethhdr.dest.addr[i] = hdr->dhwaddr.addr[i];
					hdr->ethhdr.src.addr[i] = ethaddr->addr[i];
				}

				hdr->hwtype = htons(HWTYPE_ETHERNET);
				ARPH_HWLEN_SET(hdr, 6);

				hdr->proto = htons(ETHTYPE_IP);
				ARPH_PROTOLEN_SET(hdr, sizeof(struct ip_addr));      

				hdr->ethhdr.type = htons(ETHTYPE_ARP);      
				return p;
			}
			break;
		case ARP_REPLY:    
			/* ARP reply. We insert or update the ARP table. */
			DEBUGF("arp_arp_input: ARP reply\r\n");
			if(ip_addr_cmp(&(hdr->dipaddr), &(netif->ip_addr))) {
				add_arp_entry(&(hdr->sipaddr), &(hdr->shwaddr));
#if (LWIP_DHCP && DHCP_DOES_ARP_CHECK)
				dhcp_arp_reply(&hdr->sipaddr);
#endif      
			}
			break;
		default:
			DEBUGF("arp_arp_input: unknown type %d\r\n", htons(hdr->opcode));
			break;
	}

	pbuf_free(p);
	return NULL;
}

struct eth_addr *
arp_lookup(struct ip_addr *ipaddr)
{
	int i;

	for(i = 0; i < ARP_TABLE_SIZE; ++i) {
		if(ip_addr_cmp(ipaddr, &arp_table[i].ipaddr)) {
			return &arp_table[i].ethaddr;
		}
	}
	return NULL;  
}

struct pbuf *
arp_query(struct netif *netif, struct eth_addr *ethaddr, struct ip_addr *ipaddr)
{
	struct arp_hdr *hdr;
	struct pbuf *p;
	int i;

	p = pbuf_alloc(PBUF_LINK, sizeof(struct arp_hdr), PBUF_RAM);
	if(p == NULL) {
		return NULL;
	}

	hdr = p->payload;

	hdr->opcode = htons(ARP_REQUEST);

	for(i = 0; i < 6; ++i) {
		hdr->dhwaddr.addr[i] = 0x00;
		hdr->shwaddr.addr[i] = ethaddr->addr[i];
	}

	ip_addr_set(&(hdr->dipaddr), ipaddr);
	ip_addr_set(&(hdr->sipaddr), &(netif->ip_addr));

	hdr->hwtype = htons(HWTYPE_ETHERNET);
	ARPH_HWLEN_SET(hdr, 6);

	hdr->proto = htons(ETHTYPE_IP);
	ARPH_PROTOLEN_SET(hdr, sizeof(struct ip_addr));

	for(i = 0; i < 6; ++i) {
		hdr->ethhdr.dest.addr[i] = 0xff;
		hdr->ethhdr.src.addr[i] = ethaddr->addr[i];
	}

	hdr->ethhdr.type = htons(ETHTYPE_ARP);      
	return p;
}
