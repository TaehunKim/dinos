/* net/udp.c
 *
 * User datagram protocol.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <net/inet.h>
#include <net/netif.h>
#include <net/udp.h>
#include <net/icmp.h>
#include <dinos/mem.h>

/* The list of UDP PCBs. */
static struct udp_pcb *udp_pcbs = NULL;
static struct udp_pcb *pcb_cache = NULL;

#if UDP_DEBUG
int udp_debug_print(struct udp_hdr *udphdr);
#endif /* UDP_DEBUG */
	  
void
udp_init(void)
{
}

#ifdef UDP_DEBUG
u8
udp_lookup(struct ip_hdr *iphdr, struct netif *inp)
{
    struct udp_pcb *pcb;
    struct udp_hdr *udphdr;
    u16 src, dest;

    udphdr = (struct udp_hdr *)(u8 *)iphdr + IPH_HL(iphdr) * 4/sizeof(u8);

    src = ntohs(udphdr->src);
    dest = ntohs(udphdr->dest);

    pcb = pcb_cache;
    if(pcb != NULL &&
            pcb->remote_port == src &&
            pcb->local_port == dest &&
            (ip_addr_isany(&pcb->remote_ip) ||
             ip_addr_cmp(&(pcb->remote_ip), &(iphdr->src))) &&
            (ip_addr_isany(&pcb->local_ip) ||
             ip_addr_cmp(&(pcb->local_ip), &(iphdr->dest)))) {
        return 1;
    } else {  
        for(pcb = udp_pcbs; pcb != NULL; pcb = pcb->next) {
            if(pcb->remote_port == src &&
                    pcb->local_port == dest &&
                    (ip_addr_isany(&pcb->remote_ip) ||
                     ip_addr_cmp(&(pcb->remote_ip), &(iphdr->src))) &&
                    (ip_addr_isany(&pcb->local_ip) ||
                     ip_addr_cmp(&(pcb->local_ip), &(iphdr->dest)))) {
                pcb_cache = pcb;
                break;
            }
        }

        if(pcb == NULL) {
            for(pcb = udp_pcbs; pcb != NULL; pcb = pcb->next) {
                if(pcb->local_port == dest &&
                        (ip_addr_isany(&pcb->remote_ip) ||
                         ip_addr_cmp(&(pcb->remote_ip), &(iphdr->src))) &&
                        (ip_addr_isany(&pcb->local_ip) ||
                         ip_addr_cmp(&(pcb->local_ip), &(iphdr->dest)))) {
                    break;
                }
            }
        }
    }

    if(pcb != NULL) {
        return 1;
    } else {  
        return 1;
    }
}
#endif /* LWIP_DEBUG */

void
udp_input(struct pbuf *p, struct netif *inp)
{
    struct udp_hdr *udphdr;  
    struct udp_pcb *pcb;
    struct ip_hdr *iphdr;
    u16 src, dest;

    int i;

#ifdef UDP_STATS
    ++stats.udp.recv;
#endif /* UDP_STATS */

    iphdr = p->payload;

    pbuf_header(p, -(UDP_HLEN + IPH_HL(iphdr) * 4));

    udphdr = (struct udp_hdr *)((u8 *)p->payload - UDP_HLEN);

    DEBUGF("udp_input: received datagram of length %d\r\n", p->tot_len);

    src = ntohs(udphdr->src);
    dest = ntohs(udphdr->dest);

#if UDP_DEBUG
    udp_debug_print(udphdr);

    puts("< UDP packet data >\r\n");
    for (i=0; i<p->len; ++i) {
        DEBUGF("%.2X ", ((u8*)p->payload)[i]);
	    if ((i%16)==15)
	        puts("\r\n");
    }
	puts("\r\n");
#endif /* UDP_DEBUG */
    if (ntohs(udphdr->dest) == 1234) {
        ((u8*)p->payload)[p->len] = '\0';
        printf("\r\n%d.%d.%d.%d > %s\r\n", 
                iphdr->src.addr & 0xFF,
                iphdr->src.addr >> 8 & 0xFF,
                iphdr->src.addr >> 16 & 0xFF, 
                iphdr->src.addr >> 24 & 0xFF, 
                (u8*)p->payload);
    }

    /* Demultiplex packet. First, go for a perfect match. */
    for(pcb = udp_pcbs; pcb != NULL; pcb = pcb->next) {
        DEBUGF("udp_input: pcb local port %d (dgram %d)\r\n",
                pcb->local_port, ntohs(udphdr->dest));
        if(pcb->remote_port == src &&
           pcb->local_port == dest &&
           (ip_addr_isany(&pcb->remote_ip) ||
            ip_addr_cmp(&(pcb->remote_ip), &(iphdr->src))) &&
           (ip_addr_isany(&pcb->local_ip) ||
            ip_addr_cmp(&(pcb->local_ip), &(iphdr->dest)))) {
            break;
        }
    }

    if(pcb == NULL) {
        for(pcb = udp_pcbs; pcb != NULL; pcb = pcb->next) {
            DEBUGF("udp_input: pcb local port %d (dgram %d)\r\n",
                    pcb->local_port, dest);
            if(pcb->local_port == dest &&
               (ip_addr_isany(&pcb->remote_ip) ||
                ip_addr_cmp(&(pcb->remote_ip), &(iphdr->src))) &&
               (ip_addr_isany(&pcb->local_ip) ||
                ip_addr_cmp(&(pcb->local_ip), &(iphdr->dest)))) {
                break;
            }      
        }
    }


    /* Check checksum if this is a match or if it was directed at us. */
    /*  if(pcb != NULL || ip_addr_cmp(&inp->ip_addr, &iphdr->dest)) {*/
    if(pcb != NULL) {
        DEBUGF("udp_input: calculating checksum\r\n");
        pbuf_header(p, UDP_HLEN);    
        if(IPH_PROTO(iphdr) == IP_PROTO_UDPLITE) {    
            /* Do the UDP Lite checksum */
            if(inet_chksum_pseudo(p, (struct ip_addr *)&(iphdr->src),
               (struct ip_addr *)&(iphdr->dest),
                IP_PROTO_UDPLITE, ntohs(udphdr->len)) != 0) {
                DEBUGF("udp_input: UDP Lite datagram discarded due to failing checksum\r\n");
#ifdef UDP_STATS
                ++stats.udp.chkerr;
                ++stats.udp.drop;
#endif /* UDP_STATS */
                pbuf_free(p);
                return;
            }
        } else {
            if(udphdr->chksum != 0) {
                if(inet_chksum_pseudo(p, (struct ip_addr *)&(iphdr->src),
                   (struct ip_addr *)&(iphdr->dest), IP_PROTO_UDP, p->tot_len) != 0) {
                    DEBUGF("udp_input: UDP datagram discarded due to failing checksum\r\n");
#ifdef UDP_STATS
                    ++stats.udp.chkerr;
                    ++stats.udp.drop;
#endif /* UDP_STATS */
                    pbuf_free(p);
                    return;
                }
            }
        }
        pbuf_header(p, -UDP_HLEN);    
        if(pcb != NULL) {
            pcb->recv(pcb->recv_arg, pcb, p, &(iphdr->src), src);
        } else {
            DEBUGF("udp_input: not for us.\r\n");

            /* No match was found, send ICMP destination port unreachable unless
               destination address was broadcast/multicast. */

            if(!ip_addr_isbroadcast(&iphdr->dest, &inp->netmask) &&
               !ip_addr_ismulticast(&iphdr->dest)) {

                /* deconvert from host to network byte order */
                udphdr->src = htons(udphdr->src);
                udphdr->dest = htons(udphdr->dest); 

                /* adjust pbuf pointer */
                p->payload = iphdr;
                icmp_dest_unreach(p, ICMP_DUR_PORT);
            }
#ifdef UDP_STATS
            ++stats.udp.proterr;
            ++stats.udp.drop;
#endif /* UDP_STATS */
            pbuf_free(p);
        }
    } else {
        pbuf_free(p);
    }
}

int
udp_send(struct udp_pcb *pcb, struct pbuf *p)
{
    struct udp_hdr *udphdr;
    struct netif *netif;
    struct ip_addr *src_ip;
    int err;
    struct pbuf *q;

    if(pbuf_header(p, UDP_HLEN)) {
        q = pbuf_alloc(PBUF_IP, UDP_HLEN, PBUF_RAM);
        if(q == NULL) {
            return -1;
        }
        pbuf_chain(q, p);
        p = q;
    }

    udphdr = p->payload;
    udphdr->src = htons(pcb->local_port);
    udphdr->dest = htons(pcb->remote_port);
    udphdr->chksum = 0x0000;

    if((netif = ip_route(&(pcb->remote_ip))) == NULL) {
        DEBUGF("udp_send: No route to 0x%lx\r\n", pcb->remote_ip.addr);
#ifdef UDP_STATS
        ++stats.udp.rterr;
#endif /* UDP_STATS */
        return -1;
    }

    if(ip_addr_isany(&pcb->local_ip)) {
        src_ip = &(netif->ip_addr);
    } else {
        src_ip = &(pcb->local_ip);
    }

    DEBUGF("udp_send: sending datagram of length %d\r\n", p->tot_len);

    if(pcb->flags & UDP_FLAGS_UDPLITE) {
        udphdr->len = htons(pcb->chksum_len);
        /* calculate checksum */
        udphdr->chksum = inet_chksum_pseudo(p, src_ip, &(pcb->remote_ip),
                                            IP_PROTO_UDP, pcb->chksum_len);
        if(udphdr->chksum == 0x0000) {
            udphdr->chksum = 0xffff;
        }
        err = ip_output_if(p, src_ip, &pcb->remote_ip, UDP_TTL, IP_PROTO_UDPLITE, netif);    
    } else {
        udphdr->len = htons(p->tot_len);
        /* calculate checksum */
        if((pcb->flags & UDP_FLAGS_NOCHKSUM) == 0) {
            udphdr->chksum = inet_chksum_pseudo(p, src_ip, &pcb->remote_ip,
                                                IP_PROTO_UDP, p->tot_len);
            if(udphdr->chksum == 0x0000) {
                udphdr->chksum = 0xffff;
            }
        }
        err = ip_output_if(p, src_ip, &pcb->remote_ip, UDP_TTL, IP_PROTO_UDP, netif);    
    }

#ifdef UDP_STATS
    ++stats.udp.xmit;
#endif /* UDP_STATS */
    return err;
}

int
udp_bind(struct udp_pcb *pcb, struct ip_addr *ipaddr, u16 port)
{
    struct udp_pcb *ipcb;
    ip_addr_set(&pcb->local_ip, ipaddr);
    pcb->local_port = port;

    /* Insert UDP PCB into the list of active UDP PCBs. */
    for(ipcb = udp_pcbs; ipcb != NULL; ipcb = ipcb->next) {
        if(pcb == ipcb) {
            /* Already on the list, just return. */
            return 0;
        }
    }
    /* We need to place the PCB on the list. */
    pcb->next = udp_pcbs;
    udp_pcbs = pcb;

    DEBUGF("udp_bind: bound to port %d\r\n", port);
    return 0;
}

int
udp_connect(struct udp_pcb *pcb, struct ip_addr *ipaddr, u16 port)
{
    struct udp_pcb *ipcb;
    ip_addr_set(&pcb->remote_ip, ipaddr);
    pcb->remote_port = port;

    /* Insert UDP PCB into the list of active UDP PCBs. */
    for(ipcb = udp_pcbs; ipcb != NULL; ipcb = ipcb->next) {
        if(pcb == ipcb) {
            /* Already on the list, just return. */
            return 0;
        }
    }
    /* We need to place the PCB on the list. */
    pcb->next = udp_pcbs;
    udp_pcbs = pcb;
    return 0;
}

void
udp_recv(struct udp_pcb *pcb,
	     void (* recv)(void *arg, struct udp_pcb *upcb, struct pbuf *p,
		               struct ip_addr *addr, u16 port),
	     void *recv_arg)
{
    pcb->recv = recv;
    pcb->recv_arg = recv_arg;
}

void
udp_remove(struct udp_pcb *pcb)
{
    struct udp_pcb *pcb2;

    if(udp_pcbs == pcb) {
        udp_pcbs = udp_pcbs->next;
    } else for(pcb2 = udp_pcbs; pcb2 != NULL; pcb2 = pcb2->next) {
        if(pcb2->next != NULL && pcb2->next == pcb) {
            pcb2->next = pcb->next;
        }
    }

    Mem.free(pcb);  
}

struct udp_pcb *
udp_new(void) 
{
    struct udp_pcb *pcb;
    pcb = Mem.alloc(sizeof(struct udp_pcb), 0);
    if(pcb != NULL) {
        memset(pcb, 0, sizeof(struct udp_pcb));
        return pcb;
    }
    return NULL;

}

#if UDP_DEBUG
int
udp_debug_print(struct udp_hdr *udphdr)
{
    DEBUGF("UDP header:\r\n");
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|     %5d     |     %5d     | (src port, dest port)\r\n",
            ntohs(udphdr->src), ntohs(udphdr->dest));
    DEBUGF("+-------------------------------+\r\n");
    DEBUGF("|     %5d     |     0x%04x    | (len, chksum)\r\n",
            ntohs(udphdr->len), ntohs(udphdr->chksum));
    DEBUGF("+-------------------------------+\r\n");
    return 0;
}
#endif /* UDP_DEBUG */
