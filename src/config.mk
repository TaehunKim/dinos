MACH ?= vexpress-a9
ARCH ?= arm
CROSS_COMPILE ?= arm-none-eabi-

AS	= $(CROSS_COMPILE)as
LD	= $(CROSS_COMPILE)ld
CC	= $(CROSS_COMPILE)gcc
CPP	= $(CC) -E
AR	= $(CROSS_COMPILE)ar
NM	= $(CROSS_COMPILE)nm
STRIP	= $(CROSS_COMPILE)strip
OBJCOPY = $(CROSS_COMPILE)objcopy
OBJDUMP = $(CROSS_COMPILE)objdump
RANLIB	= $(CROSS_COMPILE)ranlib

ifeq ($(BUILD_VERBOSE),1)
	Q_AS		=
	Q_LD		=
	Q_CC		=
	Q_CPP		=
	Q_AR		=
	Q_NM		=
	Q_STRIP		=
	Q_OBJCOPY	=
	Q_OBJDUMP	=
	Q_RANLIB	=
else
	Q_AS		= @echo '    AS      '$(CUR_DIR)$@;
	Q_LD		= @echo '    LINK    '$(CUR_DIR)$@;
	Q_CC		= @echo '    CC      '$(CUR_DIR)$@;
	Q_CPP		= @echo '    CPP     '$(CUR_DIR)$@;
	Q_AR		= @echo '    AR      '$(CUR_DIR)$@;
	Q_NM		= @echo '    NM      '$(CUR_DIR)$@;
	Q_STRIP		= @echo '    STRIP   '$(CUR_DIR)$@;
	Q_OBJCOPY	= @echo '    OBJCOPY '$(CUR_DIR)$@;
	Q_OBJDUMP	= @echo '    OBJDUMP '$(CUR_DIR)$@;
	Q_RANLIB	= @echo '    RANLIB  '$(CUR_DIR)$@;
endif

ifeq ($(dinos_debug), y)
ASFLAGS += -g
CFLAGS += -g -DDEBUG
endif
ifeq ($(realview), y)
CFLAGS += -DREALVIEW
endif
CFLAGS += -I$(SRCDIR)/include -nostdlib -nodefaultlibs -fno-builtin -O2
LDFLAGS = -static -nostdlib -nostartfiles -nodefaultlibs -p -X
BLDFLAGS = -static -nostdlib -nostartfiles -nodefaultlibs -p -X 

export ARCH
