/* thread/sched.c
 *
 * Scheduler module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <list.h>
#include <dinos/sched.h>
#include <dinos/thread.h>

#define MODULE "Sched"

/* Global kernel tick. */
unsigned int tick;
/* The list head of run queue. */
//static struct list_item rq_head;
struct list_item rq_head;
/* The thread count in run queue. */
int sched_count;

static int sched_init(void)
{
    list_init(&rq_head);

    log("Initialize Sched module.\n");
    
    return 0;
}

static void sched_start(void)
{
    struct thread *first = NULL;

    /* Empty runqueue. */
    if (sched_count == 0) {
        panic("%s: Runqueue Empty!\n", __func__);
        return;
    }
    DEBUGF("rq_head = 0x%X\n", (u32)&rq_head);

    first = list_get_entry(rq_head.next, struct thread, rq_item);
    DEBUGF("%s: Threads count = %d, Thread = %s, func = %X\n", __func__, sched_count, first->name, first->func);

    arch_thread_load(first);
}

static void schedule(void)
{
    struct thread *curr, *next;
    struct list_item *next_item;

    /* Empty run queue. */
    if (sched_count == 0) {
        panic("%s: Runqueue Empty!\n", __func__);
        return;
    }

    curr = Thread.cur();
    next_item = curr->rq_item.next;

    if (next_item == &rq_head)
        next_item = next_item->next;
    next = list_get_entry(next_item, struct thread, rq_item);

    if (curr != next) {
        list_move_tail(&curr->rq_item, &rq_head);
        arch_context_switch(curr, next);
    }
}

static void sched_add(struct thread *t)
{
    DEBUGF("Add thread in runqueue. tid = %d, name = %s\n", t->tid, t->name);
    list_add_tail(&t->rq_item, &rq_head);
    ++sched_count;
}

static void sched_del(struct thread *t)
{
    list_del(&t->rq_item);
    --sched_count;
}

static struct list_item *sched_get_list(void)
{
    return &rq_head;
}

/* Sched module */ 
struct Sched Sched = {
    .init       = sched_init,
    .schedule   = schedule,
    .start      = sched_start,
    .add        = sched_add,
    .del        = sched_del,
    .get_list   = sched_get_list,
};
