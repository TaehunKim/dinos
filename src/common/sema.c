/* ipc/sema.c
 *
 * Semaphore for syncronization.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <delay.h>
#include <dinos/sema.h>
#include <dinos/thread.h>
#include <dinos/irq.h>

/**
 * @brief		Initialize semaphore.
 * @param[in]	sem To initialize semaphore.
 * @param[in]	count Initial count for semaphore.
 */
void
sema_init(struct semaphore *sem, int count)
{
	sem->count = count;
	list_init(&sem->wait_head);
}

/**
 * @brief		Down semaphore.
 * @param[in]	sem To down semaphore.
 */
void
sema_down(struct semaphore *sem)
{
	unsigned int flag;

	flag = Irq.save();
	if (sem->count > 0) {
		--sem->count;
	} else {
		struct semaphore_waiter waiter;
		waiter.thread = Thread.cur();
		waiter.wake = 0;

		/** Wait for semaphore.  */
		list_add_tail(&waiter.wait_item, &sem->wait_head);
		wait(waiter.wake == 0);
	}
	Irq.restore(flag);
}

/**
 * @brief		Up semaphore.
 * @param[in]	sem To up semaphore.
 */
void
sema_up(struct semaphore *sem)
{
	unsigned int flag = Irq.save();

	if (list_empty(&sem->wait_head)) {
		++sem->count;
	} else {
		struct semaphore_waiter *waiter = list_get_entry(sem->wait_head.next, struct semaphore_waiter, wait_item);
		list_del(&waiter->wait_item);
		waiter->wake = 1;
	}
	Irq.restore(flag);
}
