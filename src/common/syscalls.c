/* common/syscalls.c
 *
 * System calls.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifdef DEBUG
#include <stdio.h>
#endif
#include <dinos/file.h>
#include <dinos/process.h>
#include <delay.h>

long sys_open(const char *name, int flag, int mode)
{
    DEBUGF("Syscall %s: %s file open, mode = 0x%X\n", __func__, name, mode);
	return Vfs.open(name, flag, mode);
}

long sys_read(int fd, char *buf, int len)
{
    DEBUGF("Syscall %s: fd = %d, buf = 0x%X, len = %d\n", __func__, fd, (u32)buf, len);
	return Vfs.read(fd, buf, len);
}

long sys_write(int fd, char *buf, int len)
{
    DEBUGF("Syscall %s: fd = %d, buf = 0x%X, len = %d\n", __func__, fd, (u32)buf, len);
	return Vfs.write(fd, buf, len);
}

long sys_close(int fd)
{
    DEBUGF("Syscall %s: fd = %d\n", __func__, fd);
    return Vfs.close(fd);
}

long sys_fork(void)
{
    int tid = -1;
    DEBUGF("Syscall %s\n", __func__);
	return tid;
}

long sys_exec(char *file_name, char *argv[])
{
    DEBUGF("Syscall %s\n", __func__);
	return 0;
}

long sys_exit(int status)
{
    DEBUGF("Syscall %s: pid = %d\n", __func__, Process.cur()->pid);
    Process.exit();
	return 0;
}

long sys_getpid(void)
{
    DEBUGF("Syscall %s\n", __func__);
    return Process.cur()->pid;
}

long sys_gettick(void)
{
    DEBUGF("Syscall %s\n", __func__);
    return tick;
}
