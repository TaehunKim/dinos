/* mem/paging.c
 *
 * Paging module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * TODO:
 *  - Change routine memory allocation.
 */

#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <dinos/mem.h>
#include <dinos/paging.h>

#include <arch/paging.h>
#include <arch/map.h>
#include <mach/board.h>

#define MODULE "Paging"

static void paging_init(void)
{
    log("Initialize Paging module.\n");
}

static pte_t *paging_cur_pt(void)
{
    return arch_cur_pt();
}

static int paging_set_pt(pte_t *pt)
{
    if (!is_align(L1_PT_SIZE, (u32)pt)) 
        return -EINVAL;
    arch_set_pt(pt);
}

static void paging_tlb_clear(void)
{
    arch_tlb_clear();
}

static int paging_enable(pte_t *pt)
{
#if 0
    int i, ret;
    struct map_desc *board_mapdesc = board_region.map;

    assert(pt != NULL);

    log("Pagine Enable.\n");
    DEBUGF("memory area count = %d\n", board_region.count);

    for (i = 0; i < board_region.count; ++i) {
        ret = Paging.addr_mapping(pt, board_mapdesc[i].virt, board_mapdesc[i].phys,
                board_mapdesc[i].size, board_mapdesc[i].type | P_KERN);
        if (ret < 0)
            return ret;
    }

    DEBUGF("memory area mapping done!\n");
    arch_tlb_clear();
    arch_set_pt(pt);
#endif
    arch_paging_enable();

    return 0;
}

static pte_t *paging_alloc_pt(void)
{
    int i;
    pte_t *pt = (pte_t *)Mem.alloc_pages_align(L1_PT_SIZE / PAGE_SIZE, L1_PT_SIZE / PAGE_SIZE);
    assert(is_align(L1_PT_SIZE, (u32)pt));

    arch_pt_init(pt, L1_PT_ENTRIES);

    return pt;
}

static int paging_addr_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, size_t size, u32 flag, size_t pg_size)
{
    int ret;
    u32 i;
    int (*map_func)(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag);

    DEBUGF("Address mapping vaddr = 0x%.8X, paddr = 0x%.8X, size = 0x%X, page size = 0x%X.\n", vaddr, paddr, size, pg_size);

    if (!is_align(PAGE_SIZE, vaddr) && !is_align(PAGE_SIZE, paddr))
        return -EINVAL;

    switch (pg_size) {
        case 0x1000000:
            DEBUGF("Super section(16M) page.\n");
            map_func = arch_super_section_mapping;
            break;
        case 0x100000:
            DEBUGF("Section(1M) page.\n");
            map_func = arch_section_mapping;
            break;
        case 0x1000:
            DEBUGF("4K page.\n");
            map_func = arch_page_mapping;
            break;
        default:
            panic("Invalid page size.\n");
            break;
    }
    for (i = 0; i < size; i += pg_size) {
        if ((ret = map_func(pt, vaddr + i, paddr + i, flag)) < 0)
            return ret;
    }

    return ret;
}

struct Paging Paging = {
    .init           = paging_init,
    .alloc_pt       = paging_alloc_pt,
    .cur_pt         = paging_cur_pt,
    .set_pt         = paging_set_pt,
    .tlb_clear      = paging_tlb_clear,
    .enable         = paging_enable,
    .addr_mapping   = paging_addr_mapping,
};
