/* common/ksh.c
 *
 * The kernel shell for kernel function test.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <config.h>
#include <delay.h>
#include <ctype.h>

#include <dinos/thread.h>
#include <dinos/sched.h>
#include <dinos/file.h>
#include <dinos/io.h>
#include <dinos/process.h>
#include <dinos/mem.h>
#include <dinos/paging.h>

//#include <net/udp.h>
//#include <net/pbuf.h>
#include <mach/board.h>

#include "ksh.h"

#define MODULE "Ksh"

static int do_mem(char *cmd)
{
    Mem.status();

    return 0;
}

static int do_help(char *cmd)
{
	struct list_item *it;
	struct cmd *cmdp;

	printf("%-30s%-30s\r\n", "< Commands >", "< Description >");
	list_for_each(it, cmd_list.head) {
		cmdp = list_get_entry(it, struct cmd, item);
		printf("%-30s- %-30s\r\n", cmdp->name, cmdp->help);
		cmdp->name;
	}
}

static char *mem[100];
static int mem_count = 0;
#if 0
const int alloc_size = 25*1024*1024; /* 25MB */;

static int do_alloc(char *cmd)
{
    unsigned int flags = Irq.save();

    mem[mem_count] = (char *)Mem.alloc(alloc_size, 0);
    Irq.restore(flags);
    if (!mem[mem_count]) {
        printf("Memory full.\n");
        return;
    }
    printf("Memory Allocation %d Kbyte(%d MB), address = %p\r\n", 
            alloc_size/1024, alloc_size/(1024*1024),mem[mem_count++]);
}

static int do_free(char *cmd)
{
    if (mem_count > 0) {
        Mem.free(mem[--mem_count]);
        printf("Memory Free %d Kbyte(%d MB), address %p\r\n", 
                alloc_size/1024, alloc_size/(1024*1024), mem[mem_count]);
    }
}
#else /* For small size test */

const int alloc_size = 100;

static int do_alloc(char *cmd)
{
    unsigned int flags = Irq.save();

    mem[mem_count] = (char *)Mem.alloc(alloc_size, 0);
    Irq.restore(flags);
    if (!mem[mem_count]) {
        printf("Memory full.\n");
        return;
    }
    printf("Memory Allocation %d byte, address = %p\r\n", alloc_size, mem[mem_count++]);
}

static int do_free(char *cmd)
{
    if (mem_count > 0) {
        Mem.free(mem[--mem_count]);
        printf("Memory Free %d byte, address = %p\r\n", alloc_size, mem[mem_count++]);
    }
}
#endif

static int do_ps(char *cmd)
{
    struct list_item *rq_head = Sched.get_list();
    struct list_item *it;
    struct thread *t;
    unsigned int flags;

    printf("<-- Thread List -->\r\n\r\n");
    printf("%7s%10s%13s%10s%15s%10s%12s\r\n",
	   "< TID >", "< Name >", "< Status >", "< Mode >", "< Priority >", "< Tick >", "< Process >");
    flags = Irq.save();
    list_for_each(it, *rq_head) {
	t = list_get_entry(it, struct thread, rq_item);
	printf("%7d%10s%13s%10s%15d%10d%12s\r\n",
	       t->tid, t->name, t->status == 0 ? "RUNNING" : "WAIT",
	       t->func == NULL ? "user" : "kernel", t->prio, t->tick, 
           t->proc == NULL ? "NULL" : t->proc->name);
    }
    Irq.restore(flags);
}

static int do_thread1(char *cmd)
{
    unsigned int flags = Irq.save();
    struct thread *t;
    t = Thread.create("app", DEF_USR_PRIO, NULL, NULL, 0);
    printf("Create user mode thread! tid = %d\r\n", t->tid);
    //Thread.exec(tid, "test2");
    Irq.restore(flags);
}

static int do_thread2(char *cmd)
{
    unsigned int flags = Irq.save();
    struct thread *t;
    t= Thread.create("app_sys", DEF_USR_PRIO, NULL, NULL, 0);
    printf("Create user mode thread using system call! tid = %d\r\n", t->tid);
    //Thread.exec(tid, "test");
    Irq.restore(flags);
}

void *kernel_thread1(void *data)
{
    u32 i = 0;
    while (1) {
        ++i;
    }
}

void *kernel_thread2(void *data)
{
    int i=0;

    while (1) {
        printf("[TID %d] Kernel mode thread, my count %d\r\n", Thread.cur()->tid, i);
        ++i;
        sleep(1);
    }
}

char ori[] = {
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
    31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    77,
};

/* For memory allocation test.*/
void *kernel_thread3(void *data)
{
    int i;
    while (1) {
        char *des = (char *)Mem.alloc(sizeof(ori), 0);
        bzero(des, sizeof(ori));

        for (i = 0; i < sizeof(ori); ++i)
            des[i] = i + 1;
        des[i - 1] = 77;

        for (i = 0; i < sizeof(ori); ++i)
            assert(ori[i] == des[i]);
        Mem.free(des);
    }
}

static int do_thread3(char *cmd)
{
    struct thread *t;
    t = Thread.create("kthread 1", DEF_KER_PRIO, Thread.cur()->proc, kernel_thread3, 0);
    printf("Create kernel mode thread tid = %d\r\n", t->tid);
}

static int do_thread4(char *cmd)
{
    Process.create("process", DEF_KER_PRIO, 0, kernel_thread2, 0);
}

static int do_kill(char *cmd)
{
    int tid;
    struct thread *t;
    cmd += 5; /* Skip 'kill' command. */
	/* Skip space. */
	while (isspace(*cmd))
		cmd++;
    tid = simple_atoi(cmd);
    t = Thread.get(tid);
    if (!strcmp(t->name, "ksh") || t == NULL)
        return 0;
    printf("Terminate %s thread tid = %d\r\n", t->name, tid);
    Thread.exit_tid(tid);
}

#if 0
static int do_send(char *cmd)
{
    struct udp_pcb *pcb = udp_new();
    struct pbuf *p;
    struct ip_addr remote = {.addr = htonl(0xc0a8017d)};
    struct ip_addr local = {.addr = htonl(0xc0a80102)};
    int len;

    ip_addr_set(&pcb->local_ip, &local);
    ip_addr_set(&pcb->remote_ip, &remote);
    pcb->local_port = 1234;
    pcb->remote_port = 12345;

    cmd += 4; /* Skip 'send' command. */
    /* Skip space. */
    while (isspace(*cmd))
        cmd++;

    len = strlen(cmd);
    if (len%2 == 1)
        ++len;
    p = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_RAM);
    memcpy(p->payload, cmd, strlen(cmd));

    printf("send message: %s\r\n", cmd);

    udp_send(pcb, p);
}
#endif

static int do_exec(char *cmd)
{
    Process.exec("/pysh");
}

/* command run loop. */
static void cmd_run(char *cmd)
{
	struct list_item *it;
	struct cmd *cmdp;

	list_for_each(it, cmd_list.head) {
		cmdp = list_get_entry(it, struct cmd, item);
		if (!strncmp(cmd, cmdp->name, strlen(cmdp->name))) {
			cmdp->func(cmd);
		}
	}
}

void init_cmd(const char *name, int argc, int (*func)(char *), const char *help,
		 struct cmd *cmd)
{
	strcpy(cmd->name, name);
	cmd->argc = argc;
	cmd->func = func;
	strcpy(cmd->help, help);
	list_add_tail(&cmd->item, &cmd_list.head);
}

void init_cmdlist(void)
{
	list_init(&cmd_list.head);
	cmd_list.cmd_count = 0;
}

char cmd[CFG_CBSIZE];
struct cmd help_cmd;
struct cmd ls_cmd;
struct cmd ps_cmd;
struct cmd mem_cmd;
struct cmd thread1_cmd;
struct cmd thread2_cmd;
struct cmd thread3_cmd;
struct cmd thread4_cmd;
struct cmd kill_cmd;
struct cmd mt_cmd;
struct cmd mt2_cmd;
struct cmd free_cmd;
struct cmd send_cmd;
struct cmd exec_cmd;

/* Kernel shell for kernel function test. */
void *sh_thread(void *data)
{
    int i;
        
    puts("\r\n< The DINOS Kernel Shell >\r\n");
    puts("Author: Taehun kim <kth3321@gmail.com>\r\n\n");

    init_cmdlist();

    /* Initialize commands. */
    init_cmd("help",    0, do_help,     "Help commands",                &help_cmd);
    init_cmd("ps",      0, do_ps,       "Display threads infomation",   &ps_cmd);
    init_cmd("mem",     0, do_mem,      "Display Memory information",   &mem_cmd);
    init_cmd("c1",      0, do_thread1,  "Create user mode thread",      &thread1_cmd);
    init_cmd("c2",      0, do_thread2,  "Crate user mode thread",       &thread2_cmd);
    init_cmd("c3",      0, do_thread3,  "Crate kernel mode thread",     &thread3_cmd);
    init_cmd("c4",      0, do_thread4,  "Crate kernel mode process",    &thread4_cmd);
    init_cmd("kill",    0, do_kill,     "Thread termination",           &kill_cmd);
    init_cmd("alloc",   0, do_alloc,    "Memory allocation test",       &mt_cmd);
    init_cmd("free",    0, do_free,     "Memory free test",             &free_cmd);
    init_cmd("exec",    0, do_exec,     "Program execution test",       &exec_cmd);
    //init_cmd("send",    0, do_send,     "Network send meaage",          &send_cmd);

    while (1) {
        bzero(cmd, CFG_CBSIZE);
        for (i = 0; i < CFG_CBSIZE; ++i)
            cmd[i] = 0;
        readline("KSH # ", cmd);
        cmd_run(cmd);
    }

    return 0;
}

