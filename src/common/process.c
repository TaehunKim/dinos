/* thread/process.c
 *
 * Process module.
 *
 * Copyright (C) 2014 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <list.h>
#include <assert.h>
#include <arch/errno.h>
#include <arch/map.h>
#include <dinos/irq.h>
#include <dinos/mem.h>
#include <dinos/thread.h>
#include <dinos/process.h>
#include <dinos/paging.h>
#include <dinos/sched.h>
#include <dinos/file.h>

#define MODULE "Process" 

static u32 process_count = 0;
static struct list_item all_process;

static int process_init(struct thread *t)
{
    struct process *p = Mem.alloc_pages(1);

    log("Initialize Process module.\n");
    list_init(&all_process);

    /* Initialize 'dangun' process. */
    p->pid = process_count++;
    p->pt = Paging.cur_pt();
    strcpy(p->name, t->name);
    t->proc = p;

    return 0;
}

static struct process *process_create(const char *name, int prio, u32 load_addr,
        void *(*func)(void *), void *data)
{
    u32 flag = Irq.save();
    struct process *p = Mem.alloc_pages(1);
    struct thread *t;

    DEBUGF("%s: name = %s, prio = %d, load_addr = %X, func = 0x%X\n",
            __func__, name, prio, load_addr, (u32)func);
    p->pid = process_count++;
    p->pt = Paging.alloc_pt();
    memcpy(p->pt, Process.cur()->pt, sizeof(pte_t) *  L1_PT_ENTRIES);
    p->pt = (pte_t *)v2p((u32)p->pt);
    strcpy(p->name, name);

    list_init(&p->threads);
    list_add_tail(&p->all_item, &all_process);

    if (!func) {
        Paging.addr_mapping(p->pt, 0x01000000, load_addr, 0x100000, MT_MEMORY | P_ALL, 0x100000);
    }

    t = Thread.create(name, prio, p, func, data);

    list_add(&t->all_item, &p->threads);
    Irq.restore(flag);
    return p;
}

static struct process *process_cur(void)
{
    return Thread.cur()->proc;
}
               
static struct process *process_get(int pid)
{
    struct list_item *it;
    struct process *p = NULL;

    list_for_each(it, all_process) {
        p = list_get_entry(it, struct process, all_item);
    }

    return NULL;
}

static int process_add_thread(int pid, struct thread *t)
{
    struct process *p = process_get(pid);

    list_add_tail(&t->all_item, &p->threads);

    return 0;
}

static int process_exit(void)
{
    u32 flag = Irq.save();
    struct process *p = process_cur();
    struct list_item *it;
    struct thread *t;

    list_for_each(it, p->threads) {
        t = list_get_entry(it, struct thread, all_item);
        Thread.exit_tid(t->tid);
    }

    Mem.free_pages((void*)p2v((u32)p->pt));
    Mem.free_pages(p);
    Irq.restore(flag);

    /* Execute next thread. */
    Sched.start();

    /* Do not execute this line. */
    panic("Process exit fail!\n");
    
    return 0;
}

static int process_exit_pid(int pid)
{
    u32 flag = Irq.save();
    struct process *p = process_get(pid);
    struct list_item *it;
    struct thread *t;

    list_for_each(it, p->threads) {
        t = list_get_entry(it, struct thread, all_item);
        Thread.exit_tid(t->tid);
    }

    Mem.free_pages(p);
    Irq.restore(flag);

    /* Execute next thread. */
    Sched.start();

    /* Do not execute this line. */
    panic("Process exit fail!\n");
 
    return 0;
}

static int process_exec(const char *file_name)
{
    int i, fsz = 0;
    int fd = Vfs.open(file_name, 0, 0);
    char *pro = Mem.alloc_pages_align(0x100, 0x100);

    /* Read executable file. */
    Vfs.read(fd, (char *)&fsz, 4);
    DEBUGF("%s: File size = %d\n", __func__, fsz);
    Vfs.read(fd, pro, 0x100000);

    /* Create process by executable file. */
    Process.create(file_name, DEF_USR_PRIO, v2p((u32)pro), NULL, NULL);
}

struct Process Process = {
    .init       = process_init,
    .create     = process_create,
    .add_thread = process_add_thread,
    .cur        = process_cur,
    .get        = process_get,
    .exit       = process_exit,
    .exit_pid   = process_exit_pid,

    .exec       = process_exec,
};
