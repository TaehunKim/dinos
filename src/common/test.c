#ifdef DEBUG
#include <stdio.h>
#include <string.h>
#include <delay.h>
#include <dinos/mem.h>
#include <dinos/io.h>
#include <dinos/sched.h>
#include <dinos/irq.h>
#include <dinos/file.h>
#include <dinos/paging.h>
#include <delay.h>
#include <mach/gic.h>

#include <mach/motherboard.h>

extern void timer_isr(void *);

void do_timer(void)
{
    u32 timer_base = 0x100e4000;
    /* (Enable) | (Period) | (Int. Enable) | (Pre Scale) | (Timer Size) | (OneShot) */
    u32 cntl = (0x1 << 7) | (0x1 << 6) | (0x1 << 5) | (0x0 << 2) | (0x1 << 1) | 0x0;
    u32 count;
    int i;

    DEBUGF("Timer test!\n");

    Io.writel(timer_base + 0x8, 0);

    DEBUGF("TIMER1 int status = 0x%X\n", Io.readl(timer_base + 0x10));

    /* Set Timer1 load register. */
    Io.writel(timer_base + 0x0, 0xfffff);

    /* Clear timer interrupt. */
    Io.writel(timer_base + 0xc, 0x1);

    /* Set Timer1 Control register. */
    Io.writel(timer_base + 0x8, cntl);

    /* To test */
    Io.writel(0x1e001000 + GIC_DIST_ENABLE_SET + 4, 0xffffffff);
    Io.writel(0x1e001000 + GIC_DIST_ENABLE_SET + 8, 0xffffffff);

    Irq.request(0x50, "timer", timer_isr, NULL);

    Irq.enable();

    while(1) {
        for(i = 0; i < 0x70000000; ++i);
    }
}

void test()
{
    DEBUGF("This is test routine.\n start test {{{\n");
#if 1
    /* Syscall test */
    {
        unsigned long vbar=0x60000000;
        unsigned long vbar2=0;
        char *name = "/test";

        asm("mrc p15, 0, %0, c12, c0, 0":"=r" (vbar2)::"memory", "cc");
        DEBUGF("VBAR after = 0x%X\n", vbar2);
        asm(
                "mov r0, %0\n"
                "mov r1, #0\n"
                "mov r2, #0\n"
                "swi 0\n"
                :: "r" (name)
           );
    }
#endif

    /* Timer test */
    //do_timer();

    /* CLCD test */
#if 1
    {
        int fd = Vfs.open("/dev/video", 0, 0);
    
        /* Display dumy image from NOR0 flash to VIDEO SRAM. */
        Vfs.write(fd, (char *)V2M_NOR0, 1024 * 768);
        mdelay(500);
        Vfs.close(fd);
    }
#endif

    /* Read CPU count */
    {
        u32 ncpu = Io.readl(0xde000000 + 0x4);
        DEBUGF("We have %d cores.\n", (ncpu & 0x3) + 1);
    }

    /* tick test */
#if 0
    {
        u32 i;

        DEBUGF("current = %s\n", Thread.cur()->name);
        while (1) {
            for(i = 0; i < 0x7000000; ++i);
            DEBUGF("tick = %u, therad tick = %u\n", tick, Thread.cur()->tick);
        }
    }
#endif
    DEBUGF("\nend test }}}\n"); 
}
#endif
