/* common/ksh.h
 *
 * Kernel Shell Header File.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _SH_H_
#define _SH_H_
#include <stdio.h>
#include <list.h>

struct cmd {
	char name[32];
	int argc;
	int (*func)(char *cmd);
	struct list_item item;
	char help[128];
};

struct cmd_list {
	int cmd_count;
	struct list_item head;
} cmd_list;

#endif
