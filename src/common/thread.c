/* thread/thread.c
 *
 * Thread module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * TODO:
 *  - Thread exit: Stack clean up before execution next thread.
 */

#include <stdio.h>
#include <list.h>
#include <config.h>
#include <string.h>
#include <errno.h>

#include <dinos/thread.h>
#include <dinos/sched.h>
#include <dinos/irq.h>
#include <dinos/mem.h>
#include <dinos/paging.h>
#include <dinos/process.h>
#include <arch/context.h>

#define MODULE "Thread"

static int thread_count = 0;
static struct list_item all_thread;

extern char __stack_start[];

static struct thread *thread_init(void)
{
    struct thread *t = (struct thread *)(__stack_start - CFG_KSTACK_SIZE);

    log("Initialize Thread module.\n");
    DEBUGF("__stack_start = %X, TCB address = %X\n", __stack_start, (u32)t);

    /* Set TCB about "Dangun" thread. */
    t->cpu_id = -1;
    t->tid = 0;
    t->prio = 0;
    t->stack_top = __stack_start;
    strcpy(t->name, "dangun");
    t->tick = 0;
    t->status = THREAD_RUNNING;
    t->heap = NULL;

    list_init(&all_thread);

    return t;
}

static struct thread *thread_current(void)
{
    return (struct thread *)(arch_get_sp() & ~(CFG_KSTACK_SIZE - 1));
}

static struct thread *thread_create(const char *name, int prio, struct process *p,
        void *(*func)(void *), void *data)
{
    unsigned int flags = Irq.save();
    struct thread *t = (struct thread *)Mem.alloc_pages(CFG_KSTACK_SIZE >> PAGE_SHIFT);

    if (t == NULL)
        return NULL;
    t = (struct thread *)((unsigned int)t & 0xFFFFFFF0);

    t->cpu_id = -1; /* TODO: depend on SMP. */
    t->tid = thread_count++;
    t->tick = DEF_TICK;
    t->prio = prio;
    t->proc = p;
    t->stack_top = (unsigned char*)t + CFG_KSTACK_SIZE - sizeof(struct context);
    strcpy(t->name, name);
    t->status = THREAD_RUNNING;
    t->func = func;

    list_add_tail(&t->all_item, &all_thread);

    arch_context_set(t, func, data);

    DEBUGF("Create %s thread: TCB = 0x%X, tid = %d, name = %s, prio = %d\n", 
            t->name, (u32)t, t->tid, t->name, t->prio);

    Sched.add(t);
    Irq.restore(flags);

    return t;
}

static int thread_exit(void)
{
    unsigned int flag;
    struct thread *t;
    DEBUGF("%s\n", __func__);

    flag = Irq.save();
    t = thread_current();

    Sched.del(t);
    list_del(&t->all_item);
    if (t->heap)
        Mem.free_heap(t->heap);
    Mem.free_pages(t);
    Irq.restore(flag);

    /* TODO: stack clear & check. */

    /* Execute next thread. */
    Sched.start();

    /* Do not execute this line. */
    panic("Thread exit fail!\n");

    return 0;
}

struct thread *thread_get(int tid)
{
    struct list_item *it;
    struct thread *t = NULL;

    list_for_each(it ,all_thread) {
        t = list_get_entry(it, struct thread, all_item);
        if( t->tid == tid ) {
            return t;
        }
    }
    return NULL;
}

static int thread_exit_tid(int tid)
{
    unsigned int flag = Irq.save();
    struct thread *t;

    t = thread_get(tid);
    if (t == NULL)
        return -1;
    DEBUGF("%s\n", __func__);

    Sched.del(t);
    list_del(&t->all_item);
    if (t->heap)
        Mem.free_heap(t->heap);
    Mem.free_pages(t);
    Irq.restore(flag);

    /* Execute next thread. */
    Sched.start();

    /* Do not execute this line. */
    panic("Thread exit fail!\n");

    return 0;
}

struct Thread Thread = {
    .init       = thread_init,
    .create     = thread_create,
    .cur        = thread_current,
    .exit       = thread_exit,
    .exit_tid   = thread_exit_tid,
    .get        = thread_get,
};
