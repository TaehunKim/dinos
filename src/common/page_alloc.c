/* mem/page_alloc.c
 *
 * Memory allocator per page.
 *
 * Copyright (C) 2012 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <assert.h>
#include <list.h>
#include <errno.h>
#include <config.h>
#include <dinos/mem.h>
#include <dinos/thread.h>
#include <dinos/page_alloc.h>
#include <arch/types.h>

#define MODULE "Mem"

static struct list_item free_list;
static struct page mem_map[MAX_PFN]; 

/*
 * Return physical address corresponding to struct page.
 * = PFN(Page Frame Number) * (page size) + (physical memory start address)
 */
static inline char *page_to_addr(struct page *p)
{
    char *addr = (char *)p;
    return (char *)(((
            (addr - (char *)mem_map) / sizeof(struct page)) /* PFN */
            * PAGE_SIZE)
            + CFG_VA_START);
}

/*
 * Return struct page corresponding to physical address.
 * = PFN(Page Frame Number) * (struct page size) + (mem_map start address)
 */
static inline struct page *addr_to_page(char *addr)
{
    return (struct page *)((
            (u32)(addr - CFG_VA_START) >> PAGE_SHIFT)
            * sizeof(struct page)
            + (char *)mem_map);
}

static inline int addr_to_pfn(char *addr)
{
    return (u32)(addr - CFG_VA_START) >> PAGE_SHIFT;
}

static inline int page_to_pfn(struct page *p)
{
    return (int)(p - mem_map);
}

static inline struct page *pfn_to_page(int pfn)
{
    return &mem_map[pfn];
}

static inline char *pfn_to_addr(int pfn)
{
    return (char *)(CFG_VA_START + (pfn << PAGE_SHIFT));
}

void page_alloc_init(int fstart_pfn, int rpage_start, int rpage_count)
{
    int pfn, i;

    DEBUGF("%s: start pfn = %d, address = 0x%x\n", __func__, fstart_pfn, (fstart_pfn << PAGE_SHIFT) + CFG_VA_START);
    DEBUGF("%s: pfn = %d ~ %d, address = 0x%x reserved\n", __func__, rpage_start, rpage_start + rpage_count, (rpage_start << PAGE_SHIFT) + CFG_VA_START);

    /* Initialize page descriptor for paga allocation. */
    list_init(&free_list);

    pfn = 0;
    while (pfn < MAX_PFN) {
        mem_map[pfn].owner = 0;
        mem_map[pfn].size = 0;
        pfn++;
    }

    /* Free memory area 1 */
    mem_map[fstart_pfn].size = rpage_start - fstart_pfn;
    list_add_tail(&mem_map[fstart_pfn].it, &free_list);

    /* Reverseved area */
    mem_map[rpage_start].size = rpage_count;
    mem_map[rpage_start].owner = -1;

    /* Free memory area 2 */
    mem_map[rpage_start + rpage_count].size = MAX_PFN - (rpage_start + rpage_count);
    list_add_tail(&mem_map[fstart_pfn].it, &free_list);

}

void *alloc_pages(int pages)
{
    struct page *p;
    struct page *alloc_p;
    struct list_item *fit;

    /* First fit */
    list_for_each(fit, free_list) {
        p = list_get_entry(fit, struct page, it);
        if (p->size >= pages)
            goto found;
    }

    DEBUGF("Cannot find free memory area.\n");
    return 0;

found:
    p->size -= pages;
    if (p->size == 0)
        list_del(&p->it);

    alloc_p = (struct page *)(p + p->size);
    alloc_p->owner = Thread.cur()->tid;
    alloc_p->size = pages;

    return page_to_addr(alloc_p);
}

void *alloc_pages_align(int pages, int align)
{
    char *addr = (char *)alloc_pages(pages * 2);
    int pfn = addr_to_pfn(addr);
    int free_tail_sz = pfn % align;
    int alloc_pfn = pfn + (align - free_tail_sz);

    if (!addr)
        panic("Memory full!\n");

    DEBUGF("pfn = 0x%X, alloc_pfn = 0x%X\n", pfn, alloc_pfn);
    mem_map[pfn].size = align - free_tail_sz;
    mem_map[alloc_pfn + align].size = free_tail_sz;
    mem_map[alloc_pfn].size = pages;
    mem_map[alloc_pfn].owner = Thread.cur()->tid;

    free_pages(pfn_to_addr(pfn));
    free_pages(pfn_to_addr(alloc_pfn + align));

    return pfn_to_addr(alloc_pfn);
}

void free_pages(void *addr)
{
    struct page *p;

    assert(is_align(PAGE_SIZE, (addr_t)addr));
    p = addr_to_page(addr);
    p->owner = 0;
    
    list_add_tail(&p->it, &free_list);
}
