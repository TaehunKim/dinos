/* common/init.c
 *
 * DINOS kernel entry point.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <assert.h>
#include <dinos/thread.h>
#include <dinos/file.h>
#include <dinos/mem.h>
#include <dinos/irq.h>
#include <dinos/timer.h>
#include <dinos/paging.h>
#include <dinos/driver.h>
#include <dinos/sched.h>
#include <dinos/init.h>
#include <dinos/process.h>
#include <net/net.h>
#include <mach/board.h>

#define MODULE "Init"

extern void *idle_thread(void *data);
extern void *sh_thread(void *data);

extern initcall_t __initcall_start[], __initcall_end[];

#ifdef DEBUG
extern void test();
#endif

static void do_initcalls(void)
{
    initcall_t *fn;

    for (fn = __initcall_start; fn != __initcall_end; ++fn)
	    (*fn)();
}

void init(int rpages_start, int rpages_count)
{
    int tid;
    int i, ret;
    pte_t *pt;
    struct thread *t;
    struct process *p;

    Irq.disable();

    Vfs.init();
    Console.init();
    log("Starting DINOS kernel...\n");
    DEBUGF("rpage_start = 0x%X, rpage_count = %d\n", rpages_start, rpages_count);

    Mem.init(rpages_start, rpages_count);
    //Paging.init();

    /*
     * Initialize core modules.
     * WARNING: Check module initialization sequence.
     */
    Irq.init();
    t = Thread.init();  /* This is going to make first kernel thread('dangun'). */
    Process.init(t);    /* This is going to initiate the kernel memory area. */
    Sched.init();
    //ret = Paging.enable(t->proc->pt);
    //assert(ret == 0);

    /*
     * Initialize modules registered initcall section.
     * (Device Drivers, Sub Modules, etc.)
     */
    do_initcalls();

    Drv.init();
    timer.init();

    DEBUGF("Create idle threads\n");
    Thread.create("idle", 0, Thread.cur()->proc, idle_thread, 0);
    DEBUGF("Create ksh threads\n");
    Thread.create("ksh", DEF_KER_PRIO, t->proc, sh_thread, 0);

    net.init();
    
    DEBUGF("Timer start\n");
    timer.start();
    DEBUGF("Timer started\n");
    Irq.enable();   /* Now, Timer interrupt will come in. */

#ifdef DEBUG
    test();
#endif

    DEBUGF("Sched start\n");
    Sched.start();	/* It is scheduled to idle thread. */

    /* Do not exectute here. */
    panic("Strage kernel initialize.\n");
}
