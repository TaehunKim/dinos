/* mem/mem.c
 *
 * Memory module.
 *
 * It consists of two-level allocator to allocate memory.
 * The level 1 allocator alloctes per page. The level 2 allocator
 * allocates from kernel heap per byte.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <assert.h>
#include <stdio.h>
#include <list.h>
#include <errno.h>
#include <config.h>
#include <dinos/mem.h>
#include <dinos/thread.h>
#include <dinos/page_alloc.h>
#include <arch/types.h>
#include "tlsf.h"

#define MODULE "Mem"

#define HEAP_SIZE   (PAGE_SIZE * 4)

extern char _kernel_end[];          /* From linker script. */
struct mem_area init_mem_area[3];   /* 'Reserved', 'Kernel', 'Free' areas. */

static void mem_init(int rpage_start, int rpage_count)
{
    int kstart_pfn = 0;
    int kend_pfn = (u32)(_kernel_end - CFG_VA_START) >> PAGE_SHIFT;

    log("Initialize Mem module.\n");
    DEBUGF("Kernel end address = 0x%X\n", (u32)_kernel_end);

    /* Initialize memory areas. */
    list_init(&Mem.mem_areas);

    /* 'Reserved' area */
    strcpy(init_mem_area[0].name, "Reserved");
    init_mem_area[0].start_pfn = 0;
    init_mem_area[0].end_pfn = kstart_pfn - 1;
    init_mem_area[0].auth = KERNEL_AUTH_RW;
    list_add_tail(&init_mem_area[0].it, &Mem.mem_areas);

    /* 'Kernel' area */
    strcpy(init_mem_area[1].name, "Kernel");
    init_mem_area[1].start_pfn = kstart_pfn;
    init_mem_area[1].end_pfn = kend_pfn - 1;
    init_mem_area[1].auth = KERNEL_AUTH_RO;
    list_add_tail(&init_mem_area[1].it, &Mem.mem_areas);

    /* 'Free' area */
    strcpy(init_mem_area[2].name, "Free");
    init_mem_area[2].start_pfn = kend_pfn;
    init_mem_area[2].end_pfn = MAX_PFN - 1;
    init_mem_area[2].auth = KERNEL_AUTH_RW;
    list_add_tail(&init_mem_area[2].it, &Mem.mem_areas);

    page_alloc_init(kend_pfn, rpage_start, rpage_count);
}

/* Allocaotr per page. */
static void *mem_alloc_pages(int pages)
{
    return alloc_pages(pages);
}

static void *mem_alloc_pages_align(int pages, int align)
{
    return alloc_pages_align(pages, align);
}

static void mem_free_pages(void *addr)
{
    free_pages(addr);
}

/* Allocator per byte from kernel heap. */
static void *mem_alloc(int sz, u32 flag)
{
    tlsf_t tlsf = Thread.cur()->heap;
    assert(sz <= (4 * PAGE_SIZE));

    if (Thread.cur()->heap == NULL) {
        Thread.cur()->heap = alloc_pages(HEAP_SIZE >> PAGE_SHIFT);
        tlsf = tlsf_create_with_pool(Thread.cur()->heap, HEAP_SIZE);
        assert(tlsf != 0 && tlsf == Thread.cur()->heap);
    }
    
    return tlsf_malloc(tlsf, sz);
}

/* Aligned allocator per byte from kernel heap. */
static void *mem_alloc_align(int sz, int align, u32 flag)
{
    tlsf_t tlsf = Thread.cur()->heap; 
    assert(sz <= (4 * PAGE_SIZE));

    if (tlsf  == NULL) {
        Thread.cur()->heap = alloc_pages(HEAP_SIZE >> PAGE_SHIFT);
        tlsf = tlsf_create_with_pool(Thread.cur()->heap, HEAP_SIZE);
        assert(tlsf != 0 && tlsf == Thread.cur()->heap);
    }
    
    return tlsf_memalign(tlsf, align, sz);
}

static void mem_free(void *addr)
{
    tlsf_free(Thread.cur()->heap, addr);
}

static void mem_free_heap(void *addr)
{
    tlsf_destroy(Thread.cur()->heap);
    mem_free_pages(addr);
}

static struct mem_info mem_status(void)
{
    int i;

    for (i = 0; i < 3; ++i) {
        DEBUGF("< %s >\n", init_mem_area[i].name);
        DEBUGF("start_pfn = %d, address = 0x%X\n", 
                init_mem_area[i].start_pfn, init_mem_area[i].start_pfn << PAGE_SHIFT);
        DEBUGF("end_pfn = %d, address = 0x%X\n", 
                init_mem_area[i].end_pfn, init_mem_area[i].end_pfn << PAGE_SHIFT);
    }

    return Mem.info;
}

struct Mem Mem = {
    .init               = mem_init,
    .alloc_pages        = mem_alloc_pages,
    .alloc_pages_align  = mem_alloc_pages_align,
    .free_pages         = mem_free_pages,
    .alloc              = mem_alloc,
    .alloc_align        = mem_alloc_align,
    .free               = mem_free,
    .free_heap          = mem_free_heap,
    .status             = mem_status,
};
