/* arch/arm/paging.c
 *
 * The ARM architecture paging.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <errno.h>
#include <assert.h>
#include <dinos/mem.h>
#include <arch/types.h>
#include <arch/paging.h>

#define MODULE "Paging"

void arch_pt_init(l1_pte_t *pt, int count)
{
    int i;

    for (i = 0; i < count; ++i) {
        pt[i].e.fixed = 0x0;
        pt[i].e.domain = 0;
        pt[i].e.base = 0;
    }
}

/* PAGE_SIZE == 0x100000 */
int arch_super_section_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag)
{
    l1_pte_t *entry1;
    int offset1;
    char *base1 = (char *)pt;   /* L1 page table base address. */
    u32 ap = (flag >> 8) & 0xFF;/* Access permission. */

    /* L1 page table offset. */
    offset1 = vaddr >> 20;

    /* L1 Page table entry address. */
    entry1 = (l1_pte_t *)(base1 + (offset1 * sizeof(l1_pte_t)));

    if (entry1->sse.fixed != 0)
        DEBUGF("Change 0x%.8X address mapping. physical address: 0x%X -> 0x%X\n", vaddr, entry1->sse.base << 24, paddr);

    entry1->sse.fixed = 0x2;
    entry1->sse.fixed2 = 0x1;
    entry1->sse.xn = 1;
    entry1->sse.b = 0;     
    entry1->sse.c = 0;
    entry1->sse.tex = 0;
    entry1->sse.ap = ap;
    entry1->sse.apx = 0;
    entry1->sse.ng = 0;
    entry1->sse.base = paddr >> 24; /* Physical memory base address. */

    return 0;
}

/* PAGE_SIZE == 0x10000 */
int arch_section_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag)
{
    l1_pte_t *entry1;
    int offset1;
    char *base1 = (char *)pt;   /* L1 page table base address. */
    u32 ap = (flag >> 8) & 0xFF;/* Access permission. */

    /* L1 page table offset. */
    offset1 = vaddr >> 20;

    /* L1 Page table entry address. */
    entry1 = (l1_pte_t *)(base1 + (offset1 * sizeof(l1_pte_t)));

    if (entry1->se.fixed != 0)
        DEBUGF("Change 0x%.8X address mapping. physical address: 0x%.8X -> 0x%.8X\n", vaddr, entry1->se.base << 20, paddr);

    entry1->se.fixed = 0x2;
    entry1->se.fixed2 = 0x0;
    entry1->se.xn = 0;
    entry1->se.b = 0;     
    entry1->se.c = 0;
    entry1->se.tex = 0;
    entry1->se.ap = ap;
    entry1->se.apx = 0;
    entry1->se.ng = 0;
    entry1->se.base = paddr >> 20; /* Physical memory base address. */

    return 0;
}

/* PAGE_SIZE == 0x1000 */
int arch_page_mapping(l1_pte_t *pt, addr_t vaddr, addr_t paddr, u32 flag)
{
    l1_pte_t *entry1;
    l2_pte_t *entry2;
    int offset1, offset2;
    char *base1 = (char *)pt;   /* L1 page table base address. */
    char *base2;                /* L2 page table base address. */
    l2_pte_t *pt2;
    u32 ap = (flag >> 8) & 0xFF;/* Access permission. */

    /* L1 page table offset. */
    offset1 = vaddr >> 20;
    /* L1 Page table entry address. */
    entry1 = (l1_pte_t *)(base1 + (offset1 * sizeof(l1_pte_t)));

    assert(entry1->e.fixed == 0);

    if (entry1->e.base == 0) {
        pt2 = (l2_pte_t *)Mem.alloc_pages(1);
        entry1->e.base = v2p((addr_t)pt2 >> 10);
    } else {
        pt2 = (l2_pte_t *)p2v((entry1->e.base << 10));
    }

    base2 = (char *)pt2;

    /* L2 page table offset. */
    offset2 = (vaddr >> 12) & 0xff;
    entry2 = (l2_pte_t *)(base2 + (offset2 * sizeof(l2_pte_t)));

    /* TODO: Set B,C field into the flag option. */
    entry2->e.fixed = 0x2;/* Small page */
    entry2->e.b = 0;      /* Buffer */
    entry2->e.c = 0;      /* Cache */

#if CFG_ARM_VERSION >= 7
    /*
     * < Access permissions for ARMv7. (AP filed) >
     * (APX): (AP): (Privileged), (Unprivileged), (Description)
     * 0: 00: No access,    No access,    Permission Fault
     * 0: 01: Read/Write,   No access,    Privileged Access only
     * 0: 10: Read/Write,   Read,         No user-mode write
     * 0: 11: Read/Write,   Read/Write,   Full access
     * 1: 00: -,            -,            Reserved
     * 1: 01: Read,         No access,    Privileged Read only
     * 1: 10: Read,         Read,         Read only
     * 1: 11: -,            -,            Reserved
     */
    entry2->e.ap = ap;
    /* 
     * TODO: Set apx, tex, ng filed into the flag option. 
     * (Ex> The code area is read only.) 
     */
    entry2->e.apx = 0;
    entry2->e.tex = 0;
    entry2->e.ng = 0;   /* Global */
#else
    /* 
     * < Access permissions. (AP filed) >
     * (AP value): (Privilege mode), (User mode), (System bit), (Rom bit)
     * 11: R/W, R/W, ignore, ignore
     * 10: R/W, RO, ignore, ignore
     * 01: R/W, no access, ignore, ignore
     * 00: no access, no access, 0, 0
     * 00: RO, RO, 0, 1
     * 00: RO, no access, 1, 0
     * 00: unpredictable, unpredictable, 1, 1
     */
    entry2->e.ap0 = ap;
    entry2->e.ap1 = ap;
    entry2->e.ap2 = ap;
    entry2->e.ap3 = ap;
#endif /* CFG_ARM_VERSION >= 7 */

    entry2->e.base = paddr >> 12; /* Physical memory base address. */

    return 0;
}
