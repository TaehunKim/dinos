/* arch/arm/irq.c
 *
 * The ARM architecture interrupt Module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * From: Linux kernel v3.1 (arch/arm/common/gic.c)
 *  Copyright (C) 2002 ARM Limited, All Rights Reserved.
 */

#include <stdio.h>
#include <string.h>
#include <dinos/irq.h>
#include <dinos/io.h>

#include <arch/types.h>
#include <mach/gic.h>
#include <mach/board.h>

#define MODULE "Irq"

static int irq_init(void)
{
    int i;
    const char *def_name = "none";

    log("Initialize IRQ module.\n");

    /* Initilize irq vector table. */
    irq_priv.irq_count = 0;
    for (i=0; i<CFG_NR_IRQS; ++i) {
        strcpy(irq_priv.irq_tlb[i].name, def_name);
        irq_priv.irq_tlb[i].isr = NULL;
        irq_priv.irq_tlb[i].used = 0;
        irq_priv.irq_tlb[i].arg = NULL;
    }

    //board_irq_init();

    return 0;
}

static u32 irq_save(void)
{
    u32 flags;

    asm volatile ("mrs %0, cpsr	/* Save current flags. */\n"
		  "cpsid   i":"=r" (flags)::"memory", "cc");
    return flags;
}

static void irq_restore(u32 flags)
{
    asm volatile("msr cpsr_c, %0 /* Resotre flags. */"
		 ::"r" (flags):"memory", "cc");
}

static void irq_disable(void)
{
    asm volatile ("cpsid i	/* IRQ disable. */":::"memory", "cc");
}

static void irq_enable(void)
{
    asm volatile ("cpsie i	/* IRQ enable. */":::"memory", "cc");
}

static void fiq_disable(void)
{
    asm volatile ("cpsid f	/* FIQ disable. */":::"memory", "cc");
}

static void fiq_enable(void)
{
    asm volatile ("cpsie f	/* FIQ enable. */":::"memory", "cc");
}

static void irq_request(int irq, const char *name, void (*isr)(void *), void *arg)
{
    strcpy(irq_priv.irq_tlb[irq].name, name);
    irq_priv.irq_tlb[irq].isr = isr;
    irq_priv.irq_tlb[irq].used = 1;
    irq_priv.irq_tlb[irq].arg = arg;

    /* Enable interrupt. */
    Io.writel(GIC_DIST_BASE + GIC_DIST_ENABLE_SET + 4 * (irq / 32), 1 << (irq % 32));
}

static void irq_free(int irq)
{
    const char *def_name = "none";
    strcpy(irq_priv.irq_tlb[irq].name, def_name);
    irq_priv.irq_tlb[irq].isr = NULL;
    irq_priv.irq_tlb[irq].used = 0;
    irq_priv.irq_tlb[irq].arg = NULL;
}

static struct intr *irq_get(int irq)
{
    return &irq_priv.irq_tlb[irq];
}

/* IRQ module. */
struct Irq Irq = {
    .init       = irq_init,
    .save       = irq_save,
    .restore    = irq_restore,
    .disable    = irq_disable,
    .enable     = irq_enable,
    .request    = irq_request,
    .free       = irq_free,
    .get        = irq_get,
};
