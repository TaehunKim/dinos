/* arch/arm/io.c
 *
 * Device I/O Module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <dinos/io.h>
#include <mach/board.h>

extern __raw_readsb(unsigned int addr, void *buf, int len);
extern __raw_readsw(unsigned int addr, void *buf, int len);
extern __raw_readsl(unsigned int addr, void *buf, int len);
extern __raw_writesb(unsigned int addr, const void *buf, int len);
extern __raw_writesw(unsigned int addr, const void *buf, int len);
extern __raw_writesl(unsigned int addr, const void *buf, int len);

/**
 * @brief		해당 메모리 번지에서 1 byte 읽어온다.
 * @param[in]	addr 데이타를 읽어 올 주소.
 * @return     	읽은 값.
 */
static unsigned char
arm_readb(unsigned int addr)
{
    return *((volatile unsigned char *)addr);
}

/**
 * @brief		해당 메모리 번지에서 2 byte 읽어온다.
 * @param[in]	addr	데이타를 읽어 올 주소.
 * @return     	읽은 값.
 */
static unsigned short
arm_readw(unsigned int addr)
{
    return *((volatile unsigned short *)addr);
}

/**
 * @brief		해당 메모리 번지에서 4 byte 읽어온다.
 * @param[in]	addr	데이타를 읽어 올 주소.
 * @return     	읽은 값.
 */
static unsigned int
arm_readl(unsigned int addr)
{
    return *((volatile unsigned int *)addr);
}

/**
 * @brief		해당 메모리 번지에서 1 byte를 쓴다.
 * @param[in]	addr	데이타를 쓸 주소.
 * @param[in]	val		쓸 값.
 */
static void
arm_writeb(unsigned int addr, unsigned char val)
{
    *((volatile unsigned char *)addr) = val;
}

/**
 * @brief		해당 메모리 번지에서 2 byte를 쓴다.
 * @param[in]	addr	데이타를 쓸 주소.
 * @param[in]	val		쓸 값.
 */
static void
arm_writew(unsigned int addr, unsigned short val)
{
    *((volatile unsigned short *)addr) = val;
}

/**
 * @brief		해당 메모리 번지에서 4 byte를 쓴다.
 * @param[in]	addr	데이타를 쓸 주소.
 * @param[in]	val		쓸 값.
 */
static void
arm_writel(unsigned int addr, unsigned int val)
{
    *((volatile unsigned int *)addr) = val;
}

static void
arm_readsb(unsigned int addr, void *buf, int len)
{
    __raw_readsb(addr, buf, len);

}

static void
arm_readsw(unsigned int addr, void *buf, int len)
{
    __raw_readsw(addr, buf, len);
}

static void
arm_readsl(unsigned int addr, void *buf, int len)
{
    __raw_readsl(addr, buf, len);
}

static void
arm_writesb(unsigned int addr,const void *buf, int len)
{
    __raw_writesb(addr, buf, len);
}

static void
arm_writesw(unsigned int addr, const void *buf, int len)
{
    __raw_writesw(addr, buf, len);
}

static void
arm_writesl(unsigned int addr, const void *buf, int len)
{
    __raw_writesl(addr, buf, len);
}

struct Io Io = {
    .readb	= arm_readb,
    .readw	= arm_readw,
    .readl	= arm_readl,
    .writeb	= arm_writeb,
    .writew	= arm_writew,
    .writel	= arm_writel,
    .readsb	= arm_readsb,
    .readsw	= arm_readsw,
    .readsl	= arm_readsl,
    .writesb	= arm_writesb,
    .writesw	= arm_writesw,
    .writesl	= arm_writesl,
};
