/* arch/arm/thread.c
 *
 * The ARM architecture thread control module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <dinos/thread.h>
#include <dinos/process.h>
#include <dinos/sched.h>
#include <dinos/mem.h>
#include <arch/context.h>
#include <mach/system.h>

void *idle_thread(void *data)
{
    while(1) {
        Sched.schedule();
        dmb();
        asm("wfi");
    }
    return 0;
}

void arch_context_set(struct thread *t, void *(*func)(void *), void *data)
{
    struct context *con = (struct context *)t->stack_top;

    if (func == NULL) {         /* User mode thread. */
        con->cpsr = 0x10;       /* USER mode. */
        con->pc = 0x01000000;
    } else {                    /* Kernel mode thread. */
        con->cpsr = 0x13;       /* SVC mode. */
        con->pc = (unsigned int)func;
    }

    con->r0 = (unsigned int)data;
    con->r1 = 1;
    con->r2 = 2;
    con->r3 = 3;
    con->r4 = 4;
    con->r5 = 5;
    con->r6 = 6;
    con->r7 = 7;
    con->r8 = 8;
    con->r9 = 9;
    con->r10 = 10;
    con->r11 = 11;
    con->r12 = 12;
    con->lr = (unsigned int)Thread.exit;
}

u32 arch_get_sp(void)
{
    unsigned int sp;
    asm("mov %0, sp":"=r" (sp)::"memory", "cc");
    return sp;
}

void arch_thread_load(struct thread *t)
{
    asm volatile(
        "ldr    sp, %0\n"
        "mov    r1, #0\n"
        "ldr	r10, [sp], #0x4\n"          /* Pop cpsr. */
        "msr    spsr, r10\n"
        "ldmfd	sp!, {r0-r12, lr, pc}^\n"   /* Pop r0-r12, lr, pc. */
        :: "m" (t->stack_top)
	);
}

void arch_context_switch(struct thread *cur, struct thread *next)
{
    register unsigned reg = 0;

    if (cur->proc != next->proc) {
        asm volatile(
            /* Save current context. */
            "stmfd	sp!, {r10-r11}\n"           /* Push r11-r12. */
            "mrs    r10, cpsr\n"
            "adr    r11, switch_end1\n"         /* Set return address. */
            "str    r11, [sp, #-0x4]!\n"        /* Push pc. */
            "stmfd	sp!, {r0-r12, lr}\n"        /* Push r0-r12, lr. */
            "str    r10, [sp, #-0x4]!\n"        /* Push cpsr. */
            "mrc    p15, 0, r10, c2, c0, 0\n"   /* get cp15.c2 */
            "str    r10, %1\n"                  /* Push cp15.c2 */

            "str    sp, %0\n"
            "ldr    sp, %2\n"

            /* Load next context. */
            "ldr	r10, %3\n"                  /* Pop pagetable base addr. */
            "mcr    p15, 0, r10, c2, c0, 0\n"   /* set page table to CP15.c2 */
            "mov	r10, #0\n"
            "mcr    p15, 0, r10, c8, c7, 0\n"   /* flush tlb */

            "ldr	r10, [sp], #0x4\n"          /* Pop cpsr. */
            "msr    spsr, r10\n"
            "ldmfd	sp!, {r0-r12, lr, pc}^\n"   /* Pop r0-r12, lr, pc. */

            "switch_end1:\n"
            "ldmfd	sp!, {r10-r11}\n"           /* Pop r11-r12. */
            :: "m" (cur->stack_top), "m" (cur->proc->pt),
               "m" (next->stack_top), "m" (next->proc->pt)
        );
    } else {
        asm volatile(
            /* Save current context. */
            "stmfd	sp!, {r10-r11}\n"           /* Push r11-r12. */
            "mrs    r10, cpsr\n"
            "adr    r11, switch_end2\n"         /* Set return address. */
            "str    r11, [sp, #-0x4]!\n"        /* Push pc. */
            "stmfd	sp!, {r0-r12, lr}\n"        /* Push r0-r12, lr. */
            "str    r10, [sp, #-0x4]!\n"        /* Push cpsr. */

            "str    sp, %0\n"
            "ldr    sp, %1\n"

            /* Load next context. */
            "ldr	r10, [sp], #0x4\n"          /* Pop cpsr. */
            "msr    spsr, r10\n"
            "ldmfd	sp!, {r0-r12, lr, pc}^\n"   /* Pop r0-r12, lr, pc. */

            "switch_end2:\n"
            "ldmfd	sp!, {r10-r11}\n"           /* Pop r11-r12. */
            :: "m" (cur->stack_top), "m" (next->stack_top)
        );
    }
}
