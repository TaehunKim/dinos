/* arch/arm/mem.c
 *
 * Memory routine for the ARM architecture.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <config.h>

void arch_pt_size(int *pt_size)
{
    int i;

    for (i = 0; i < CFG_PGING_STEP; ++i) {
        pt_size[i] = 0;
    }
}
