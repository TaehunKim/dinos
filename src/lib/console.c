/* lib/console.c
 *
 * Console module.
 *
 * Copyright (C) 2011 Taehun Kim <kth3321@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <config.h>
#include <dinos/file.h>
#include <dinos/thread.h>
#include <dinos/irq.h>
#include <mach/console.h>

static int console_inited = 0;

char getchar(void)
{
    char c;
    struct thread *t = Thread.cur();
    struct file *f = Vfs.get(STDOUT);

    f->f_op->read(f, (char *)&c, 1);
    return c;
}

int putc(char c)
{
    int len;
    unsigned int flags = Irq.save();
    struct thread *t = Thread.cur();
    struct file *f = Vfs.get(STDOUT);

    if (c == '\n')
        len = f->f_op->write(f, (char *)&c, '\r');
    len = f->f_op->write(f, (char *)&c, 1);

    Irq.restore(flags);
    return len;
}

extern void board_print(const char *s, size_t len);

int puts(const char *s)
{
    int len;
    unsigned int flags = Irq.save();

    if (console_inited) {
        struct thread *t = Thread.cur();
        struct file *f = Vfs.get(STDOUT);

        len = f->f_op->write(f, (char *)s, strlen(s));
    } else {
        board_print(s, strlen(s));
    }

    Irq.restore(flags);
    return len;
}

int readline(const char *prompt, char *buf)
{
    char c;
    int len=0;

    buf[0] = '\0';

    puts(prompt);
    while ((c = getchar()) != '\r') {
        putc(c);
        *(buf+len) = c;
        len++;
        if (len > CFG_CBSIZE)
            break;
    }
    puts("\n");
    return len;
}

int printf(const char *format, ...)
{
    va_list args;
    int retval;
    char print_buf[CFG_PBSIZE]={0,};

    va_start(args, format);
    retval = vsprintf(print_buf, format, args);
    //retval = vscnprintf(print_buf, sizeof(print_buf), format, args);
    va_end(args);

    puts(print_buf);

    return retval;
}

struct file f_stdin = {
    .ref_count = 0,
    .f_op = &drv_console_op,
    .name = "stdin"
};

struct file f_stdout = {
    .ref_count = 0,
    .f_op = &drv_console_op,
    .name = "stdout"
};

struct file f_stderr = {
    .ref_count = 0,
    .f_op = &drv_console_op,
    .name = "stderr"
};

static int console_init(void)
{
    Vfs.add(&f_stdin);
    Vfs.add(&f_stdout);
    Vfs.add(&f_stderr);
    drv_console_op.open(NULL);

    console_inited = 1;
    return 0;
}

struct Console Console = {
    .init	= console_init,
};
