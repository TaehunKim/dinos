include $(SRCDIR)/config.mk
include $(SRCDIR)/mach/$(MACH)/config.mk

# Object files.
BOOT_OBJS	+= $(SRCDIR)/mach/$(MACH)/boot_in.o
KERNEL_OBJS	+= $(SRCDIR)/mach/$(MACH)/kernel_in.o
KERNEL_OBJS	+= $(SRCDIR)/arch/$(ARCH)/kernel_in.o
KERNEL_OBJS	+= $(SRCDIR)/lib/kernel_in.o
KERNEL_OBJS	+= $(SRCDIR)/drivers/kernel_in.o
KERNEL_OBJS	+= $(SRCDIR)/net/kernel_in.o
KERNEL_OBJS	+= $(SRCDIR)/common/kernel_in.o
KERNEL_OBJS	+= $(SRCDIR)/fs/kernel_in.o

PYSH_OBJS 	+= $(SRCDIR)/pysh/pysh_in.o

# Create dependency file gcc option
CFLAGS-y += -MMD -MF .$(@F).d

# Dependency files.
DEPS = .*.d

# Compile option. (ref> arch/$(ARCH)/config.mk)
CFLAGS += $(CFLAGS-y)

AFLAGS += -D__ASSEM__
AFLAGS += $(filter-out -std=gnu%,$(filter-out -flto,$(CFLAGS)))

include Makefile

# Sub directories in each kernel_in.o object file
subdir-n := $(patsubst %,%/,$(patsubst %/,%,$(subdir-n) $(subdir-)))
subdir-y := $(patsubst %,%/,$(patsubst %/,%,$(subdir-y)))

# kernel_in.o files in sub directory
kobj-y		+= $(patsubst %/,%/kernel_in.o,$(subdir-y))
subdir-y	+= $(filter %/,$(kobj-y))

# Sub directories in each kernel_in.o object file
py_subdir-n := $(patsubst %,%/,$(patsubst %/,%,$(py_subdir-n) $(py_subdir-)))
py_subdir-y := $(patsubst %,%/,$(patsubst %/,%,$(py_subdir-y)))

# pysh_in.o files in sub directory
pyobj-y		+= $(patsubst %/,%/pysh_in.o,$(py_subdir-y))
py_subdir-y	+= $(filter %/,$(pyobj-y))


# Object files included each built-in.o
bobj-y	:= $(patsubst %/,%/boot_in.o,$(bobj-y))
kobj-y	:= $(patsubst %/,%/kernel_in.o,$(kobj-y))

pyobj-y	:= $(patsubst %/,%/pysh_in.o,$(pyobj-y))

subdir-all := $(subdir-y) $(subdir-n)
py_subdir-all := $(py_subdir-y) $(py_subdir-n)

# Linking for boot_in.o
boot_in.o: CFLAGS += -DBOOT
boot_in.o: $(bobj-y)
	$(Q_LD)$(LD) $(LDFLAGS) -r -o $@ $^

# Linking for kernel_in.o
kernel_in.o: $(kobj-y)
	$(Q_LD)$(LD) $(LDFLAGS) -r -o $@ $^

pysh_in.o: $(pyobj-y)
	$(Q_LD)$(LD) $(LDFLAGS) -r -o $@ $^

# Force execution of pattern rules (for which PHONY cannot be directly used).
.PHONY: FORCE
FORCE:

%/boot_in.o: FORCE
	@$(MAKE) CUR_DIR=$(CUR_DIR)$(patsubst $(SRCDIR)/%,%,$*)/ -f $(SRCDIR)/Rules.mk -C $* boot_in.o

%/kernel_in.o: FORCE
	@$(MAKE) CUR_DIR=$(CUR_DIR)$(patsubst $(SRCDIR)/%,%,$*)/ -f $(SRCDIR)/Rules.mk -C $* kernel_in.o

%/pysh_in.o: FORCE
	@$(MAKE) CUR_DIR=$(CUR_DIR)$(patsubst $(SRCDIR)/%,%,$*)/ -f $(SRCDIR)/Rules.mk -C $* pysh_in.o

.PHONY: clean
clean:: $(addprefix _clean_, $(subdir-all)) $(addprefix _clean_, $(py_subdir-all))
	@rm -f *.o *~ $(DEPS)

_clean_%/: FORCE
	@$(MAKE) -f $(SRCDIR)/Rules.mk -C $* clean

%.o: %.c $(SRCDIR)/Makefile $(SRCDIR)/Rules.mk
	$(Q_CC)$(CC) $(CFLAGS) -c $< -o $@

%.o: %.S $(SRCDIR)/Makefile $(SRCDIR)/Rules.mk
	$(Q_CC)$(CC) $(AFLAGS) -c $< -o $@

# Include dependency files.
-include $(DEPS)
