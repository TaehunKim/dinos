# DINOS kernel repository
DINOS(DIstributed Node Operating System) is next generation operating system for fully cloud OS. 
## Installation
### Ubuntu/Linux
1. Install cross-compiler:
  <pre>
  $ sudo apt-get install gcc-arm-none-eabi
  </pre>
  
2. Install qemu virtual machine:
  <pre>
  $ git clone https://github.com/qemu/qemu.git
  $ cd qemu
  $ ./configure --target-list=arm-softmmu --enable-sdl
  $ make
  $ sudo make install
  </pre>

3. Run DINOS in qemu virtual machine:
  <pre>
  $ git clone https://github.com/Taehun/dinos.git
  $ cd dinos/src
  $ make
  $ /usr/local/bin/qemu-system-arm -m 1024 -M vexpress-a9 -bios pysh.exe -cpu cortex-a9 -nographic -kernel dinos.img
  </pre>
